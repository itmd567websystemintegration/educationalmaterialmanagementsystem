# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repository contains the project for my course ITMD567 - Web System Integration.   
It contains two modules:   
1) Frontend - AngularJS   
2) Backend - Spring Boot project.

### How do I get set up? ###

* Summary of set up
  The setup will require the following things installed on your system.   
  1) Java JDK 1.8   
  2) Mysql Server and workbench   
  3) NodeJS latest verion available   
  i) Install angular-cli module   
  npm install -g @angular/cli@latest   
  ii) You might require these packages too   
  npm install angular2-jwt --save   
  npm install auth0-lock --save    
  Visual Studio Code Editor. (Any other editor would do)   
  IntelliJ or SpringToolSuite to check my backend code.

* Configuration.
For now there are no configurations required.

* Dependencies
For integrating the auth0 service. I have created trial account.

* Database configuration
Create a database 'emms'   
JPA configurations handles the connection on the backend. No need to configure separately.

* How to run tests
1)Open nodejs. Browse to the director educationalmaterialmanagementsystem/emms-frontend   
2)Run 'npm start'   
3) Open any browser.   
4) Browse to 'http://localhost:4200/'. This should start the application.   
5) Click on login button and signup either through new credentials or social login.    
6) After signup, try login with the same credentials.    
7) Once logged in, check out the profile page.    
8) Click logout.    
9) Try logging in again. It should remember your account.    

* Deployment instructions

