import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { StudentService } from '../../../layout/services/student.service';
import { Student } from '../../../layout/models/student';
import { AuthService } from '../../../login/auth/auth.service';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

    pushRightClass: string = 'push-right';
    private student: Student = new Student();
    private idToken: string;

    constructor(public auth: AuthService, private studentService: StudentService, private translate: TranslateService, public router: Router) {
        this.router.events.subscribe((val) => {
            if (val instanceof NavigationEnd && window.innerWidth <= 992 && this.isToggled()) {
                this.toggleSidebar();
            }
        });
        this.idToken =   localStorage.getItem('id_token');
        console.log('header bar token: '+this.idToken);
    }

    getCurrentStudent() {

		this.studentService.getCurrentStudent(this.idToken).subscribe(
			res => {				
				this.student = res.json();
				console.log('current student from db: '+JSON.stringify(this.student));
			},
			err => {
				console.log(err);
			}
		);
	}
  
	ngOnInit() {
		this.getCurrentStudent();
	}

    isToggled(): boolean {
        const dom: Element = document.querySelector('body');
        return dom.classList.contains(this.pushRightClass);
    }

    toggleSidebar() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle(this.pushRightClass);
    }

    rltAndLtr() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle('rtl');
    }

    onLoggedout() {
        this.auth.logout();
    }

    changeLang(language: string) {
        this.translate.use(language);
    }
}
