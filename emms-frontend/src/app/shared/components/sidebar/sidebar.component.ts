import { Component, OnInit } from '@angular/core';
import { StudentService } from '../../../layout/services/student.service';
import { Student } from '../../../layout/models/student';
@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit{
    isActive = false;
    showMenu = '';
    private idToken: string;
    private student: Student = new Student();
    
    constructor(private studentService: StudentService) {
	 }
    eventCalled() {
        this.isActive = !this.isActive;
    }
    addExpandClass(element: any) {
        if (element === this.showMenu) {
            this.showMenu = '0';
        } else {
            this.showMenu = element;
        }
    }

    ngOnInit() {
		this.getCurrentStudent();
    }

    getCurrentStudent() {

		this.studentService.getCurrentStudent(this.idToken).subscribe(
			res => {				
				this.student = res.json();
				console.log('current student from db: '+JSON.stringify(this.student));
			},
			err => {
				console.log(err);
			}
		);
	}
}
