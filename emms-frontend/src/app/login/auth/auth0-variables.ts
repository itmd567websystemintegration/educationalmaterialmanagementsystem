// Source Code Reference: www.auth0.com, quickstart tutorial
interface AuthConfig {
  clientID: string;
  domain: string;
  callbackURL: string;
}

export const AUTH_CONFIG: AuthConfig = {
  clientID: 'qbcXU1zV92P3Bxs5OFdhNSpTCQynGkls',
  domain: 'centralized-auth.auth0.com',
  callbackURL: 'http://localhost:4200/login/callback'
};
