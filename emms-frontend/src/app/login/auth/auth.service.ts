// Source Code Reference: www.auth0.com, quickstart tutorial

import { Injectable } from '@angular/core';
import { AUTH_CONFIG } from './auth0-variables';
import { Router } from '@angular/router';
import 'rxjs/add/operator/filter';
import * as auth0 from 'auth0-js';
import { StudentService } from './../../layout/services/student.service';

@Injectable()
export class AuthService {
  profile: any;
  private emailSent: boolean = false;
  private usernameExists: boolean;
  private emailExists: boolean;
  private username: string;
  private email: string;
  private givenName: string;
  private familyName: string;
  private idToken: string;

  auth0 = new auth0.WebAuth({
    clientID: AUTH_CONFIG.clientID,
    domain: AUTH_CONFIG.domain,
    responseType: 'token id_token',
    audience: `https://${AUTH_CONFIG.domain}/userinfo`,
    redirectUri: AUTH_CONFIG.callbackURL,
    scope: 'openid profile'
  });

  userProfile: any;

  constructor(public router: Router, private studentService: StudentService) { }

  public login(): void {
    console.log("LOGIN CALLED");
    this.auth0.authorize();
  }

  // Check whether the current time is past the
  // access token's expiry time
  public isAuthenticated(): boolean {
    const expiresAt = JSON.parse(localStorage.getItem('expires_at'));
    return new Date().getTime() < expiresAt;
  }

  onLoggedin() {
    localStorage.setItem('isLoggedin', 'true');
  }

  public handleAuthentication(): void {
    if(!this.isAuthenticated()) {
      console.log("HANDLE AUTHENTICATION CALLED");
      this.auth0.parseHash((err, authResult) => {
        if (authResult && authResult.accessToken && authResult.idToken) {
          window.location.hash = '';
          this.setSession(authResult);
          this.setProfile();          
        } else if (err) {
          this.router.navigate(['/login']);
          console.log(err);
          alert(`Error: ${err.error}. Check the console for further details.`);
        }
      });
    }
  }

  public getProfile(cb): void {
    const accessToken = localStorage.getItem('access_token');
    if (!accessToken) {
      throw new Error('Access token must exist to fetch profile');
    }
    const self = this;
    this.auth0.client.userInfo(accessToken, (err, profile) => {
      if (profile) {
        self.userProfile = profile;
      }
      cb(err, profile);
    });
  }

  private setSession(authResult): void {
    // Set the time that the access token will expire at
    console.log('setting settion attributes');
    const expiresAt = JSON.stringify((authResult.expiresIn * 1000) + new Date().getTime());
    localStorage.setItem('access_token', authResult.accessToken);
    localStorage.setItem('id_token', authResult.idToken);
    localStorage.setItem('expires_at', expiresAt);
  }

  public logout(): void {
    // Remove tokens and expiry time from localStorage
    console.log("logging out user>>>");
    localStorage.removeItem('access_token');
    localStorage.removeItem('id_token');
    localStorage.removeItem('expires_at');
    localStorage.removeItem('isLoggedin');
    location.replace('/');
    console.log("logged out successfully>>>");
  }

  setProfile() {
    this.idToken = localStorage.getItem('id_token');
    this.getProfile((err, profile) => {
      this.profile = profile;
      console.log(JSON.stringify(this.profile.nickname));
      this.onNewAccount(this.profile, this.idToken);
    });
  }

  onNewAccount(profile: any, idToken: string) {
    console.log(JSON.stringify(this.profile));
    this.emailSent = false;
    if (this.profile != null) {
      this.username = this.profile.nickname;
      this.email = this.username + "@gmail.com";
      this.givenName = this.profile.given_name;
      this.familyName = this.profile.family_name;
      this.idToken = idToken;
    }


    this.studentService.addStudent(this.username, this.email, this.givenName, this.familyName, this.idToken).subscribe(
      res => {
        console.log(res);
        this.emailSent = true;
      },
      error => {
        console.log(error.text());
        let errorMessage = error.text();
        if (errorMessage === "usernameExists") this.usernameExists = true;
        if (errorMessage === "emailExists") this.emailExists = true;
      }
    );

    location.replace('/dashboard');
  }

}

