import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { routerTransition } from '../router.animations';
import { AuthService } from './auth/auth.service';

@Component({
     selector: 'app-login',
     templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: [routerTransition()]
})
export class LoginComponent implements OnInit {
   
    
    constructor(public router: Router, public auth: AuthService) {
        if(!this.auth.isAuthenticated()) {
            console.log("Session expired: login initiated>>>")
            this.auth.login();      
            // this.auth.handleAuthentication();   
            console.log("login complete>>>") 
        }     
    }

    ngOnInit() {
            
    }
}
