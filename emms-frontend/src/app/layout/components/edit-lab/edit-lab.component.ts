import { Component, OnInit } from '@angular/core';
import { UploadImageService } from '../../services/upload-image.service';
import { LabMaterial } from '../../models/lab';
import { Course } from '../../models/course';
import { Department } from '../../models/department';
import { Params, ActivatedRoute, Router } from '@angular/router';
import { GetLabService } from '../../services/get-lab.service';
import { GetDepartmentListService } from '../../services/get-department-list.service';
import { EditLabService } from '../../services/edit-lab.service';
import { GetCourseListService } from '../../services/get-course-list.service';

@Component({
	selector: 'app-edit-lab',
	templateUrl: './edit-lab.component.html',
	styleUrls: ['./edit-lab.component.scss']
})
export class EditLabComponent implements OnInit {
	private labId: number;
	private lab: LabMaterial = new LabMaterial();
	private department: Department = new Department();
	private course: Course = new Course();
	private labUpdated: boolean;
	private departmentList: Department[] = [];
	private courseList: Course[] = [];

	constructor(
		private uploadImageService: UploadImageService,
		private editLabService: EditLabService,
		private getLabService: GetLabService,
		private getCourseListService: GetCourseListService,
		private getDepartmentListService: GetDepartmentListService,
		private route: ActivatedRoute,
		private router: Router
	) { }

	onSubmit() {
		this.editLabService.editLab(this.lab).subscribe(
			data => {
				console.log("edited lab: " + this.lab);
				this.uploadImageService.modify(JSON.parse(JSON.parse(JSON.stringify(data))._body).id);
				this.labUpdated = true;
				this.lab = new LabMaterial();
			},
			error => console.log(error)
		);
	}

	ngOnInit() {
		this.route.params.forEach((params: Params) => {
			this.labId = Number.parseInt(params['id']);
		});

		this.getLabService.getLab(this.labId).subscribe(
			res => {
				this.lab = res.json();
				console.log("Lab to edit: "+JSON.stringify(this.lab));
				this.department = this.lab.department;
				this.course = this.lab.course;
			},
			error => console.log(error)
		)

		this.getDepartmentListService.getDepartmentList().subscribe(
			res => {
				console.log(res.json());
				this.departmentList = res.json();
			},
			error => {
				console.log(error.text());
			}
		);

		this.getCourseListService.getCourseList().subscribe(
			res => {
				console.log(res.json());
				this.courseList = res.json();
			},
			error => {
				console.log(error.text());
			}
		);
	}
}
