import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { OrderHistoryRoutingModule } from './order-history.routing.module';
import { OrderHistoryComponent } from './order-history.component';
import { PageHeaderModule } from './../../../shared';
import { DataFilterPipe } from './../../components/order-history/data-filter.pipe';
import { DataTableModule } from 'angular2-datatable';


import { StudentOrderService } from './../../services/order.service';
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        OrderHistoryRoutingModule,
        PageHeaderModule,
        DataTableModule
    ],
declarations: [OrderHistoryComponent, DataFilterPipe],
providers: [
    StudentOrderService
  ]
})
export class OrderHistoryModule { }
