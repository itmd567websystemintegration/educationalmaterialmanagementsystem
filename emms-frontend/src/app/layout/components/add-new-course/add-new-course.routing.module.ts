import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddNewCourseComponent } from './add-new-course.component';

const routes: Routes = [
    { path: '', component: AddNewCourseComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AddNewCourseRoutingModule { }
