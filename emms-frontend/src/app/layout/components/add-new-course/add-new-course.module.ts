import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AddNewCourseRoutingModule } from './add-new-course.routing.module';
import { AddNewCourseComponent } from './add-new-course.component';
import { PageHeaderModule } from './../../../shared';
import { GetCourseListService } from './../../services/get-course-list.service';
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        AddNewCourseRoutingModule,
        PageHeaderModule
    ],
declarations: [AddNewCourseComponent],
providers: [
    GetCourseListService,
  ]
})
export class AddNewCourseModule { }
