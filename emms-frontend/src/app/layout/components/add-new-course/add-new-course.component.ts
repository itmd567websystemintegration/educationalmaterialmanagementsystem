import { Component, OnInit } from '@angular/core';
import { Course } from '../../models/course';
import { GetCourseListService } from '../../services/get-course-list.service';
@Component({
	selector: 'app-add-new-course',
	templateUrl: './add-new-course.component.html',
	styleUrls: ['./add-new-course.component.css']
})
export class AddNewCourseComponent implements OnInit {
	private course: Course = new Course();
	private courseAdded: boolean;
	
	constructor(private getCourseListService: GetCourseListService) { }
	
	onSubmit() {
		console.log(this.course);
			this.getCourseListService.addCourse(this.course).subscribe(
				res => {
					
					this.courseAdded = true;
					this.course = new Course();
				},
				error => {
					console.log(error);
				}
			);
	}

	ngOnInit() {
		this.courseAdded = false;
	}
}
