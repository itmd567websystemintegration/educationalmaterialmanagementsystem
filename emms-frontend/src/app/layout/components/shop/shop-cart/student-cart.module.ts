import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { StudentCartRoutingModule } from './student-cart.routing.module';
import { StudentCartComponent } from './student-cart.component';
import { PageHeaderModule } from './../../../../shared';
import { StudentCartService } from './../../../services/student-cart.service';
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        StudentCartRoutingModule,
        PageHeaderModule
    ],
declarations: [StudentCartComponent],
providers: [StudentCartService]
})
export class StudentCartModule { }
