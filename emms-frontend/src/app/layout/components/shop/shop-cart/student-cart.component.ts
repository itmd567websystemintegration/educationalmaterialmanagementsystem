import { Component, OnInit } from '@angular/core';
import { AppProperties } from '../../../constants/app-properties';
import { Router } from '@angular/router';
import { Book } from '../../../models/book';
import { Notes } from '../../../models/notes';
import { LabMaterial } from '../../../models/lab';
import { StudentCartService } from '../../../services/student-cart.service';
import { StudentCart } from '../../../models/student-cart';
import { SelectedBook } from '../../../models/selected-book';
import { SelectedNotes } from '../../../models/selected-notes';
import { SelectedLabMaterial } from '../../../models/selected-lab';

@Component({
	selector: 'app-student-cart',
	templateUrl: './student-cart.component.html',
	styleUrls: ['./student-cart.component.scss']
})
export class StudentCartComponent implements OnInit {
	private serverPath = AppProperties.serverPath;
	private selectedBook: Book;
	private selectedBookist: SelectedBook[] = [];
	private selectedNotes: Notes;
	private selectedNotesList: SelectedNotes[] = [];
	private selectedLab: LabMaterial;
	private selectedLabList: SelectedLabMaterial[] = [];
	private studentCart: StudentCart = new StudentCart();
	private studentCartUpdated: boolean;
	private emptyCart: boolean = false;
	private isBookCartEmpty: boolean = false;
	private isNotesCartEmpty: boolean = false;
	private isLabCartEmpty: boolean = false;
	constructor(
		private router: Router,
		private studentCartService: StudentCartService
	) { }

	onSelect(book: Book) {
		this.selectedBook = book;
		this.router.navigate(['/book-detail', this.selectedBook.id]);
	}

	onRemoveCartItem(selectedBook: SelectedBook) {
		this.studentCartService.deleteSelectedItem(selectedBook.id).subscribe(
			res => {
				console.log(res.text());
				this.getSelectedBookList();
				this.getStudentCart();
			},
			error => {
				console.log(error.text());
			}
		);
	}

	onRemoveCartNotesItem(selectedNotes: SelectedNotes) {
		this.studentCartService.deleteSelectedNotesItem(selectedNotes.id).subscribe(
			res => {
				console.log(res.text());
				this.getSelectedNotesList();
				this.getStudentCart();
			},
			error => {
				console.log(error.text());
			}
		);
	}
	
	onRemoveCartLabsItem(selectedLab: SelectedLabMaterial) {
		this.studentCartService.deleteSelectedLabItem(selectedLab.id).subscribe(
			res => {
				console.log(res.text());
				this.getSelectedLabList();
				this.getStudentCart();
			},
			error => {
				console.log(error.text());
			}
		);
	}

	getSelectedBookList() {
		this.studentCartService.getSelectedBookList().subscribe(
			res => {
				this.selectedBookist = res.json();
				console.log(JSON.stringify(this.selectedBookist.length));						
				if (typeof this.selectedBookist === 'undefined'|| this.selectedBookist.length == 0) {
					console.log('isBookCartEmpty is empty'+this.isBookCartEmpty);
					this.isBookCartEmpty = false;				
				} else {
					console.log('selectedBookist is not empty'+this.isBookCartEmpty);
					this.isBookCartEmpty = true;
				}
				if (this.isLabCartEmpty || this.isNotesCartEmpty || this.isBookCartEmpty) {	
					console.log('emptyCart'+this.emptyCart);				
					this.emptyCart =  false;
					console.log('emptyCart'+this.emptyCart);
				} else {
					console.log('emptyCart'+this.emptyCart);
					this.emptyCart =  true;
					console.log('emptyCart'+this.emptyCart);
				}
			},
			error => {
				console.log(error.text());
			}
		)
	}

	getSelectedNotesList() {
		this.studentCartService.getSelectedNotesList().subscribe(
			res => {
				this.selectedNotesList = res.json();
				console.log(JSON.stringify(this.selectedNotesList));				
				if (typeof this.selectedNotesList === 'undefined' || this.selectedNotesList.length == 0) {
					console.log('isNotesCartEmpty is empty'+this.isLabCartEmpty);
					this.isNotesCartEmpty = false;
				} else {
					console.log('selectedNotesList is not empty'+this.isNotesCartEmpty);
					this.isNotesCartEmpty = true;
				}
				if (this.isLabCartEmpty || this.isNotesCartEmpty || this.isBookCartEmpty) {	
					console.log('emptyCart'+this.emptyCart);				
					this.emptyCart =  false;
					console.log('emptyCart'+this.emptyCart);
				} else {
					console.log('emptyCart'+this.emptyCart);
					this.emptyCart =  true;
					console.log('emptyCart'+this.emptyCart);
				}
			},
			error => {
				console.log(error.text());
			}
		)
	}

	getSelectedLabList() {
		this.studentCartService.getSelectedLabList().subscribe(
			res => {
				this.selectedLabList = res.json();
				if (typeof this.selectedLabList === 'undefined' || this.selectedLabList.length == 0) {
					console.log('isLabCartEmpty is empty'+this.isLabCartEmpty);
					this.isLabCartEmpty = false;
				} else {
					console.log('isLabCartEmpty is not empty'+this.isLabCartEmpty);
					this.isLabCartEmpty = true;
				}
				if (this.isLabCartEmpty || this.isNotesCartEmpty || this.isBookCartEmpty) {	
					console.log('emptyCart'+this.emptyCart);				
					this.emptyCart =  false;
					console.log('emptyCart'+this.emptyCart);
				} else {
					console.log('emptyCart'+this.emptyCart);
					this.emptyCart =  true;
					console.log('emptyCart'+this.emptyCart);
				}
			},
			error => {
				console.log(error.text());
			}
		)
	}

	getStudentCart() {
		this.studentCartService.getStudentCart().subscribe(
			res => {
				console.log(res.json());
				this.studentCart = res.json();
			},
			error => {
				console.log(error.text());
			}
		)
	}

	ngOnInit() {
		this.getSelectedBookList();
		this.getSelectedNotesList();
		this.getSelectedLabList();
		this.getStudentCart();
	}

}
