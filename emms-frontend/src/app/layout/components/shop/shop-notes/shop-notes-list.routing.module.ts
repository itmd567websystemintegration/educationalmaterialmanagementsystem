import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ShopNotesListComponent } from './shop-notes-list.component';

const routes: Routes = [
    { path: '', component: ShopNotesListComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ShopNotesListRoutingModule { }
