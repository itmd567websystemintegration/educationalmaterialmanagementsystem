import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ShopNotesListRoutingModule } from './shop-notes-list.routing.module';
import { ShopNotesListComponent } from './shop-notes-list.component';
import { PageHeaderModule } from './../../../../shared';
import { GetNotesListService } from './../../../services/get-notes-list.service';
import { ShopNotesDataFilterPipe } from './../../../components/shop/shop-notes/data-filter.pipe';
import { DataTableModule } from 'angular2-datatable';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ShopNotesListRoutingModule,
        PageHeaderModule,
        DataTableModule
    ],
declarations: [ShopNotesListComponent, ShopNotesDataFilterPipe],
providers: [
    GetNotesListService,
  ]
})
export class ShopNotesListModule { }