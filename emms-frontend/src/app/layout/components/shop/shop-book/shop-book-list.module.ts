import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ShopBookListRoutingModule } from './shop-book-list.routing.module';
import { ShopBookListComponent } from './shop-book-list.component';
import { PageHeaderModule } from './../../../../shared';
import { GetBookListService } from './../../../services/get-book-list.service';
import { ShopBookDataFilterPipe } from './../../../components/shop/shop-book/data-filter.pipe';
import { DataTableModule } from 'angular2-datatable';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ShopBookListRoutingModule,
        PageHeaderModule,
        DataTableModule
    ],
declarations: [ShopBookListComponent, ShopBookDataFilterPipe],
providers: [
    GetBookListService,
  ]
})
export class ShopBookListModule { }
