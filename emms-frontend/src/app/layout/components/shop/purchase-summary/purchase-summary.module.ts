import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { PurchaseSummaryRoutingModule } from './purchase-summary.routing.module';
import { PurchaseSummaryComponent } from './purchase-summary.component';
import { PageHeaderModule } from './../../../../shared';
import { StudentCartService } from './../../../services/student-cart.service';
import {StudentDeliveryDetailsService} from '../../../services/delivery.service';
import {PaymentService} from '../../../services/payment.service';
import {StudentCheckoutService} from '../../../services/checkout.service';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        PurchaseSummaryRoutingModule,
        PageHeaderModule,
    ],
declarations: [PurchaseSummaryComponent],
providers: [
    StudentCartService, StudentCheckoutService, StudentDeliveryDetailsService, PaymentService ]
})
export class PurchaseSummaryModule { }
