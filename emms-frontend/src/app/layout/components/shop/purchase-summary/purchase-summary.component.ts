import { Component, OnInit } from '@angular/core';
import { AppProperties } from '../../../constants/app-properties';
import { Params, ActivatedRoute, Router } from '@angular/router';
import { StudentCheckoutService } from '../../../services/checkout.service';
import { StudentOrder } from '../../../models/student-order';
import { SelectedBook } from '../../../models/selected-book';
import {SelectedNotes} from '../../../models/selected-notes';
import {SelectedLabMaterial} from '../../../models/selected-lab';
import {Book} from '../../../models/book';
import {Notes} from '../../../models/notes';
import {LabMaterial} from '../../../models/lab';

@Component({
	selector: 'app-purchase-summary',
	templateUrl: './purchase-summary.component.html',
	styleUrls: ['./purchase-summary.component.scss']
})
export class PurchaseSummaryComponent implements OnInit {
	public serverPath = AppProperties.serverPath;
	public order: StudentOrder = new StudentOrder();
	public estimatedDeliveryDate: string;
	public selectedBookList: SelectedBook[] = [];
	public selectedBook: Book;
	private selectedNotes: Notes;
	private selectedNotesList: SelectedNotes[] = [];
	private selectedLabMaterial: LabMaterial;
	private selectedLabMaterialList: SelectedLabMaterial[] = [];
	public studentPaymentStatus: boolean = false;
	public studentPaymentAuthId: string;
	constructor(
		private studentCheckoutService: StudentCheckoutService,
		private route: ActivatedRoute,
		private router: Router
	) { }

	ngOnInit() {
		this.route.queryParams.subscribe(params => {
			this.order = JSON.parse(params['order']);
			console.log("Order Summary: "+JSON.stringify(this.order));
			if(this.order.studentPaymentStatus == 'APPROVED') {
				this.studentPaymentStatus = true;
				this.studentPaymentAuthId = this.order.studentPaymentAuthorizationId;
				let deliveryDate = new Date();
				if (this.order.deliveryMethod == "groundShipping") {
					deliveryDate.setDate(deliveryDate.getDate() + 5);
				} else {
					deliveryDate.setDate(deliveryDate.getDate() + 3);
				}
	
				let days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
				this.estimatedDeliveryDate = days[deliveryDate.getDay()] + ', ' + deliveryDate.getFullYear() + '/' + (deliveryDate.getMonth() + 1) + '/' + deliveryDate.getDate();
				console.log("EstimatedDeliveryDate: "+this.estimatedDeliveryDate);
				this.selectedBookList = this.order.selectedBookList;
				this.selectedNotesList = this.order.selectedNotesList;
				this.selectedLabMaterialList = this.order.selectedLabMaterialList;
				console.log("Selected Book List: "+JSON.stringify(this.selectedBookList));
			} else {
				this.studentPaymentStatus = false;
			}			
		});
	}

}
