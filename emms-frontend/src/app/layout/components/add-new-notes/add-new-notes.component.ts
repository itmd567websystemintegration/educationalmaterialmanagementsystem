import { Component, OnInit } from '@angular/core';
import { Notes } from '../../models/notes';
import { AddItemService } from '../../services/add-item.service';
import { UploadImageService } from '../../services/upload-image.service';
import { Department } from '../../models/department';
import { Course } from '../../models/course';
import { GetDepartmentListService } from '../../services/get-department-list.service';
import { GetCourseListService } from '../../services/get-course-list.service';
import { StudentService } from '../../../layout/services/student.service';
import { Student } from '../../../layout/models/student';

@Component({
	selector: 'app-add-new-notes',
	templateUrl: './add-new-notes.component.html',
	styleUrls: ['./add-new-notes.component.scss']
})
export class AddNewNotesComponent implements OnInit {
	private notesAdded: boolean;
	private department: Department = new Department();
	private course: Course = new Course();
	private newNotes: Notes = new Notes();
	private departmentList: Department[] = [];
	private courseList: Course[] = [];
	private idToken: string;
	private student: Student = new Student();
	
	constructor(private studentService: StudentService, private addItemService: AddItemService, private uploadImageService: UploadImageService,
		private getDepartmentListService: GetDepartmentListService, private getCourseListService: GetCourseListService) { }

	onSubmit() {
		this.newNotes.department = this.department;
		this.newNotes.course = this.course;
		this.newNotes.student = this.student;
		console.log(this.newNotes);
		this.addItemService.addNotes(this.newNotes).subscribe(
			res => {
				console.log('image to add: ' + JSON.parse(JSON.parse(JSON.stringify(res))._body).id);
				this.uploadImageService.upload(JSON.parse(JSON.parse(JSON.stringify(res))._body).id, "notes");
				this.notesAdded = true;
				this.newNotes = new Notes();
			},
			error => {
				console.log(error);
			}
		);
	}

	ngOnInit() {
		this.notesAdded = false;

		this.idToken = localStorage.getItem('id_token');

		this.getDepartmentListService.getDepartmentList().subscribe(
			res => {
				console.log(res.json());
				this.departmentList = res.json();
			},
			error => {
				console.log(error.text());
			}
		);

		this.getCourseListService.getCourseList().subscribe(
			res => {
				console.log(res.json());
				this.courseList = res.json();
			},
			error => {
				console.log(error.text());
			}
		);

		this.studentService.getCurrentStudent(this.idToken).subscribe(
			res => {
				this.student = res.json();
				console.log('current student from db: ' + JSON.stringify(this.student));
			},
			err => {
				console.log(err);
			}
		);
	}
}
