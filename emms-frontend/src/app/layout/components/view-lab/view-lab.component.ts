import { Component, OnInit } from '@angular/core';
import {Params, ActivatedRoute, Router} from '@angular/router';
import {GetLabService} from '../../services/get-lab.service';
import {LabMaterial} from '../../models/lab';
import {Student} from '../../models/student';

@Component({
  selector: 'app-view-lab',
  templateUrl: './view-lab.component.html',
  styleUrls: ['./view-lab.component.scss']
})
export class ViewLabComponent implements OnInit {

  private lab:LabMaterial = new LabMaterial();
  private labId: number;

  constructor(private getLabService:GetLabService,
  	private route:ActivatedRoute, private router:Router) { }

  onSelect(lab:LabMaterial) {
    console.log(lab);
    this.router.navigate(['/edit-lab', this.lab.id]);
  }

  ngOnInit() {
  	this.route.params.forEach((params: Params) => {
  		this.labId = Number.parseInt(params['id']);
  	});

  	this.getLabService.getLab(this.labId).subscribe(
  		res => {
  			this.lab = res.json();
  		},
  		error => {
  			console.log(error);
  		}
  	);

  	
  }

}
