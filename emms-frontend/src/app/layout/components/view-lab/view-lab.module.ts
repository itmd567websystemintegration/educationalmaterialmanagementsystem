import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ViewLabRoutingModule } from './view-lab.routing.module';
import { ViewLabComponent } from './view-lab.component';
import { PageHeaderModule } from './../../../shared';
import { GetLabService } from './../../services/get-lab.service';
import { UploadImageService } from './../../services/upload-image.service';
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ViewLabRoutingModule,
        PageHeaderModule
    ],
declarations: [ViewLabComponent],
providers: [
    GetLabService,
    UploadImageService
  ]
})
export class ViewLabModule { }
