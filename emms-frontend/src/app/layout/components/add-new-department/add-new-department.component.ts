import { Component, OnInit } from '@angular/core';
import { Department } from '../../models/department';
import { GetDepartmentListService } from '../../services/get-department-list.service';
@Component({
	selector: 'app-add-new-department',
	templateUrl: './add-new-department.component.html',
	styleUrls: ['./add-new-department.component.css']
})
export class AddNewDepartmentComponent implements OnInit {
	private department: Department = new Department();
	private departmentAdded: boolean;
	
	constructor(private getDepartmentListService: GetDepartmentListService) { }
	
	onSubmit() {
		console.log(this.department);
			this.getDepartmentListService.addDepartment(this.department).subscribe(
				res => {
					
					this.departmentAdded = true;
					this.department = new Department();
				},
				error => {
					console.log(error);
				}
			);
	}

	ngOnInit() {
		this.departmentAdded = false;
	}
}
