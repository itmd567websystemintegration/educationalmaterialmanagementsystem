import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddNewDepartmentComponent } from './add-new-department.component';

const routes: Routes = [
    { path: '', component: AddNewDepartmentComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AddNewDepartmentRoutingModule { }
