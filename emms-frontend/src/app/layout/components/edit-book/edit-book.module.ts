import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { EditBookRoutingModule } from './edit-book.routing.module';
import { EditBookComponent } from './edit-book.component';
import { PageHeaderModule } from './../../../shared';

import { GetCourseListService } from './../../services/get-course-list.service';
import { GetDepartmentListService } from './../../services/get-department-list.service';
import { GetBookService } from './../../services/get-book.service';
import { EditBookService } from './../../services/edit-book.service';
import { UploadImageService } from './../../services/upload-image.service';
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        EditBookRoutingModule,
        PageHeaderModule
    ],
declarations: [EditBookComponent],
providers: [
    GetDepartmentListService,
    GetCourseListService,
    GetBookService,
    EditBookService,
    UploadImageService
  ]
})
export class EditBookModule { }
