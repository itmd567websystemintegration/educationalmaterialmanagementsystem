import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ViewOrderRoutingModule } from './view-order.routing.module';
import { ViewOrderComponent } from './view-order.component';
import { PageHeaderModule } from './../../../shared';

import { StudentOrderService } from './../../services/order.service';
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ViewOrderRoutingModule,
        PageHeaderModule
    ],
declarations: [ViewOrderComponent],
providers: [
    StudentOrderService
  ]
})
export class ViewOrderModule { }
