import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AddNewLabRoutingModule } from './add-new-lab.routing.module';
import { AddNewLabComponent } from './add-new-lab.component';
import { PageHeaderModule } from './../../../shared';
import { StudentService } from '../../../layout/services/student.service';
import { GetDepartmentListService } from './../../services/get-department-list.service';
import { GetCourseListService } from './../../services/get-course-list.service';

import { AddItemService } from './../../services/add-item.service';
import { UploadImageService } from './../../services/upload-image.service';
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        AddNewLabRoutingModule,
        PageHeaderModule
    ],
declarations: [AddNewLabComponent],
providers: [
    AddItemService,
    UploadImageService,
    GetDepartmentListService,
    GetCourseListService,
    StudentService
  ]
})
export class AddNewLabModule { }
