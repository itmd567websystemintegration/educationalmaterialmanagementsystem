import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddNewLabComponent } from './add-new-lab.component';

const routes: Routes = [
    { path: '', component: AddNewLabComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AddNewLabRoutingModule { }
