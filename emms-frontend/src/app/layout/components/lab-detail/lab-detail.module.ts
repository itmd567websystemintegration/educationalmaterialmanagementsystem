import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ItemService } from '../../services/item.service';
import { StudentCartService } from '../../services/student-cart.service';
import { LabDetailRoutingModule } from './lab-detail.routing.module';
import { LabDetailComponent } from './lab-detail.component';
import { PageHeaderModule } from './../../../shared';

@NgModule({
    imports: [
        CommonModule,
        LabDetailRoutingModule,
        PageHeaderModule
    ],
declarations: [LabDetailComponent],
providers: [StudentCartService, ItemService]
})
export class LabDetailModule { }
