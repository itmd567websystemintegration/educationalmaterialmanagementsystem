import { Component, OnInit } from '@angular/core';
import { LabMaterial } from '../../models/lab';
import { ItemService } from '../../services/item.service';
import { StudentCartService } from '../../services/student-cart.service';
import {Params, ActivatedRoute, Router} from '@angular/router';
import {Http} from '@angular/http';
import {AppProperties} from '../../constants/app-properties';

@Component({
  selector: 'app-lab-detail',
  templateUrl: './lab-detail.component.html',
	styleUrls: ['./lab-detail.component.scss']
})
export class LabDetailComponent implements OnInit {
	private labId: number;
	private lab: LabMaterial = new LabMaterial();
	private serverPath = AppProperties.serverPath;
	private addLabSuccess: boolean = false;

  constructor(
  	private itemService:ItemService,
    private studentCartService: StudentCartService,
		private router:Router,
		private http:Http,
		private route:ActivatedRoute
  	) { }

  onAddLabToStudentCart() {
    this.studentCartService.addLab(this.labId).subscribe(
      res => {
        console.log(res.text());
        this.addLabSuccess=true;
      },
      err => {
        console.log(err.text());
      }
    );
	}
	
  ngOnInit() {

  	this.route.params.forEach((params: Params) => {
  		this.labId = Number.parseInt(params['id']);
  	});

  	this.itemService.getLabs(this.labId).subscribe(
  		res => {
				this.lab=res.json();
				console.log(JSON.stringify(this.lab));
  		},
  		error => {
  			console.log(error);
  		}
  	);
  }
}
