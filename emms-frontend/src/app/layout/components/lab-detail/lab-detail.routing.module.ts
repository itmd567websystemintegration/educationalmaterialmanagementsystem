import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LabDetailComponent } from './lab-detail.component';

const routes: Routes = [
    { path: '', component: LabDetailComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LabDetailRoutingModule { }
