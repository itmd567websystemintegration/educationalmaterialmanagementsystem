import { Component, OnInit } from '@angular/core';
import { Notes } from '../../models/notes';
import { Router } from '@angular/router';
// import {LoginService} from '../../services/login.service';
import { GetNotesListService } from '../../services/get-notes-list.service';
import { DeleteNotesService } from '../../services/delete-notes.service';


@Component({
  selector: 'app-notes-list',
  templateUrl: './notes-list.component.html',
  styleUrls: ['./notes-list.component.scss']
})
export class NotesListComponent implements OnInit {
  private selectedNotes: Notes;
  private checked: boolean;
  private notesList: Notes[];
  private allChecked: boolean;
  private deleteNotesList: Notes[] = new Array();
  public rowsOnPage = 5;
  constructor(
    private getNotesListService: GetNotesListService,
    private deleteNotesService: DeleteNotesService,
    private router: Router
  ) { }

  onSelect(notes: Notes) {
    this.selectedNotes = notes;
    console.log("notes-list: " + notes);
    this.router.navigate(['/view-notes', this.selectedNotes.id]);
  }

  onDelete(notes: Notes) {
    console.log('delete called...');
    this.deleteNotesService.deleteNotes(notes.id).subscribe(
      res => {
        console.log(res);
        this.getNotesListByLoggedInStudent();
      },
      err => {
        console.log(err);
      }
    );
  }


  updateDeleteNotesList(checked: boolean, notes: Notes) {
    if (checked) {
      this.deleteNotesList.push(notes);
    } else {
      this.deleteNotesList.splice(this.deleteNotesList.indexOf(notes), 1);
    }
  }

  updateSelected(checked: boolean) {
    if (checked) {
      this.allChecked = true;
      this.deleteNotesList = this.notesList.slice();
    } else {
      this.allChecked = false;
      this.deleteNotesList = [];
    }
  }

  removeSelectedNotes() {
    for (let notes of this.deleteNotesList) {
      this.deleteNotesService.deleteNotes(notes.id).subscribe(
        res => {
          this.getNotesListByLoggedInStudent();
        },
        err => {
        }
      );
    }
    location.reload();
  }


  getNotesListByLoggedInStudent() {
    this.getNotesListService.getNotesListByLoggedInStudent().subscribe(
      res => {        
        this.notesList = res.json();
      },
      error => {
        console.log(error);
      }
    );
  }

  ngOnInit() {
    console.log('on init called');
    this.getNotesListByLoggedInStudent();
  }
}