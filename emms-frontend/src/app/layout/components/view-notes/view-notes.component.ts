import { Component, OnInit } from '@angular/core';
import {Params, ActivatedRoute, Router} from '@angular/router';
import {GetNotesService} from '../../services/get-notes.service';
import {Notes} from '../../models/notes';
import {Student} from '../../models/student';

@Component({
  selector: 'app-view-notes',
  templateUrl: './view-notes.component.html',
  styleUrls: ['./view-notes.component.scss']
})
export class ViewNotesComponent implements OnInit {

  private notes:Notes = new Notes();
  private notesId: number;

  constructor(private getNotesService:GetNotesService,
  	private route:ActivatedRoute, private router:Router) { }

  onSelect(notes:Notes) {
    console.log(notes);
    this.router.navigate(['/edit-notes', this.notes.id])
    // .then(s => location.reload())
    ;
  }

  ngOnInit() {
  	this.route.params.forEach((params: Params) => {
  		this.notesId = Number.parseInt(params['id']);
  	});

  	this.getNotesService.getNotes(this.notesId).subscribe(
  		res => {
  			this.notes = res.json();
  		},
  		error => {
  			console.log(error);
  		}
  	);

  	
  }

}
