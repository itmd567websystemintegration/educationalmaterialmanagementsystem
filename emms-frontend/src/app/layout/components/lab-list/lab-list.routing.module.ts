import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LabListComponent } from './lab-list.component';

const routes: Routes = [
    { path: '', component: LabListComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LabListRoutingModule { }
