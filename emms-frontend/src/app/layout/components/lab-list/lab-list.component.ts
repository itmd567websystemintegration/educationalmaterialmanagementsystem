import { Component, OnInit } from '@angular/core';
import { LabMaterial } from '../../models/lab';
import { Router } from '@angular/router';
import { GetLabListService } from '../../services/get-lab-list.service';
import { DeleteLabService } from '../../services/delete-lab.service';


@Component({
  selector: 'app-lab-list',
  templateUrl: './lab-list.component.html',
  styleUrls: ['./lab-list.component.scss']
})
export class LabListComponent implements OnInit {
  private selectedLab: LabMaterial;
  private checked: boolean;
  private labList: LabMaterial[];
  private allChecked: boolean;
  private deleteLabList: LabMaterial[] = new Array();
  public rowsOnPage = 5;
  constructor(
    private getLabListService: GetLabListService,
    private deleteLabService: DeleteLabService,
    private router: Router
  ) { }

  onSelect(lab: LabMaterial) {
    this.selectedLab = lab;
    console.log("lab-list: " + lab);
    this.router.navigate(['/view-lab', this.selectedLab.id]);
  }

  onDelete(lab: LabMaterial) {
    console.log('delete called...');
    this.deleteLabService.deleteLab(lab.id).subscribe(
      res => {
        console.log(res);
        this.getLabListByLoggedInStudent();
      },
      err => {
        console.log(err);
      }
    );
  }


  updateDeleteLabList(checked: boolean, lab: LabMaterial) {
    if (checked) {
      this.deleteLabList.push(lab);
    } else {
      this.deleteLabList.splice(this.deleteLabList.indexOf(lab), 1);
    }
  }

  updateSelected(checked: boolean) {
    if (checked) {
      this.allChecked = true;
      this.deleteLabList = this.labList.slice();
    } else {
      this.allChecked = false;
      this.deleteLabList = [];
    }
  }

  removeSelectedLab() {
    for (let lab of this.deleteLabList) {
      this.deleteLabService.deleteLab(lab.id).subscribe(
        res => {
          this.getLabListByLoggedInStudent();
        },
        err => {
        }
      );
    }
    location.reload();
  }


  getLabListByLoggedInStudent() {
    this.getLabListService.getLabListByLoggedInStudent().subscribe(
      res => {        
        this.labList = res.json();
      },
      error => {
        console.log(error);
      }
    );
  }

  ngOnInit() {
    console.log('on init called');
    this.getLabListByLoggedInStudent();
  }
}