import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../../router.animations';

@Component({
    selector: 'add-book',
    templateUrl: './add-book.component.html',
    styleUrls: ['./add-book.component.scss'],
    animations: [routerTransition()]
})
export class AddBookComponent implements OnInit {
    constructor() { }
    ngOnInit() {}
}
