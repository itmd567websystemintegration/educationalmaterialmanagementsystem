import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../../router.animations';

@Component({
    selector: 'add-notes',
    templateUrl: './add-notes.component.html',
    styleUrls: ['./add-notes.component.scss'],
    animations: [routerTransition()]
})
export class AddNotesComponent implements OnInit {
    constructor() { }
    ngOnInit() {}
}
