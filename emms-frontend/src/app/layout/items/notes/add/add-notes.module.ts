import { Component, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { AddNotesRoutingModule } from './add-notes-routing.module';
import { AddNotesComponent } from './add-notes.component';
import { DepartmentListComponent } from './department-list.component';
import { PageHeaderModule } from './../../../../shared';

@NgModule({
    imports: [
        CommonModule, 
        FormsModule,
        AddNotesRoutingModule,
        PageHeaderModule,
        
    ],
declarations: [AddNotesComponent, DepartmentListComponent], 
bootstrap: [ DepartmentListComponent ]
})
export class AddNotesModule { }
