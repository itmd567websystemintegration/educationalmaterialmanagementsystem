import { Component, OnInit } from '@angular/core';
import {BrowserModule} from '@angular/platform-browser'
import { Department } from './department';

@Component({
  selector: 'department-list',
  templateUrl: './department-list.component.html'
})
export class DepartmentListComponent {
  selectedDepartment:Department = new Department(2, 'Computer Science');
  countries = [
     new Department(1, 'Information Technology and Management' ),
     new Department(2, 'Computer Science' )
  ];
}