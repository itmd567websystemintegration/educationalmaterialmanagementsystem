import { Component, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { AddLabRoutingModule } from './add-lab-routing.module';
import { AddLabComponent } from './add-lab.component';
import { DepartmentListComponent } from './department-list.component';
import { PageHeaderModule } from './../../../../shared';

@NgModule({
    imports: [
        CommonModule, 
        FormsModule,
        AddLabRoutingModule,
        PageHeaderModule,
        
    ],
declarations: [AddLabComponent, DepartmentListComponent], 
bootstrap: [ DepartmentListComponent ]
})
export class AddLabModule { }
