import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddLabComponent } from './add-lab.component';

const routes: Routes = [
    { path: '', component: AddLabComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AddLabRoutingModule { }
