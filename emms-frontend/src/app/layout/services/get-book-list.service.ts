import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';

@Injectable()
export class GetBookListService {
  private idToken: string;

  constructor(private http:Http) { }

  getBookList() {
  	let url = "http://localhost:8181/item/bookList";
    let headers = new Headers ({
      'Content-Type': 'application/json'
    });
    return this.http.get(url, {headers: headers});
  }

  getBookListByLoggedInStudent() {
    this.idToken = localStorage.getItem('id_token');
    let studentDetails = {
      "id_token": this.idToken
    }

  	let url = "http://localhost:8181/item/bookListByLoggedInStudent";
    let tokenHeader = new Headers ({
      'Content-Type': 'application/json'
    });
    return this.http.post(url, JSON.stringify(studentDetails), { headers: tokenHeader });
  }
}
