import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
import {Notes} from '../models/notes';

@Injectable()
export class DeleteNotesService {

  constructor(private http:Http) { }

  deleteNotes(notesId: number) {
  	let url = "http://localhost:8181/item/notes/delete";
    
    let headers = new Headers ({
      'Content-Type': 'application/json'
    });

    return this.http.post(url, notesId, {headers: headers});
  }

}
