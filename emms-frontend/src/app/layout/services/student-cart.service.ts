import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { AppProperties } from '../constants/app-properties';

@Injectable()
export class StudentCartService {

	constructor(private http: Http) { }

	addBook(id: number) {
		let url = AppProperties.serverPath + "/cart/add/book";
		let selectedItemInfo = {
			"bookId": id,
			"id_token": localStorage.getItem('id_token')
		}
		let tokenHeader = new Headers({
			'Content-Type': 'application/json'
		});
		return this.http.post(url, JSON.stringify(selectedItemInfo), { headers: tokenHeader });
	}

	addNotes(id: number) {
		let url = AppProperties.serverPath + "/cart/add/notes";
		let selectedItemInfo = {
			"notesId": id,
			"id_token": localStorage.getItem('id_token')
		}
		let tokenHeader = new Headers({
			'Content-Type': 'application/json'
		});
		return this.http.post(url, JSON.stringify(selectedItemInfo), { headers: tokenHeader });
	}

	addLab(id: number) {
		let url = AppProperties.serverPath + "/cart/add/lab";
		let selectedItemInfo = {
			"labId": id,
			"id_token": localStorage.getItem('id_token')
		}
		let tokenHeader = new Headers({
			'Content-Type': 'application/json'
		});
		return this.http.post(url, JSON.stringify(selectedItemInfo), { headers: tokenHeader });
	}

	getSelectedBookList() {
		let url = AppProperties.serverPath + "/cart/getSelectedBookList";
		let requestMapper = {
			"id_token": localStorage.getItem('id_token')
		}
		let tokenHeader = new Headers({
			'Content-Type': 'application/json'
		});
		return this.http.post(url, JSON.stringify(requestMapper), { headers: tokenHeader });
	}

	getSelectedNotesList() {
		let url = AppProperties.serverPath + "/cart/getSelectedNotesList";
		let requestMapper = {
			"id_token": localStorage.getItem('id_token')
		}
		let tokenHeader = new Headers({
			'Content-Type': 'application/json'
		});
		return this.http.post(url, JSON.stringify(requestMapper), { headers: tokenHeader });
	}

	getSelectedLabList() {
		let url = AppProperties.serverPath + "/cart/getSelectedLabList";
		let requestMapper = {
			"id_token": localStorage.getItem('id_token')
		}
		let tokenHeader = new Headers({
			'Content-Type': 'application/json'
		});
		return this.http.post(url, JSON.stringify(requestMapper), { headers: tokenHeader });
	}

	getStudentCart() {
		let url = AppProperties.serverPath + "/cart/getStudentCart";
		let requestMapper = {
			"id_token": localStorage.getItem('id_token')
		}
		let tokenHeader = new Headers({
			'Content-Type': 'application/json'
		});
		return this.http.post(url, JSON.stringify(requestMapper), { headers: tokenHeader });
	}

	deleteSelectedItem(selectedBookId: number) {
		let url = AppProperties.serverPath + "/cart/remove/book";
		let studentDetails = {
			"id_token": localStorage.getItem('id_token'),
			"selectedBookId": selectedBookId
		}
		let tokenHeader = new Headers({
			'Content-Type': 'application/json'
		});
		return this.http.post(url, JSON.stringify(studentDetails), { headers: tokenHeader });
	}

	deleteSelectedNotesItem(selectedNotesId: number) {
		let url = AppProperties.serverPath + "/cart/remove/notes";
		let studentDetails = {
			"id_token": localStorage.getItem('id_token'),
			"selectedNotesId": selectedNotesId
		}
		let tokenHeader = new Headers({
			'Content-Type': 'application/json'
		});
		return this.http.post(url, JSON.stringify(studentDetails), { headers: tokenHeader });
	}

	deleteSelectedLabItem(selectedLabMaterialId: number) {
		let url = AppProperties.serverPath + "/cart/remove/labs";
		let studentDetails = {
			"id_token": localStorage.getItem('id_token'),
			"selectedLabMaterialId": selectedLabMaterialId
		}
		let tokenHeader = new Headers({
			'Content-Type': 'application/json'
		});
		return this.http.post(url, JSON.stringify(studentDetails), { headers: tokenHeader });
	}
}
