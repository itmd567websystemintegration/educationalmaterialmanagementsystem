import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { AppProperties } from '../constants/app-properties';
import { Student } from '../models/student';

@Injectable()
export class StudentService {
  private serverPath: string = AppProperties.serverPath;

  constructor(private http: Http) { }

  addStudent(userName: string, studentEmailId: string, givenName: string, familyName: string, idToken: string) {
    let url = this.serverPath + '/student/addStudent';
    console.log('addStudent: idToken :'+idToken);
    let studentDetails = {
      "id_token" : idToken,
      "userName": userName,
      "studentEmailId": studentEmailId,
      "familyName": familyName,
      "givenName": givenName
    }

    console.log(userName+' '+studentEmailId+' '+familyName+' '+givenName+' '+idToken);
    let tokenHeader = new Headers({
      'Content-Type': 'application/json'
    });

    return this.http.post(url, JSON.stringify(studentDetails), { headers: tokenHeader });
  }

  updateStudentDetails(student: Student, newPassword: string, currentPassword: string) {
    let url = this.serverPath + "/student/updateStudentDetails";
    let studentDetails = {
      "id_token" : localStorage.getItem('id_token'),
      "id": student.id,
      "firstName": student.firstName,
      "lastName": student.lastName,
      "username": student.userName,
      "currentPassword": currentPassword,
      "email": student.emailId,
      "newPassword": newPassword,
      "phone": student.phone,      
    };

    let tokenHeader = new Headers({
      'Content-Type': 'application/json'
    });
    return this.http.post(url, JSON.stringify(studentDetails), { headers: tokenHeader });
  }

  forgetPassword(studentEmailId: string) {
    let url = this.serverPath + '/student/forgetPassword';
    let studentDetails = {
      "studentEmailId": studentEmailId
    }
    let tokenHeader = new Headers({
      'Content-Type': 'application/json'
    });

    return this.http.post(url, JSON.stringify(studentDetails), { headers: tokenHeader });
  }

  getCurrentStudent(idToken: string) {
    let url = this.serverPath + '/student/getCurrentStudent';
    let studentDetails = {
      "id_token": idToken
    }
    let tokenHeader = new Headers({
      'Content-Type': 'application/json'
    });

    return this.http.post(url, JSON.stringify(studentDetails), { headers: tokenHeader });
  }
}
