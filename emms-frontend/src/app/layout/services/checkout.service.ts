import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { AppProperties } from '../constants/app-properties';
import { DeliveryAddress } from '../models/delivery-address';
import { BillingAddress } from '../models/billing-address';
import { Payment } from '../models/payment';


@Injectable()
export class StudentCheckoutService {

	constructor(private http: Http) { }

	checkout(
		deliveryAddress: DeliveryAddress,
		billingAddress: BillingAddress,
		payment: Payment,
		deliveryMethod: string
	) {
		console.log("deliveryMethod: " + deliveryMethod);
		let url = AppProperties.serverPath + "/checkout/placeOrder";
		let order = {
			"deliveryAddress": deliveryAddress,
			"billingAddress": billingAddress,
			"payment": payment,
			"deliveryMethod": deliveryMethod,
			"idToken": localStorage.getItem('id_token')
		}
		console.log('deliveryAddress'+JSON.stringify(deliveryAddress));
		console.log('billingAddress'+JSON.stringify(billingAddress));
		let tokenHeader = new Headers({
			'Content-Type': 'application/json'
		});
		return this.http.post(url, order, { headers: tokenHeader });
	}

	getStudentOrder() {
		let url = AppProperties.serverPath + "/checkout/getStudentOrder";

		let tokenHeader = new Headers({
			'Content-Type': 'application/json'
		});
		return this.http.get(url, { headers: tokenHeader });

	}

}
