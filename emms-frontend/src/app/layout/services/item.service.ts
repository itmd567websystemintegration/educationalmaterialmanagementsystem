import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { AppProperties } from '../constants/app-properties';

@Injectable()
export class ItemService {

	constructor(private http: Http) { }

	getItemList() {
		let url = AppProperties.serverPath + "/item/itemList";

		let tokenHeader = new Headers({
			'Content-Type': 'application/json'
		});
		return this.http.get(url, { headers: tokenHeader });
	}

	getItem(id: number) {
		let url = AppProperties.serverPath + "/item/" + id;

		let tokenHeader = new Headers({
			'Content-Type': 'application/json'
		});
		return this.http.get(url, { headers: tokenHeader });
	}

	getNotes(id: number) {
		let url = AppProperties.serverPath + "/item/notes/" + id;

		let tokenHeader = new Headers({
			'Content-Type': 'application/json'
		});
		return this.http.get(url, { headers: tokenHeader });
	}

	getLabs(id: number) {
		let url = AppProperties.serverPath + "/item/labs/" + id;

		let tokenHeader = new Headers({
			'Content-Type': 'application/json'
		});
		return this.http.get(url, { headers: tokenHeader });
	}

	findItem(keyword: string) {
		let url = AppProperties.serverPath + "/item/findItem";

		let tokenHeader = new Headers({
			'Content-Type': 'application/json'
		});
		return this.http.post(url, keyword, { headers: tokenHeader });
	}
}