import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { AppProperties } from '../constants/app-properties';
import { StudentOrder } from '../models/student-order';

@Injectable()
export class StudentOrderService {

	constructor(private http: Http) { }

	getStudentOrder(id: number) {
		let url = AppProperties.serverPath + "/order/studentOrder/"+id;

		let tokenHeader = new Headers({
			'Content-Type': 'application/json'
		});
		return this.http.get(url, { headers: tokenHeader });
	}

	getStudentOrderList() {
		let url = AppProperties.serverPath + "/order/getStudentOrderHistory";
		let studentDetails = {
			"idToken": localStorage.getItem('id_token')
		}
		let tokenHeader = new Headers({
			'Content-Type': 'application/json'
		});
		return this.http.post(url, JSON.stringify(studentDetails), { headers: tokenHeader });
	}

	getAllStudentOrderList() {
		let url = AppProperties.serverPath + "/order/getStudentOrderHistory";
		let studentDetails = {
			"idToken": localStorage.getItem('id_token')
		}
		let tokenHeader = new Headers({
			'Content-Type': 'application/json'
		});
		return this.http.post(url, JSON.stringify(studentDetails), { headers: tokenHeader });
	}
}
