import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
import {LabMaterial} from '../models/lab';

@Injectable()
export class EditLabService {

  constructor(private http:Http) { }

  editLab(lab:LabMaterial) {
  	let url = "http://localhost:8181/item/labs/update";
    
    let headers = new Headers ({
      'Content-Type': 'application/json'
    });

    return this.http.post(url, JSON.stringify(lab), {headers: headers});
  }
}
