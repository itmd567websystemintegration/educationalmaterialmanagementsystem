import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
import {LabMaterial} from '../models/lab';

@Injectable()
export class DeleteLabService {

  constructor(private http:Http) { }

  deleteLab(labId: number) {
  	let url = "http://localhost:8181/item/lab/delete";
    
    let headers = new Headers ({
      'Content-Type': 'application/json'
    });

    return this.http.post(url, labId, {headers: headers});
  }

}
