import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
import { Course } from '../models/course';
@Injectable()
export class GetCourseListService {

  constructor(private http:Http) { }

  getCourseList() {
  	let url = "http://localhost:8181/course/courseList";
    let headers = new Headers ({
      'Content-Type': 'application/json'
    });

    return this.http.get(url, {headers: headers});
  }

  addCourse(course:Course) {
    console.log("addCourse(): "+course);
  	let url = "http://localhost:8181/course/addCourse";
    
    let headers = new Headers ({
      'Content-Type': 'application/json'
    });

    return this.http.post(url, JSON.stringify(course), {headers: headers});
  }

}
