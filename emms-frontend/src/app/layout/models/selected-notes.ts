import {Notes} from './notes';
import {StudentCart} from './student-cart';

export class SelectedNotes {
	public id: number;
	public notes: Notes;
	public shoppingCart: StudentCart
	public toUpdate:boolean;
}
