import {Book} from './book';
import {StudentCart} from './student-cart';

export class SelectedBook {
	public id: number;
	public book: Book;
	public shoppingCart: StudentCart
	public toUpdate:boolean;
}
