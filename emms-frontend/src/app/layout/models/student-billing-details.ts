export class StudentBillingDetails {
	public id: number;
	public studentBillingName: string;
	public studentBillingStreet1: string;
	public studentBillingStreet2: string;
	public studentBillingCity: string;
    public studentBillingState: string;
    public studentBillingPostalcode: string;
	public studentBillingCountry: string;
}
