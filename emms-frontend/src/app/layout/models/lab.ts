import {Department} from './department';
import {Course} from './course';
import {Student} from './student';

export class LabMaterial {
	public id: number;
	public title: string;
	public professor: string;
	public itemdetails1: string;
	public itemdetails2: string;
    public itemdetails3: string;
    public itemdetails4: string;
	public itemdetails5: string;
	public price: number;
    public description: string;
    public student: Student;
    public department: Department;
    public course: Course;
}
