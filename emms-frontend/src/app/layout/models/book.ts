import {Department} from './department';
import {Course} from './course';
import {Student} from './student';


export class Book {
	public id: number;
    public student: Student;
    public department: Department;
    public course: Course;
	public title: string;
	public author: string;
	public publisher: string;
	public edition: string;
	public price: number;
	public description: string;
}
