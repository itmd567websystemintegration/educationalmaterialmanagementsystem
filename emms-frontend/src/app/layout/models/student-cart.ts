import {Student} from './student';

export class StudentCart {
	public id: number;
	public total: number;
	public student: Student;
}
