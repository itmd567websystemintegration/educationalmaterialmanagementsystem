import {SelectedBook} from './selected-book';
import {SelectedNotes} from '../models/selected-notes';
import {SelectedLabMaterial} from '../models/selected-lab';

export class StudentOrder {

	public id: number;
	public orderDate: string;
	public deliveryDate: string;
	public deliveryMethod: string;
	public orderStatus: string;
	public orderTotal: number;
	public studentPaymentAuthorizationId: string;
	public studentPaymentStatus: string;
	public selectedBookList: SelectedBook[];
	public selectedNotesList: SelectedNotes[];
	public selectedLabMaterialList: SelectedLabMaterial[];
}
