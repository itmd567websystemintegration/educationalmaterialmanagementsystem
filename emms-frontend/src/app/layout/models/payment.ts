import {BillingAddress} from './billing-address';

export class Payment {
	public id: number;
	public cardType: string;
    public cardNumber: string;
    public cardHolderName: string;
    public cvc: number;
	public expiryMonth: string;
    public expiryYear: string;
    public billingAddress: BillingAddress;
}
