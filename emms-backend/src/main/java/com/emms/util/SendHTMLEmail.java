package com.emms.util;


import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.emms.model.Student;
import com.emms.model.StudentOrder;

/**
 * This class is used to send appropriate emails to the users.
 * * @author Harshal Deolekar
 */
public class SendHTMLEmail {

    // Recipient's email ID needs to be mentioned.
    String to;
    MimeMessage message;
    public static final int STUDENT_REG = 1;
    public static final int ORDER_PLACED = 2;
    
    private static Properties properties = System.getProperties();

    public SendHTMLEmail() throws AddressException, MessagingException {

        // Setup mail server
        properties.setProperty("mail.transport.protocol", "smtp");

        properties.put("mail.smtp.host", "smtp.gmail.com");

        properties.put("mail.smtp.socketFactory.port", "587");

        properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");

        properties.put("mail.smtp.auth", "true");

        properties.put("mail.smtp.port", "587");

        properties.put("mail.smtp.starttls.enable", "true");

        // creates a valid session by authenticating user.
        Session session = Session.getDefaultInstance(properties, new javax.mail.Authenticator() {
  			protected PasswordAuthentication getPasswordAuthentication() {  			
  				return new PasswordAuthentication("emms.integration@gmail.com", "itmd567@Emms");
  			}
  		});
        message = new MimeMessage(session);
        message.setFrom(new InternetAddress("emms.integration@gmail.com"));
	}

	/**
	 * This method sends an email.
	 * @param to email recipient.
	 * @param emailMessage email body/message to be sent.
	 * @param messageType type of email, e.g. user registration, event regisr
	 * @throws MessagingException
	 */
	public void sendMessage(String to, String emailMessage, int messageType) throws MessagingException {
		try {
			switch (messageType) {
			case 1:
				message.setSubject("Registration Successful!");
				break;
			case 2:
				message.setSubject("Your Order is placed successfully");
				break;
			default:
				break;
			}

			// Set  the header.
			message.setRecipient(Message.RecipientType.TO, new InternetAddress(to));

			// sets content type
			message.setContent(emailMessage, "text/html");

			// Send message
			Transport.send(message);
			System.out.println("Sent message successfully....");
		} catch (MessagingException mex) {
			mex.printStackTrace();
		}
	}

	/**
	 * This method returns the email html message body.
	 * @param obj Object used for creating email.
	 * @param messageType
	 * @return
	 */
	public String getMessage(Object obj, int messageType){
		String message = "";
		switch(messageType){
		case 1:
			Student newUser = (Student) obj;
			message = "<h3>Dear,"+newUser.getFirstName()+"</h3><br>"
					+ "<h4>Congratualations! you have been successfully registered with Educational Material Management System<br>";


			message="<BODY BGCOLOR='FFFFFF'>"
					+ "<HR><H3>Login Successful!!!</H3>"
					+ "<h3>Dear,"+newUser.getFirstName()+"</h3><br>"
					+ "<H3>Welcome on-board</H4>"
					+ "<p>Your login credentials are validated</p>"
					+ "username : "+newUser.getEmailId()+"</br>"
					+ "<HR><br>Regards,</br><br>Educational Material Management Team</br><br>3300 S Federal St,Chicago, IL - 60616 | (510)-640-6361 </br></HR></BODY>";

			break;
		case 2:
			StudentOrder newOrder = (StudentOrder) obj;
			message="<BODY BGCOLOR=''><HR>"+
					"<H3>Dear,"+newOrder.getStudent().getFirstName()+"</H3><br>"+
			"<H4><font color =Black>Order Placed Successfully!!!</font></H4>"+
			"<H4><font color =Black>You will receive your order by: </font>"+newOrder.getDeliveryDate()+"</H4>"+
			"<p><font color =Black>Please carry your Identity proof</font></p>"+
			"<br><font color =Black><em><B>Order Confirmation Number: </B>"+newOrder.getStudentPaymentAuthorizationId()+"</em></font></br>"+			
			"<HR>"+
			"<br><font color =Black>Regards,</font></br>"+
			"<br><font color =Black>Educational Material Management Team</font></br>"+
			"<br><font color =Black>3300 S Federal St,Chicago, IL - 60616 | (510)-640-6361 </font></br>"+
			"</HR>"+
			"</BODY>";
			break;
		}
		return message;
	}
}