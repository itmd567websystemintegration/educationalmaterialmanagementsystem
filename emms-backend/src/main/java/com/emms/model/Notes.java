/**
 * 
 */
package com.emms.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.web.multipart.MultipartFile;

/**
 * @author Harshal
 *
 */
@Entity
@Table(name="NOTES")
public class Notes implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4485376760750673348L;	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO )
    private Long id;
	private String title;
	private String professor;
	private String notesdetails1;
	private String notesdetails2;
	private String notesdetails3;
	@Transient
	private MultipartFile notesImage;
	@ManyToOne
	@JoinColumn(name = "student_id")
	private Student student;
	@ManyToOne
	@JoinColumn(name = "department_id")
	private Department department;
	@ManyToOne
	@JoinColumn(name = "course_id")
	private Course course;
	private BigDecimal price;
	@Column(columnDefinition = "text")
	private String description;
	
	
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the professor
	 */
	public String getProfessor() {
		return professor;
	}

	/**
	 * @param professor the professor to set
	 */
	public void setProfessor(String professor) {
		this.professor = professor;
	}

	/**
	 * @return the notesdetails1
	 */
	public String getNotesdetails1() {
		return notesdetails1;
	}

	/**
	 * @param notesdetails1 the notesdetails1 to set
	 */
	public void setNotesdetails1(String notedetails1) {
		this.notesdetails1 = notedetails1;
	}

	/**
	 * @return the notesdetails2
	 */
	public String getNotesdetails2() {
		return notesdetails2;
	}

	/**
	 * @param notesdetails2 the notesdetails2 to set
	 */
	public void setNotesdetails2(String notedetails2) {
		this.notesdetails2 = notedetails2;
	}

	/**
	 * @return the notesdetails3
	 */
	public String getNotesdetails3() {
		return notesdetails3;
	}

	/**
	 * @param notesdetails3 the notesdetails3 to set
	 */
	public void setNotesdetails3(String notedetails3) {
		this.notesdetails3 = notedetails3;
	}

	/**
	 * @return the notesImage
	 */
	public MultipartFile getNotesImage() {
		return notesImage;
	}

	/**
	 * @param notesImage the notesImage to set
	 */
	public void setNotesImage(MultipartFile notesImage) {
		this.notesImage = notesImage;
	}

	/**
	 * @return the student
	 */
	public Student getStudent() {
		return student;
	}

	/**
	 * @param student the student to set
	 */
	public void setStudent(Student student) {
		this.student = student;
	}

	/**
	 * @return the department
	 */
	public Department getDepartment() {
		return department;
	}

	/**
	 * @param department the department to set
	 */
	public void setDepartment(Department department) {
		this.department = department;
	}

	/**
	 * @return the course
	 */
	public Course getCourse() {
		return course;
	}

	/**
	 * @param course the course to set
	 */
	public void setCourse(Course course) {
		this.course = course;
	}

	/**
	 * @return the price
	 */
	public BigDecimal getPrice() {
		return price;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Notes [id=" + id + ", professor=" + professor + ", notesdetails1=" + notesdetails1 + ", notesdetails2="
				+ notesdetails2 + ", notesdetails3=" + notesdetails3 + ", notesImage=" + notesImage + ", student="
				+ student + ", department=" + department + ", course=" + course + ", price=" + price + "]";
	}
}
