/**
 * 
 */
package com.emms.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author Harshal
 *
 */
@Entity
@Table(name="BILLING_ADDRESS")
public class BillingAddress implements Serializable{
	private static final long serialVersionUID = -3817601740321088903L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String billingAddressName;
	private String billingAddressStreet1;
	private String billingAddressStreet2;
	private String billingAddressCity;
	private String billingAddressState;
	private String billingAddressCountry;
	private String billingAddressPostalcode;
	@OneToOne
	@JsonIgnore
    private StudentOrder studentOrder;
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the billingAddressName
	 */
	public String getBillingAddressName() {
		return billingAddressName;
	}
	/**
	 * @param billingAddressName the billingAddressName to set
	 */
	public void setBillingAddressName(String billingAddressName) {
		this.billingAddressName = billingAddressName;
	}
	/**
	 * @return the billingAddressStreet1
	 */
	public String getBillingAddressStreet1() {
		return billingAddressStreet1;
	}
	/**
	 * @param billingAddressStreet1 the billingAddressStreet1 to set
	 */
	public void setBillingAddressStreet1(String billingAddressStreet1) {
		this.billingAddressStreet1 = billingAddressStreet1;
	}
	/**
	 * @return the billingAddressStreet2
	 */
	public String getBillingAddressStreet2() {
		return billingAddressStreet2;
	}
	/**
	 * @param billingAddressStreet2 the billingAddressStreet2 to set
	 */
	public void setBillingAddressStreet2(String billingAddressStreet2) {
		this.billingAddressStreet2 = billingAddressStreet2;
	}
	/**
	 * @return the billingAddressCity
	 */
	public String getBillingAddressCity() {
		return billingAddressCity;
	}
	/**
	 * @param billingAddressCity the billingAddressCity to set
	 */
	public void setBillingAddressCity(String billingAddressCity) {
		this.billingAddressCity = billingAddressCity;
	}
	/**
	 * @return the billingAddressState
	 */
	public String getBillingAddressState() {
		return billingAddressState;
	}
	/**
	 * @param billingAddressState the billingAddressState to set
	 */
	public void setBillingAddressState(String billingAddressState) {
		this.billingAddressState = billingAddressState;
	}
	/**
	 * @return the billingAddressCountry
	 */
	public String getBillingAddressCountry() {
		return billingAddressCountry;
	}
	/**
	 * @param billingAddressCountry the billingAddressCountry to set
	 */
	public void setBillingAddressCountry(String billingAddressCountry) {
		this.billingAddressCountry = billingAddressCountry;
	}
	/**
	 * @return the billingAddressPostalcode
	 */
	public String getBillingAddressPostalcode() {
		return billingAddressPostalcode;
	}
	/**
	 * @param billingAddressPostalcode the billingAddressPostalcode to set
	 */
	public void setBillingAddressPostalcode(String billingAddressPostalcode) {
		this.billingAddressPostalcode = billingAddressPostalcode;
	}
	/**
	 * @return the studentOrder
	 */
	public StudentOrder getStudentOrder() {
		return studentOrder;
	}
	/**
	 * @param studentOrder the studentOrder to set
	 */
	public void setStudentOrder(StudentOrder studentOrder) {
		this.studentOrder = studentOrder;
	}	
}
