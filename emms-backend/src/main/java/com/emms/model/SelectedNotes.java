/**
 * 
 */
package com.emms.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author Harshal
 *
 */
@Entity
@Table(name="SELECTED_NOTES")
public class SelectedNotes implements Serializable {
	private static final long serialVersionUID = -611176252369932655L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@OneToOne
	private Notes notes;

	@ManyToOne
	@JoinColumn(name = "student_cart_id")
	private StudentCart studentCart;

	@ManyToOne
	@JoinColumn(name = "order_id")
	@JsonIgnore
	private StudentOrder order;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public StudentOrder getOrder() {
		return order;
	}

	public void setOrder(StudentOrder order) {
		this.order = order;
	}
	
	/**
	 * @return the notes
	 */
	public Notes getNotes() {
		return notes;
	}

	/**
	 * @param notes the notes to set
	 */
	public void setNotes(Notes notes) {
		this.notes = notes;
	}

	/**
	 * @return the studentCart
	 */
	public StudentCart getStudentCart() {
		return studentCart;
	}

	/**
	 * @param studentCart the studentCart to set
	 */
	public void setStudentCart(StudentCart studentCart) {
		this.studentCart = studentCart;
	}			
}
