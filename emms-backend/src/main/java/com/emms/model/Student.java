/**
 * 
 */
package com.emms.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.emms.security.Authority;
import com.emms.security.StudentRole;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author Harshal
 *
 */
@Entity
@Table(name="STUDENT")
public class Student implements UserDetails, Serializable {
	private static final long serialVersionUID = -4555943394550320400L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "CampusWID", updatable = false, nullable = false)
	private Long id;
	private String firstName;
	private String lastName;
	private String userName;
	private String password;
	private String emailId;
	private String phone;
	@Column(columnDefinition = "text")
	private String accessToken;
	private boolean isEnabled;

	
	
	@OneToMany(mappedBy = "student", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JsonIgnore
	private Set<StudentRole> studentRole = new HashSet<StudentRole>();

	@OneToMany(mappedBy = "student", cascade = CascadeType.ALL)
	@JsonIgnore
	private List<StudentOrder> orderList;

	@OneToMany(mappedBy = "student", cascade = CascadeType.ALL)
	@JsonIgnore
	private List<StudentDeliveryDetails> studentDeliveryDetailsList;
	
	@OneToMany( mappedBy = "student", cascade=CascadeType.ALL)
	@JsonIgnore
	private List<StudentPaymentDetails> studentPaymentDetailsList;

	@OneToOne(cascade = CascadeType.ALL)
	@JsonIgnore
	private StudentCart studentCart;
	
	@OneToMany(mappedBy = "student", cascade = CascadeType.ALL)
	@JsonIgnore
	private List<Book> bookList;
	
	@OneToMany(mappedBy = "student", cascade = CascadeType.ALL)
	@JsonIgnore
	private List<Notes> notesList;
	
	@OneToMany(mappedBy = "student", cascade = CascadeType.ALL)
	@JsonIgnore
	private List<LabMaterial> labList;


	/**
	 * @return the bookList
	 */
	public List<Book> getBookList() {
		return bookList;
	}

	/**
	 * @param bookList the bookList to set
	 */
	public void setBookList(List<Book> bookList) {
		this.bookList = bookList;
	}
	
	/**
	 * @return the notesList
	 */
	public List<Notes> getNotesList() {
		return notesList;
	}

	/**
	 * @param notesList the notesList to set
	 */
	public void setNotesList(List<Notes> notesList) {
		this.notesList = notesList;
	}

	
	/**
	 * @return the labList
	 */
	public List<LabMaterial> getLabList() {
		return labList;
	}

	/**
	 * @param labList the labList to set
	 */
	public void setLabList(List<LabMaterial> labList) {
		this.labList = labList;
	}

	/**
	 * @return the accessToken
	 */
	public String getAccessToken() {
		return accessToken;
	}

	/**
	 * @param accessToken the accessToken to set
	 */
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	/**
	 * @return the studentRole
	 */
	public Set<StudentRole> getStudentRole() {
		return studentRole;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the emailId
	 */
	public String getEmailId() {
		return emailId;
	}

	/**
	 * @param emailId
	 *            the emailId to set
	 */
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @param phone
	 *            the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	/**
	 * @return the studentDeliveryDetailsList
	 */
	public List<StudentDeliveryDetails> getStudentDeliveryDetailsList() {
		return studentDeliveryDetailsList;
	}

	/**
	 * @param studentDeliveryDetailsList the studentDeliveryDetailsList to set
	 */
	public void setStudentDeliveryDetailsList(List<StudentDeliveryDetails> studentDeliveryDetailsList) {
		this.studentDeliveryDetailsList = studentDeliveryDetailsList;
	}

	/**
	 * @return the studentPaymentDetailsList
	 */
	public List<StudentPaymentDetails> getStudentPaymentDetailsList() {
		return studentPaymentDetailsList;
	}

	/**
	 * @param studentPaymentDetailsList the studentPaymentDetailsList to set
	 */
	public void setStudentPaymentDetailsList(List<StudentPaymentDetails> studentPaymentDetailsList) {
		this.studentPaymentDetailsList = studentPaymentDetailsList;
	}

	/**
	 * @param isEnabled
	 *            the isEnabled to set
	 */
	public void setEnabled(boolean isEnabled) {
		this.isEnabled = isEnabled;
	}

	
	/**
	 * @return the studentRole
	 */
	public Set<StudentRole> getStudentRoles() {
		return studentRole;
	}

	/**
	 * @param studentRole the studentRole to set
	 */
	public void setStudentRole(Set<StudentRole> studentRole) {
		this.studentRole = studentRole;
	}

	/**
	 * @return the orderList
	 */
	public List<StudentOrder> getOrderList() {
		return orderList;
	}

	/**
	 * @param orderList
	 *            the orderList to set
	 */
	public void setOrderList(List<StudentOrder> orderList) {
		this.orderList = orderList;
	}

	

	/**
	 * @return the studentCart
	 */
	public StudentCart getStudentCart() {
		return studentCart;
	}

	/**
	 * @param studentTrolley
	 *            the studentCart to set
	 */
	public void setStudentCart(StudentCart studentCart) {
		this.studentCart = studentCart;
	}

	/**
	 * @param userName
	 *            the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	@JsonIgnore
	public Collection<? extends GrantedAuthority> getAuthorities() {
		Set<GrantedAuthority> authorities = new HashSet<>();
		studentRole.forEach(ur -> authorities.add(new Authority(ur.getRole().getName())));
		return authorities;
	}

	@Override
	public String getPassword() {
		return password;
	}
	
	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return isEnabled;
	}

	@Override
	public String getUsername() {
		return userName;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Student [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", userName=" + userName
				+ ", password=" + password + ", emailId=" + emailId + ", phone=" + phone + ", accessToken="
				+ accessToken + ", isEnabled=" + isEnabled + ", studentRole=" + studentRole + ", orderList=" + orderList
				+ ", studentDeliveryDetailsList=" + studentDeliveryDetailsList + ", studentPaymentDetailsList="
				+ studentPaymentDetailsList + ", studentCart=" + studentCart + ", bookList=" + bookList + "]";
	}
	
	
}
