/**
 * 
 */
package com.emms.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * @author Harshal
 *
 */
@Entity
@Table(name="STUDENT_BILLING_DETAILS")
public class StudentBillingDetails implements Serializable {
	private static final long serialVersionUID = 9091055770311405002L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@OneToOne(cascade = CascadeType.ALL)
	private StudentPaymentDetails studentPaymentDetails;
	private String studentBillingName;
	private String studentBillingStreet1;
	private String studentBillingStreet2;
	private String studentBillingCity;
	private String studentBillingState;
	private String studentBillingCountry;
	private String studentBillingZipcode;
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the studentPaymentDetails
	 */
	public StudentPaymentDetails getStudentPaymentDetails() {
		return studentPaymentDetails;
	}
	/**
	 * @param studentPaymentDetails the studentPaymentDetails to set
	 */
	public void setStudentPaymentDetails(StudentPaymentDetails studentPaymentDetails) {
		this.studentPaymentDetails = studentPaymentDetails;
	}
	/**
	 * @return the studentBillingName
	 */
	public String getStudentBillingName() {
		return studentBillingName;
	}
	/**
	 * @param studentBillingName the studentBillingName to set
	 */
	public void setStudentBillingName(String studentBillingName) {
		this.studentBillingName = studentBillingName;
	}
	/**
	 * @return the studentBillingStreet1
	 */
	public String getStudentBillingStreet1() {
		return studentBillingStreet1;
	}
	/**
	 * @param studentBillingStreet1 the studentBillingStreet1 to set
	 */
	public void setStudentBillingStreet1(String studentBillingStreet1) {
		this.studentBillingStreet1 = studentBillingStreet1;
	}
	/**
	 * @return the studentBillingStreet2
	 */
	public String getStudentBillingStreet2() {
		return studentBillingStreet2;
	}
	/**
	 * @param studentBillingStreet2 the studentBillingStreet2 to set
	 */
	public void setStudentBillingStreet2(String studentBillingStreet2) {
		this.studentBillingStreet2 = studentBillingStreet2;
	}
	/**
	 * @return the studentBillingCity
	 */
	public String getStudentBillingCity() {
		return studentBillingCity;
	}
	/**
	 * @param studentBillingCity the studentBillingCity to set
	 */
	public void setStudentBillingCity(String studentBillingCity) {
		this.studentBillingCity = studentBillingCity;
	}
	/**
	 * @return the studentBillingState
	 */
	public String getStudentBillingState() {
		return studentBillingState;
	}
	/**
	 * @param studentBillingState the studentBillingState to set
	 */
	public void setStudentBillingState(String studentBillingState) {
		this.studentBillingState = studentBillingState;
	}
	/**
	 * @return the studentBillingCountry
	 */
	public String getStudentBillingCountry() {
		return studentBillingCountry;
	}
	/**
	 * @param studentBillingCountry the studentBillingCountry to set
	 */
	public void setStudentBillingCountry(String studentBillingCountry) {
		this.studentBillingCountry = studentBillingCountry;
	}
	/**
	 * @return the studentBillingZipcode
	 */
	public String getStudentBillingZipcode() {
		return studentBillingZipcode;
	}
	/**
	 * @param studentBillingZipcode the studentBillingZipcode to set
	 */
	public void setStudentBillingZipcode(String studentBillingZipcode) {
		this.studentBillingZipcode = studentBillingZipcode;
	}
	
}
