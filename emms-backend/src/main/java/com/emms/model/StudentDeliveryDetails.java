/**
 * 
 */
package com.emms.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author Harshal
 *
 */

@Entity
@Table(name="STUDENT_DELIVERY_DETAILS")
public class StudentDeliveryDetails implements Serializable {
	private static final long serialVersionUID = -3989239867028519863L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String studentShippingName;
	private String studentShippingStreet1;
	private String studentShippingStreet2;
	private String studentShippingCity;
	private String studentShippingState;
	private String studentShippingCountry;
	private String studentShippingZipcode;
	private Boolean studentShippingDefault;
	@ManyToOne
	@JoinColumn(name = "student_id")
	private Student student;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	
	/**
	 * @return the studentShippingName
	 */
	public String getStudentShippingName() {
		return studentShippingName;
	}

	/**
	 * @param studentShippingName the studentShippingName to set
	 */
	public void setStudentShippingName(String studentShippingName) {
		this.studentShippingName = studentShippingName;
	}

	/**
	 * @return the studentShippingStreet1
	 */
	public String getStudentShippingStreet1() {
		return studentShippingStreet1;
	}

	/**
	 * @param studentShippingStreet1 the studentShippingStreet1 to set
	 */
	public void setStudentShippingStreet1(String studentShippingStreet1) {
		this.studentShippingStreet1 = studentShippingStreet1;
	}

	/**
	 * @return the studentShippingStreet2
	 */
	public String getStudentShippingStreet2() {
		return studentShippingStreet2;
	}

	/**
	 * @param studentShippingStreet2 the studentShippingStreet2 to set
	 */
	public void setStudentShippingStreet2(String studentShippingStreet2) {
		this.studentShippingStreet2 = studentShippingStreet2;
	}

	/**
	 * @return the studentShippingCity
	 */
	public String getStudentShippingCity() {
		return studentShippingCity;
	}

	/**
	 * @param studentShippingCity the studentShippingCity to set
	 */
	public void setStudentShippingCity(String studentShippingCity) {
		this.studentShippingCity = studentShippingCity;
	}

	/**
	 * @return the studentShippingState
	 */
	public String getStudentShippingState() {
		return studentShippingState;
	}

	/**
	 * @param studentShippingState the studentShippingState to set
	 */
	public void setStudentShippingState(String studentShippingState) {
		this.studentShippingState = studentShippingState;
	}

	/**
	 * @return the studentShippingCountry
	 */
	public String getStudentShippingCountry() {
		return studentShippingCountry;
	}

	/**
	 * @param studentShippingCountry the studentShippingCountry to set
	 */
	public void setStudentShippingCountry(String studentShippingCountry) {
		this.studentShippingCountry = studentShippingCountry;
	}

	/**
	 * @return the studentShippingZipcode
	 */
	public String getStudentShippingZipcode() {
		return studentShippingZipcode;
	}

	/**
	 * @param studentShippingZipcode the studentShippingZipcode to set
	 */
	public void setStudentShippingZipcode(String studentShippingZipcode) {
		this.studentShippingZipcode = studentShippingZipcode;
	}

	/**
	 * @return the studentShippingDefault
	 */
	public Boolean getStudentShippingDefault() {
		return studentShippingDefault;
	}

	/**
	 * @param studentShippingDefault the studentShippingDefault to set
	 */
	public void setStudentShippingDefault(Boolean studentShippingDefault) {
		this.studentShippingDefault = studentShippingDefault;
	}

	/**
	 * @return the student
	 */
	public Student getStudent() {
		return student;
	}

	/**
	 * @param student
	 *            the student to set
	 */
	public void setStudent(Student student) {
		this.student = student;
	}
}
