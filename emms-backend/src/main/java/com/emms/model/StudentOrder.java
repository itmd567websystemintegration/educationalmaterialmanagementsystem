/**
 * 
 */
package com.emms.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author Harshal
 *
 */
@Entity
@Table(name = "STUDENT_ORDER")
public class StudentOrder implements Serializable {
	private static final long serialVersionUID = 7430604595096509347L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String orderStatus;
	private String deliveryMethod;
	private Date orderDate;
	private Date deliveryDate;
	private BigDecimal orderTotal;
	private String studentPaymentAuthorizationId;
	private String studentPaymentStatus;

	@OneToOne(cascade = CascadeType.ALL)
	private BillingAddress studentBillingAddress;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "student_id")
	private Student student;
	
	@OneToOne(cascade = CascadeType.ALL)
	private DeliveryAddress deliveryAddress;
	
	@OneToMany(mappedBy = "order", cascade = CascadeType.ALL)
	private List<SelectedBook> selectedBookList;
	
	@OneToMany(mappedBy = "order", cascade = CascadeType.ALL)
	private List<SelectedNotes> selectedNotesList;
	
	@OneToMany(mappedBy = "order", cascade = CascadeType.ALL)
	private List<SelectedLabMaterial> selectedLabMaterialList;
	
	@OneToOne(cascade = CascadeType.ALL)
	private Payment payment;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the orderDate
	 */
	public Date getOrderDate() {
		return orderDate;
	}

	/**
	 * @param orderDate the orderDate to set
	 */
	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	/**
	 * @return the deliveryDate
	 */
	public Date getDeliveryDate() {
		return deliveryDate;
	}

	/**
	 * @param deliveryDate the deliveryDate to set
	 */
	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	/**
	 * @return the orderStatus
	 */
	public String getOrderStatus() {
		return orderStatus;
	}

	/**
	 * @param orderStatus the orderStatus to set
	 */
	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	/**
	 * @return the deliveryMethod
	 */
	public String getDeliveryMethod() {
		return deliveryMethod;
	}

	/**
	 * @param deliveryMethod the deliveryMethod to set
	 */
	public void setDeliveryMethod(String deliveryMethod) {
		this.deliveryMethod = deliveryMethod;
	}

	/**
	 * @return the orderTotal
	 */
	public BigDecimal getOrderTotal() {
		return orderTotal;
	}

	/**
	 * @param orderTotal the orderTotal to set
	 */
	public void setOrderTotal(BigDecimal orderTotal) {
		this.orderTotal = orderTotal;
	}

	/**
	 * @return the studentBillingAddress
	 */
	public BillingAddress getStudentBillingAddress() {
		return studentBillingAddress;
	}

	/**
	 * @param studentBillingAddress the studentBillingAddress to set
	 */
	public void setStudentBillingAddress(BillingAddress studentBillingAddress) {
		this.studentBillingAddress = studentBillingAddress;
	}

	

	/**
	 * @return the selectedBookList
	 */
	public List<SelectedBook> getSelectedBookList() {
		return selectedBookList;
	}

	/**
	 * @param selectedBookList the selectedBookList to set
	 */
	public void setSelectedBookList(List<SelectedBook> selectedBookList) {
		this.selectedBookList = selectedBookList;
	}

	/**
	 * @return the selectedNotesList
	 */
	public List<SelectedNotes> getSelectedNotesList() {
		return selectedNotesList;
	}

	/**
	 * @param selectedNotesList the selectedNotesList to set
	 */
	public void setSelectedNotesList(List<SelectedNotes> selectedNotesList) {
		this.selectedNotesList = selectedNotesList;
	}
	
	/**
	 * @return the selectedLabMaterialList
	 */
	public List<SelectedLabMaterial> getSelectedLabMaterialList() {
		return selectedLabMaterialList;
	}

	/**
	 * @param selectedLabMaterialList the selectedLabMaterialList to set
	 */
	public void setSelectedLabMaterialList(List<SelectedLabMaterial> selectedLabMaterialList) {
		this.selectedLabMaterialList = selectedLabMaterialList;
	}

	/**
	 * @return the student
	 */
	public Student getStudent() {
		return student;
	}

	/**
	 * @param student the student to set
	 */
	public void setStudent(Student student) {
		this.student = student;
	}

	/**
	 * @return the payment
	 */
	public Payment getPayment() {
		return payment;
	}

	/**
	 * @param payment the payment to set
	 */
	public void setPayment(Payment payment) {
		this.payment = payment;
	}

	/**
	 * @return the deliveryAddress
	 */
	public DeliveryAddress getDeliveryAddress() {
		return deliveryAddress;
	}

	/**
	 * @param deliveryAddress the deliveryAddress to set
	 */
	public void setDeliveryAddress(DeliveryAddress deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}

	/**
	 * @return the studentPaymentAuthorizationId
	 */
	public String getStudentPaymentAuthorizationId() {
		return studentPaymentAuthorizationId;
	}

	/**
	 * @param studentPaymentAuthorizationId the studentPaymentAuthorizationId to set
	 */
	public void setStudentPaymentAuthorizationId(String studentPaymentAuthorizationId) {
		this.studentPaymentAuthorizationId = studentPaymentAuthorizationId;
	}

	/**
	 * @return the studentPaymentStatus
	 */
	public String getStudentPaymentStatus() {
		return studentPaymentStatus;
	}

	/**
	 * @param studentPaymentStatus the studentPaymentStatus to set
	 */
	public void setStudentPaymentStatus(String studentPaymentStatus) {
		this.studentPaymentStatus = studentPaymentStatus;
	}
	
}
