/**
 * 
 */
package com.emms.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * @author Harshal
 *
 */
@Entity
@Table(name="STUDENT_PAYMENT_DETAILS")
public class StudentPaymentDetails extends Payment implements Serializable {
	private static final long serialVersionUID = 1709993615435141232L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@OneToOne(cascade = CascadeType.ALL, mappedBy = "studentPaymentDetails")
	private StudentBillingDetails studentBillingDetails;
	@ManyToOne
	@JoinColumn(name = "student_id")
	private Student student;
	
	private StudentPaymentDetails() {

	}
	
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the student
	 */
	public Student getStudent() {
		return student;
	}

	/**
	 * @param student the student to set
	 */
	public void setStudent(Student student) {
		this.student = student;
	}


	/**
	 * @return the studentBillingDetails
	 */
	public StudentBillingDetails getStudentBillingDetails() {
		return studentBillingDetails;
	}

	/**
	 * @param studentBillingDetails the studentBillingDetails to set
	 */
	public void setStudentBillingDetails(StudentBillingDetails studentBillingDetails) {
		this.studentBillingDetails = studentBillingDetails;
	}
}
