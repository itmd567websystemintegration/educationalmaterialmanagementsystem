/**
 * 
 */
package com.emms.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.web.multipart.MultipartFile;

/**
 * @author Harshal
 *
 */
@Entity
@Table(name="LAB_MATERIAL")
public class LabMaterial implements Serializable {
	private static final long serialVersionUID = -6665276560470986662L;
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO )
    private Long id;
	private String title;
	private String professor;
	private String itemdetails1;
	private String itemdetails2;
	private String itemdetails3;
	private String itemdetails4;
	private String itemdetails5;
	private String description;
	@Transient
	private MultipartFile labMaterialImage;
	@ManyToOne
	@JoinColumn(name = "student_id")
	private Student student;
	@ManyToOne
	@JoinColumn(name = "department_id")
	private Department department;
	@ManyToOne
	@JoinColumn(name = "course_id")
	private Course course;
	private BigDecimal price;
	
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the professorName
	 */
	
	/**
	 * @return the labMaterialImage
	 */
	public MultipartFile getLabMaterialImage() {
		return labMaterialImage;
	}
	/**
	 * @return the professor
	 */
	public String getProfessor() {
		return professor;
	}
	/**
	 * @param professor the professor to set
	 */
	public void setProfessor(String professor) {
		this.professor = professor;
	}
	/**
	 * @return the itemdetails1
	 */
	public String getItemdetails1() {
		return itemdetails1;
	}
	/**
	 * @param itemdetails1 the itemdetails1 to set
	 */
	public void setItemdetails1(String itemdetails1) {
		this.itemdetails1 = itemdetails1;
	}
	/**
	 * @return the itemdetails2
	 */
	public String getItemdetails2() {
		return itemdetails2;
	}
	/**
	 * @param itemdetails2 the itemdetails2 to set
	 */
	public void setItemdetails2(String itemdetails2) {
		this.itemdetails2 = itemdetails2;
	}
	/**
	 * @return the itemdetails3
	 */
	public String getItemdetails3() {
		return itemdetails3;
	}
	/**
	 * @param itemdetails3 the itemdetails3 to set
	 */
	public void setItemdetails3(String itemdetails3) {
		this.itemdetails3 = itemdetails3;
	}
	/**
	 * @return the itemdetails4
	 */
	public String getItemdetails4() {
		return itemdetails4;
	}
	/**
	 * @param itemdetails4 the itemdetails4 to set
	 */
	public void setItemdetails4(String itemdetails4) {
		this.itemdetails4 = itemdetails4;
	}
	/**
	 * @return the itemdetails5
	 */
	public String getItemdetails5() {
		return itemdetails5;
	}
	/**
	 * @param itemdetails5 the itemdetails5 to set
	 */
	public void setItemdetails5(String itemdetails5) {
		this.itemdetails5 = itemdetails5;
	}
	/**
	 * @param labMaterialImage the labMaterialImage to set
	 */
	public void setLabMaterialImage(MultipartFile labMaterialImage) {
		this.labMaterialImage = labMaterialImage;
	}
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * @return the student
	 */
	public Student getStudent() {
		return student;
	}
	/**
	 * @param student the student to set
	 */
	public void setStudent(Student student) {
		this.student = student;
	}
	/**
	 * @return the department
	 */
	public Department getDepartment() {
		return department;
	}
	/**
	 * @param department the department to set
	 */
	public void setDepartment(Department department) {
		this.department = department;
	}
	/**
	 * @return the course
	 */
	public Course getCourse() {
		return course;
	}
	/**
	 * @param course the course to set
	 */
	public void setCourse(Course course) {
		this.course = course;
	}
	/**
	 * @return the price
	 */
	public BigDecimal getPrice() {
		return price;
	}
	/**
	 * @param price the price to set
	 */
	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	@Override
	public String toString() {
		return "LabMaterial [id=" + id + ", title=" + title + ", professor=" + professor + ", itemdetails1="
				+ itemdetails1 + ", itemdetails2=" + itemdetails2 + ", itemdetails3=" + itemdetails3 + ", itemdetails4="
				+ itemdetails4 + ", itemdetails5=" + itemdetails5 + ", labMaterialImage=" + labMaterialImage
				+ ", student=" + student + ", department=" + department + ", course=" + course + ", price=" + price
				+ "]";
	}
	
}
