/**
 * 
 */
package com.emms.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author Harshal
 *
 */
@Entity
@Table(name="DELIVERY_ADDRESS")
public class DeliveryAddress implements Serializable {
	private static final long serialVersionUID = -2550095893649545609L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String deliveryAddressName;
	private String deliveryAddressStreet1;
	private String deliveryAddressStreet2;
	private String deliveryAddressCity;
	private String deliveryAddressState;
	private String deliveryAddressCountry;
	private String deliveryAddressPostalcode;
	@OneToOne
	@JsonIgnore
    private StudentOrder studentOrder;
	
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the deliveryAddressName
	 */
	public String getDeliveryAddressName() {
		return deliveryAddressName;
	}
	/**
	 * @param deliveryAddressName the deliveryAddressName to set
	 */
	public void setDeliveryAddressName(String deliveryAddressName) {
		this.deliveryAddressName = deliveryAddressName;
	}
	/**
	 * @return the deliveryAddressStreet1
	 */
	public String getDeliveryAddressStreet1() {
		return deliveryAddressStreet1;
	}
	/**
	 * @param deliveryAddressStreet1 the deliveryAddressStreet1 to set
	 */
	public void setDeliveryAddressStreet1(String deliveryAddressStreet1) {
		this.deliveryAddressStreet1 = deliveryAddressStreet1;
	}
	/**
	 * @return the deliveryAddressStreet2
	 */
	public String getDeliveryAddressStreet2() {
		return deliveryAddressStreet2;
	}
	/**
	 * @param deliveryAddressStreet2 the deliveryAddressStreet2 to set
	 */
	public void setDeliveryAddressStreet2(String deliveryAddressStreet2) {
		this.deliveryAddressStreet2 = deliveryAddressStreet2;
	}
	/**
	 * @return the deliveryAddressCity
	 */
	public String getDeliveryAddressCity() {
		return deliveryAddressCity;
	}
	/**
	 * @param deliveryAddressCity the deliveryAddressCity to set
	 */
	public void setDeliveryAddressCity(String deliveryAddressCity) {
		this.deliveryAddressCity = deliveryAddressCity;
	}
	/**
	 * @return the deliveryAddressState
	 */
	public String getDeliveryAddressState() {
		return deliveryAddressState;
	}
	/**
	 * @param deliveryAddressState the deliveryAddressState to set
	 */
	public void setDeliveryAddressState(String deliveryAddressState) {
		this.deliveryAddressState = deliveryAddressState;
	}
	/**
	 * @return the deliveryAddressCountry
	 */
	public String getDeliveryAddressCountry() {
		return deliveryAddressCountry;
	}
	/**
	 * @param deliveryAddressCountry the deliveryAddressCountry to set
	 */
	public void setDeliveryAddressCountry(String deliveryAddressCountry) {
		this.deliveryAddressCountry = deliveryAddressCountry;
	}
	
	/**
	 * @return the deliveryAddressPostalcode
	 */
	public String getDeliveryAddressPostalcode() {
		return deliveryAddressPostalcode;
	}
	/**
	 * @param deliveryAddressPostalcode the deliveryAddressPostalcode to set
	 */
	public void setDeliveryAddressPostalcode(String deliveryAddressPostalcode) {
		this.deliveryAddressPostalcode = deliveryAddressPostalcode;
	}
	/**
	 * @return the studentOrder
	 */
	public StudentOrder getStudentOrder() {
		return studentOrder;
	}
	/**
	 * @param studentOrder the studentOrder to set
	 */
	public void setStudentOrder(StudentOrder studentOrder) {
		this.studentOrder = studentOrder;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "DeliveryAddress [id=" + id + ", deliveryAddressName=" + deliveryAddressName
				+ ", deliveryAddressStreet1=" + deliveryAddressStreet1 + ", deliveryAddressStreet2="
				+ deliveryAddressStreet2 + ", deliveryAddressCity=" + deliveryAddressCity + ", deliveryAddressState="
				+ deliveryAddressState + ", deliveryAddressCountry=" + deliveryAddressCountry
				+ ", deliveryAddressPostalcode=" + deliveryAddressPostalcode + "]";
	}
	
}
