/**
 * 
 */
package com.emms.model;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author Harshal
 *
 */
@Entity
@Table(name="STUDENT_CART")
public class StudentCart {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	private BigDecimal total;

	@OneToMany(mappedBy = "studentCart", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JsonIgnore
	private List<SelectedBook> selectedBookList;

	@OneToMany(mappedBy = "studentCart", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JsonIgnore
	private List<SelectedNotes> selectedNotesList;
	
	
	@OneToOne
	@JsonIgnore
	private Student student;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}
	
	public List<SelectedBook> getSelectedBookList() {
		return selectedBookList;
	}

	public void setCartBookList(List<SelectedBook> selectedBookList) {
		this.selectedBookList = selectedBookList;
	}

	/**
	 * @return the selectedNotesList
	 */
	public List<SelectedNotes> getSelectedNotesList() {
		return selectedNotesList;
	}

	/**
	 * @param selectedNotesList the selectedNotesList to set
	 */
	public void setSelectedNotesList(List<SelectedNotes> selectedNotesList) {
		this.selectedNotesList = selectedNotesList;
	}		
}
