/**
 * 
 */
package com.emms.service;

import java.util.List;

import com.emms.model.Book;
import com.emms.model.Department;

/**
 * @author Harshal
 *
 */
public interface DepartmentService {
	// adding all books
    List<Department> getAllDepartment();
    
    Department findById(Long id);
    
    void addDepartment(Department department);
    
    
}
