/**
 * 
 */
package com.emms.service;

import java.util.List;

import com.emms.model.Book;
import com.emms.model.LabMaterial;
import com.emms.model.Notes;
import com.emms.model.SelectedBook;
import com.emms.model.SelectedLabMaterial;
import com.emms.model.SelectedNotes;
import com.emms.model.Student;
import com.emms.model.StudentCart;

/**
 * @author Harshal
 *
 */
public interface SelectedItemService{
	SelectedBook addToBookCart(Book selectedBook, Student student);
	
	SelectedNotes addToNotesCart(Notes selectedNotes, Student student);
	
	SelectedLabMaterial addToLabCart(LabMaterial labMaterial, Student student);
	
	SelectedBook  addToCart(SelectedBook selectedBook);
	SelectedNotes addToCart(SelectedNotes selectedNotes);
	SelectedLabMaterial addToCart(SelectedLabMaterial selectedLabMaterial);
	
	SelectedNotes modifySelectedNotes(SelectedNotes selectedNotes);
	
	SelectedLabMaterial modifySelectedLabMaterial(SelectedLabMaterial selectedLabMaterial);
	
	SelectedBook modifySelectedBook(SelectedBook selectedBook);
	
	SelectedBook searchById(Long id);
	
	SelectedNotes searchByNotesId(Long id);
	
	SelectedLabMaterial searchByLabId(Long id);
	
	List<SelectedBook> searchBookByStudentCart(StudentCart studentCart);
	
	List<SelectedNotes> searchNotesByStudentCart(StudentCart studentCart);
	
	List<SelectedLabMaterial> searchLabMaterialByStudentCart(StudentCart studentCart);
	
	void deleteSelectedBook(SelectedBook selectedItem);
	void deleteSelectedNotes(SelectedNotes selectedItem);
	void deleteSelectedLabMaterial(SelectedLabMaterial selectedItem);

}
