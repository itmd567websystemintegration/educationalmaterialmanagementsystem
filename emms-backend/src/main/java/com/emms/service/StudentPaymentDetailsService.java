package com.emms.service;

import com.emms.model.StudentPaymentDetails;

/**
 * @author Harshal
 *
 */
public interface StudentPaymentDetailsService {
	void deleteById(Long id);
	
	StudentPaymentDetails searchByById(Long id);
}
