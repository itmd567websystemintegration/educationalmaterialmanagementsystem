/**
 * 
 */
package com.emms.service;

import com.emms.model.StudentCart;

/**
 * @author Harshal
 *
 */
public interface StudentCartService {
    void deleteStudentCart(StudentCart studentCart);
    
    StudentCart modifyStudentCart(StudentCart studentCart);
}
