package com.emms.service;

import com.emms.model.StudentDeliveryDetails;

public interface StudentDeliveryDetailsService {
	
	void deleteById(Long id);
	 
	StudentDeliveryDetails findById(Long id);  
}
