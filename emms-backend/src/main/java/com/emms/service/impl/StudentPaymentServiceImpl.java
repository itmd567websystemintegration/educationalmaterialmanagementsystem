/**
 * 
 */
package com.emms.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.emms.model.StudentPaymentDetails;
import com.emms.repository.StudentPaymentDetailsRepository;
import com.emms.service.StudentPaymentDetailsService;

/**
 * @author Harshal
 *
 */
@Service
public class StudentPaymentServiceImpl implements StudentPaymentDetailsService {

	@Autowired
	private StudentPaymentDetailsRepository studentPaymentDetailsRepository;
	
	/* (non-Javadoc)
	 * @see com.emms.service.StudentPaymentDetailsService#deleteById(java.lang.Long)
	 */
	@Override
	public void deleteById(Long studentId) {
		studentPaymentDetailsRepository.delete(studentId);
	}

	/* (non-Javadoc)
	 * @see com.emms.service.StudentPaymentDetailsService#searchByById(java.lang.Long)
	 */
	@Override
	public StudentPaymentDetails searchByById(Long studentId) {
		return studentPaymentDetailsRepository.findOne(studentId);
	}

}
