/**
 * 
 */
package com.emms.service.impl;

/**
 * @author Harshal
 *
 */

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.emms.model.Book;
import com.emms.model.LabMaterial;
import com.emms.model.Notes;
import com.emms.model.SelectedBook;
import com.emms.model.SelectedLabMaterial;
import com.emms.model.SelectedNotes;
import com.emms.model.Student;
import com.emms.model.StudentCart;
import com.emms.repository.SelectedBookRepository;
import com.emms.repository.SelectedLabMaterialRepository;
import com.emms.repository.SelectedNotesRepository;
import com.emms.service.SelectedItemService;

@Service
public class SelectedItemServiceImpl implements SelectedItemService{

	@Autowired
	private SelectedBookRepository selectedBookRepository;
	
	@Autowired
	private SelectedNotesRepository selectedNotesRepository;

	@Autowired
	private SelectedLabMaterialRepository selectedLabMaterialRepository;
	
	public SelectedBook searchById(Long id) {
		return (SelectedBook) selectedBookRepository.findOne(id);
	}
	
	public SelectedNotes searchByNotesId(Long id) {
		return (SelectedNotes) selectedNotesRepository.findOne(id);
	}

	public SelectedLabMaterial searchByLabId(Long id) {
		return (SelectedLabMaterial) selectedLabMaterialRepository.findOne(id);
	}
	
	public SelectedBook addToBookCart(Book book, Student student) {
		List<SelectedBook> selectedBookList = searchBookByStudentCart(student.getStudentCart());

		for (SelectedBook selectedBook : selectedBookList) {
			if (book.getId() == selectedBook.getBook().getId()) {
				selectedBookRepository.save(selectedBook);
				return selectedBook;
			}
		}

		SelectedBook selectedBook = new SelectedBook();
		selectedBook.setStudentCart(student.getStudentCart());
		selectedBook.setBook(book);
		selectedBook = selectedBookRepository.save(selectedBook);

		return selectedBook;
	}
	
	public SelectedNotes addToNotesCart(Notes notes, Student student) {
		List<SelectedNotes> selectedNotesList = searchNotesByStudentCart(student.getStudentCart());

		for (SelectedNotes selectedNotes : selectedNotesList) {
			if (notes.getId() == selectedNotes.getNotes().getId()) {
				selectedNotesRepository.save(selectedNotes);
				return selectedNotes;
			}
		}

		SelectedNotes selectedNotes = new SelectedNotes();
		selectedNotes.setStudentCart(student.getStudentCart());
		selectedNotes.setNotes(notes);
		selectedNotes = selectedNotesRepository.save(selectedNotes);

		return selectedNotes;
	}
	
	public SelectedLabMaterial addToLabCart(LabMaterial labMaterial, Student student) {
		List<SelectedLabMaterial> selectedLabMaterialList = searchLabMaterialByStudentCart(student.getStudentCart());

		for (SelectedLabMaterial selectedLabMaterial : selectedLabMaterialList) {
			if (labMaterial.getId() == selectedLabMaterial.getLab().getId()) {
				selectedLabMaterialRepository.save(selectedLabMaterial);
				return selectedLabMaterial;
			}
		}

		SelectedLabMaterial selectedLabMaterial = new SelectedLabMaterial();
		selectedLabMaterial.setStudentCart(student.getStudentCart());
		selectedLabMaterial.setLab(labMaterial);
		selectedLabMaterial = selectedLabMaterialRepository.save(selectedLabMaterial);

		return selectedLabMaterial;
	}
	
	@Transactional
	public void deleteSelectedBook(SelectedBook selectedBook) {
		selectedBookRepository.delete(selectedBook);
	}
	
	@Transactional
	public void deleteSelectedNotes(SelectedNotes selectedNotes) {
		selectedNotesRepository.delete(selectedNotes);
	}
	
	@Transactional
	public void deleteSelectedLabMaterial(SelectedLabMaterial selectedLabMaterial) {
		selectedLabMaterialRepository.delete(selectedLabMaterial);
	}

	public SelectedBook modifySelectedBook(SelectedBook selectedBook) {
		BigDecimal bigDecimal = selectedBook.getBook().getPrice();
		bigDecimal = bigDecimal.setScale(2, BigDecimal.ROUND_HALF_UP);
		selectedBookRepository.save(selectedBook);

		return selectedBook;		
	}
	
	public SelectedNotes modifySelectedNotes(SelectedNotes selectedNotes) {
		BigDecimal bigDecimal = selectedNotes.getNotes().getPrice();
		bigDecimal = bigDecimal.setScale(2, BigDecimal.ROUND_HALF_UP);
		selectedNotesRepository.save(selectedNotes);
		return selectedNotes;		
	}
	

	public List<SelectedBook> searchBookByStudentCart(StudentCart studentCart) {
		return selectedBookRepository.findSelectedBookByStudentCart(studentCart);
	}

	public List<SelectedNotes> searchNotesByStudentCart(StudentCart studentCart) {
		return selectedNotesRepository.findSelectedNotesByStudentCart(studentCart);
	}
	
	public List<SelectedLabMaterial> searchLabMaterialByStudentCart(StudentCart studentCart) {
		return selectedLabMaterialRepository.findLabMaterialByStudentCart(studentCart);
	}
	
	@Override
	public SelectedBook addToCart(SelectedBook selectedBook) {
		return selectedBookRepository.save(selectedBook);
	}
	
	@Override
	public SelectedNotes addToCart(SelectedNotes selectedNotes) {
		return selectedNotesRepository.save(selectedNotes);
	}
	
	@Override
	public SelectedLabMaterial addToCart(SelectedLabMaterial selectedLabMaterial) {
		return selectedLabMaterialRepository.save(selectedLabMaterial);
	}

	@Override
	public SelectedLabMaterial modifySelectedLabMaterial(SelectedLabMaterial selectedLabMaterial) {
		BigDecimal bigDecimal = selectedLabMaterial.getLab().getPrice();
		bigDecimal = bigDecimal.setScale(2, BigDecimal.ROUND_HALF_UP);
		selectedLabMaterialRepository.save(selectedLabMaterial);
		return selectedLabMaterial;
	}
}

