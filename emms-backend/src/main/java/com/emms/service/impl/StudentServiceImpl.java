/**
 * 
 */
package com.emms.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.emms.model.Student;
import com.emms.model.StudentBillingDetails;
import com.emms.model.StudentCart;
import com.emms.model.StudentDeliveryDetails;
import com.emms.model.StudentPaymentDetails;
import com.emms.repository.LoggedInUserRole;
import com.emms.repository.StudentBillingDetailsRepository;
import com.emms.repository.StudentDeliveryAddressRepository;
import com.emms.repository.StudentPaymentDetailsRepository;
import com.emms.repository.StudentRepository;
import com.emms.security.StudentRole;
import com.emms.service.StudentService;

/**
 * @author Harshal
 *
 */
@Service
public class StudentServiceImpl implements StudentService {
	private static final Logger LOG = LoggerFactory.getLogger(StudentServiceImpl.class);

	@Autowired
	private StudentRepository studentRepository;

	@Autowired
	private LoggedInUserRole loggedInUserRole;

	@Autowired
	private StudentBillingDetailsRepository studentBillingDetailsRepository;

	@Autowired
	private StudentPaymentDetailsRepository studentPaymentDetailsRepository;

	@Autowired
	private StudentDeliveryAddressRepository studentDeliveryAddressRepository;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.emms.service.StudentService#createStudent(com.emms.model.Student,
	 * java.util.Set)
	 */
	@Override
	public Student createStudent(Student student, Set<StudentRole> studentRoles) {
		Student tempStudent = studentRepository.findByUserName(student.getUsername());

		if (tempStudent != null) {
			LOG.info("Student <{}> already exist.", student.getUsername());
		} else {
			for (StudentRole studentRole : studentRoles) {
				loggedInUserRole.save(studentRole.getRole());
			}

			student.getStudentRoles().addAll(studentRoles);

			StudentCart studentCart = new StudentCart();
			studentCart.setStudent(student);
			;
			student.setStudentCart(studentCart);
			student.setStudentPaymentDetailsList(new ArrayList<StudentPaymentDetails>());
			student.setStudentDeliveryDetailsList(new ArrayList<StudentDeliveryDetails>());

			tempStudent = studentRepository.save(student);
		}

		return tempStudent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.emms.service.StudentService#add(com.emms.model.Student)
	 */
	@Override
	public Student add(Student student) {
		return studentRepository.save(student);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.emms.service.StudentService#searchdByStudentId(java.lang.Long)
	 */
	@Override
	public Student searchdByStudentId(Long studentId) {
		return studentRepository.findOne(studentId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.emms.service.StudentService#searchByStudentEmail(java.lang.String)
	 */
	@Override
	public Student searchByStudentEmail(String studentEmailId) {
		return studentRepository.findByEmailId(studentEmailId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.emms.service.StudentService#searchByStudentUserName(java.lang.String)
	 */
	@Override
	public Student searchByStudentUserName(String studentUserName) {
		return studentRepository.findByUserName(studentUserName);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.emms.service.StudentService#setStudentDefaultDelivery(com.emms.model.
	 * Student, java.lang.Long)
	 */
	@Override
	public void setStudentDefaultDelivery(Student student, Long studentDeliveryDetailsId) {
		List<StudentDeliveryDetails> studentDeliveryList = (List<StudentDeliveryDetails>) studentDeliveryAddressRepository.findAll();

		for (StudentDeliveryDetails studentDeliveryDetails : studentDeliveryList) {
			if (studentDeliveryDetails.getId() == studentDeliveryDetailsId) {
				studentDeliveryDetails.setStudentShippingDefault(true);
				studentDeliveryAddressRepository.save(studentDeliveryDetails);
			} else {
				studentDeliveryDetails.setStudentShippingDefault(false);
				studentDeliveryAddressRepository.save(studentDeliveryDetails);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.emms.service.StudentService#modifyStudentPaymentDetails(com.emms.
	 * model.Student, com.emms.model.StudentPaymentDetails,
	 * com.emms.model.StudentBillingDetails,
	 * com.emms.model.StudentDeliveryDetails)
	 */
	@Override
	public void modifyStudentPaymentDetails(Student student, StudentPaymentDetails studentPaymentDetails,
			StudentBillingDetails studentBillingDetails) {
		studentBillingDetailsRepository.save(studentBillingDetails);
		studentPaymentDetailsRepository.save(studentPaymentDetails);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.emms.service.StudentService#modifyStudentBillingDetails(com.emms.
	 * model.Student, com.emms.model.StudentPaymentDetails,
	 * com.emms.model.StudentBillingDetails)
	 */
	@Override
	public void modifyStudentBillingDetails(Student student, StudentPaymentDetails studentPaymentDetails,
			StudentBillingDetails studentBillingDetails) {
		studentPaymentDetails.setStudent(student);
		studentPaymentDetails.setStudentBillingDetails(studentBillingDetails);
		studentBillingDetails.setStudentPaymentDetails(studentPaymentDetails);
		student.getStudentPaymentDetailsList().add(studentPaymentDetails);
		add(student);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.emms.service.StudentService#modifyStudentDeliveryDetails(com.emms.
	 * model.StudentDeliveryDetails, com.emms.model.Student)
	 */
	@Override
	public void modifyStudentDeliveryDetails(StudentDeliveryDetails studentDeliveryDetails, Student student) {
		studentDeliveryDetails.setStudent(student);;
		studentDeliveryDetails.setStudentShippingDefault(true);;
		add(student);
		student.getStudentDeliveryDetailsList().add(studentDeliveryDetails);
	}

	@Override
	public Student searchByStudentAccessToken(String accessToken) {
		return studentRepository.findByAccessToken(accessToken);
	}

}
