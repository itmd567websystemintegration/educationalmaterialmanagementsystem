package com.emms.service.impl;

import java.sql.Date;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.emms.model.BillingAddress;
import com.emms.model.DeliveryAddress;
import com.emms.model.Payment;
import com.emms.model.SelectedBook;
import com.emms.model.SelectedLabMaterial;
import com.emms.model.SelectedNotes;
import com.emms.model.Student;
import com.emms.model.StudentCart;
import com.emms.model.StudentOrder;
import com.emms.payments.ProtectPayAuth;
import com.emms.repository.StudentOrderRepository;
import com.emms.service.SelectedItemService;
import com.emms.service.StudentOrderService;
@Service
public class StudentOrderServiceImp implements StudentOrderService {
	@Autowired
	private StudentOrderRepository studentOrderRepository;

	@Autowired
	private SelectedItemService selectedItemService;

	@Override
	public StudentOrder searchStudentOrderById(Long id) {
		return studentOrderRepository.findOne(id);
	}

	@Override
	public StudentOrder checkOutOrder(Student student, StudentCart studentCart, Payment orderPayment,
			BillingAddress orderBillingAddress, DeliveryAddress orderDeliveryAddress, String deliveryMode) throws Exception {
		StudentOrder studentOrder = new StudentOrder();
		studentOrder.setStudentBillingAddress(orderBillingAddress);
		studentOrder.setOrderStatus("NEW");
		studentOrder.setPayment(orderPayment);
		System.out.println("DeliveryAddress: "+orderDeliveryAddress);
		studentOrder.setDeliveryAddress(orderDeliveryAddress);
		studentOrder.setDeliveryMethod(deliveryMode);
		LocalDate today = LocalDate.now();
		LocalDate estimatedDeliveryDate;
		// check for nulll
		
		if (deliveryMode.equals("groundShipping")) {
			estimatedDeliveryDate=today.plusDays(5);
		} else {
			estimatedDeliveryDate=today.plusDays(3);
		}
		studentOrder.setDeliveryDate(Date.valueOf(estimatedDeliveryDate));
		List<SelectedBook> selectedBookList = selectedItemService.searchBookByStudentCart(studentCart);		
		for (SelectedBook selectedBook : selectedBookList) {
			selectedBook.setOrder(studentOrder);
		}
		
		List<SelectedNotes> selectedNotesList = selectedItemService.searchNotesByStudentCart(studentCart);
		for(SelectedNotes selectedNotes : selectedNotesList) {
			selectedNotes.setOrder(studentOrder);
		}
		
		List<SelectedLabMaterial> selectedLabMaterialList = selectedItemService.searchLabMaterialByStudentCart(studentCart);
		for(SelectedLabMaterial selectedLabMaterial : selectedLabMaterialList) {
			selectedLabMaterial.setOrder(studentOrder);
		}
		
		studentOrder.setSelectedBookList(selectedBookList);
		studentOrder.setSelectedNotesList(selectedNotesList);
		studentOrder.setSelectedLabMaterialList(selectedLabMaterialList);
		studentOrder.setOrderDate(Calendar.getInstance().getTime());
		studentOrder.setOrderTotal(studentCart.getTotal());
		orderDeliveryAddress.setStudentOrder(studentOrder);
		orderBillingAddress.setStudentOrder(studentOrder);
		orderPayment.setOrder(studentOrder);
		studentOrder.setStudent(student);
		
		
		System.out.println("sending request to payment gateway>>>>>>>>>>>>");
		ProtectPayAuth protectpayAuth = new ProtectPayAuth(studentOrder);
		protectpayAuth.performSale();
			

		studentOrder = studentOrderRepository.save(studentOrder);
		System.out.println("payment processing completed>>>>>>>>>>>>");
		
		return studentOrder;
	}

	@Override
	public List<StudentOrder> searchAllStudentOrderByStudent(Student student) {
		return studentOrderRepository.findByStudent(student);
	}
	
	@Override
	public List<StudentOrder> searchAllStudentOrder() {
		return (List<StudentOrder>) studentOrderRepository.findAll();
	}

}
