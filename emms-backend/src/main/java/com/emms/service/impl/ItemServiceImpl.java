/**
 * 
 */
package com.emms.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.emms.model.Book;
import com.emms.model.LabMaterial;
import com.emms.model.Notes;
import com.emms.repository.BookRepository;
import com.emms.repository.LabMaterialRepository;
import com.emms.repository.NotesRepository;
import com.emms.service.ItemService;

/**
 * @author Harshal
 *
 */
@Service
public class ItemServiceImpl implements ItemService {

	@Autowired
	private BookRepository bookRepository;
	
	@Autowired
	private NotesRepository notesRepository;
	
	@Autowired
	private LabMaterialRepository labMaterialRepository;
	
	@Override
	public Book searchBookById(Long id) {
		return bookRepository.findOne(id);
	}
	
	@Override
	public Notes searchNotesById(Long id) {
		return notesRepository.findOne(id);
	}

	@Override
	public Book addBook(Book book) {
		return bookRepository.save(book);
	}

	@Override
	public List<Book> searchByTitle(String title) {
		List<Book> bookList = bookRepository.findBookByTitle(title);
		return bookList;
	}

	@Override
	public void deleteBook(Long id) {
		bookRepository.delete(id);		
	}


	@Override
	public void deleteNotes(Long id) {
		notesRepository.delete(id);	
	}

	@Override
	public void deleteLabMaterial(Long id) {
		labMaterialRepository.delete(id);			
	}
	
	@Override
	public Notes addNotes(Notes notes) {
		return notesRepository.save(notes);
	}

	@Override
	public LabMaterial addLabMaterial(LabMaterial labMaterial) {
		return labMaterialRepository.save(labMaterial);
	}

	@Override
	public LabMaterial searchLabMaterialById(Long id) {
		return labMaterialRepository.findOne(id);
	}

	@Override
	public List<Notes> searchAllNotes() {
		List<Notes> notesList = (List<Notes>) notesRepository.findAll();
		return notesList;
	}

	@Override
	public List<LabMaterial> searchAllLabMaterial() {
		List<LabMaterial> labMaterialList = (List<LabMaterial>) labMaterialRepository.findAll();
		return labMaterialList;
	}
	
	@Override
	public List<Book> searchAllBook() {
		List<Book> bookList = (List<Book>) bookRepository.findAll();
		return bookList;
	}
}
