/**
 * 
 */
package com.emms.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.emms.model.Course;
import com.emms.model.Department;
import com.emms.repository.CourseRepository;
import com.emms.service.CourseService;

/**
 * @author Harshal
 *
 */
@Service
public class CourseServiceImpl implements CourseService {

	
	@Autowired
	CourseRepository courseRepository;
	
	@Override
	public List<Course> getAllCourse() {
		return (List<Course>) courseRepository.findAll();
	}

	@Override
	public Course findById(Long Id) {
		// TODO Auto-generated method stub
		return courseRepository.findOne(Id);
	}

	@Override
	public void addCourse(Course course) {
		courseRepository.save(course);
	}
	
}
