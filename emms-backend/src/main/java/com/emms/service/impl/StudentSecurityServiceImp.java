/**
 * 
 */
package com.emms.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.emms.model.Student;
import com.emms.repository.StudentRepository;

/**
 * @author Harshal
 *
 */
@Service
public class StudentSecurityServiceImp implements UserDetailsService {

	private static final Logger LOG = LoggerFactory.getLogger(StudentSecurityServiceImp.class);

	@Autowired
	private StudentRepository studentRepository;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.security.core.userdetails.UserDetailsService#
	 * loadUserByUsername(java.lang.String)
	 */
	@Override
	public UserDetails loadUserByUsername(String studentUserName) throws UsernameNotFoundException {
		// TODO Auto-generated method stub
		Student student = studentRepository.findByUserName(studentUserName);
		if (null == student) {
			LOG.warn("Student with username '{}' not found", studentUserName);
			throw new UsernameNotFoundException("Student with username " + studentUserName + " not found");
		}
		return student;
	}

}
