/**
 * 
 */
package com.emms.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.emms.model.Department;
import com.emms.repository.DepartmentRepository;
import com.emms.service.DepartmentService;

/**
 * @author Harshal
 *
 */
@Service
public class DepartmentServiceImpl implements DepartmentService {

	
	@Autowired
	DepartmentRepository departmentRepository;
	
	@Override
	public List<Department> getAllDepartment() {
		return (List<Department>) departmentRepository.findAll();
	}

	@Override
	public Department findById(Long id) {
		// TODO Auto-generated method stub
		return departmentRepository.findOne(id);
	}

	@Override
	public void addDepartment(Department department) {
		departmentRepository.save(department);
	}
}
