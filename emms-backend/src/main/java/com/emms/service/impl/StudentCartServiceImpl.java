/**
 * 
 */
package com.emms.service.impl;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.emms.model.SelectedBook;
import com.emms.model.SelectedLabMaterial;
import com.emms.model.SelectedNotes;
import com.emms.model.StudentCart;
import com.emms.repository.StudentCartRepository;
import com.emms.service.SelectedItemService;
import com.emms.service.StudentCartService;

/**
 * @author Harshal
 *
 */
@Service
public class StudentCartServiceImpl implements StudentCartService {
	@Autowired
	private SelectedItemService selectedItemService;

	@Autowired
	private StudentCartRepository studentCartRepository;
	/* (non-Javadoc)
	 * @see com.emms.service.StudentCartService#deleteStudentCart(com.emms.model.StudentCart)
	 */
	@Override
	public void deleteStudentCart(StudentCart studentCart) {
		List<SelectedBook> selectedItemList = selectedItemService.searchBookByStudentCart(studentCart);
		for(SelectedBook selectedItem : selectedItemList) {
			selectedItem.setStudentCart(null);
			selectedItemService.addToCart(selectedItem);
		}
		
		List<SelectedNotes> selectedNotesList = selectedItemService.searchNotesByStudentCart(studentCart);
		for(SelectedNotes selectedNotes : selectedNotesList) {
			selectedNotes.setStudentCart(null);
			selectedItemService.addToCart(selectedNotes);
		}
		
		List<SelectedLabMaterial> selectedLabMaterialList = selectedItemService.searchLabMaterialByStudentCart(studentCart);
		for(SelectedLabMaterial selectedLabMaterial : selectedLabMaterialList) {
			selectedLabMaterial.setStudentCart(null);
			selectedItemService.addToCart(selectedLabMaterial);
		}
		
		studentCart.setTotal(new BigDecimal(0));
		studentCartRepository.save(studentCart);
	}

	/* 
	 * Since the cart is not a shopping cart. The student would only add or remove item from the 
	 * cart. Item belongs to some student.
	 * (non-Javadoc)
	 * @see com.emms.service.StudentCartService#modifyStudentCart(com.emms.model.StudentCart)
	 */
	@Override
	public StudentCart modifyStudentCart(StudentCart studentCart) {
		BigDecimal cartTotal = new BigDecimal(0);
		
		List<SelectedBook> selectedItemList = selectedItemService.searchBookByStudentCart(studentCart);
		
		List<SelectedNotes> selectedNotesItemList = selectedItemService.searchNotesByStudentCart(studentCart);
		
		List<SelectedLabMaterial> selectedLabMaterialItemList = selectedItemService.searchLabMaterialByStudentCart(studentCart);
		
		
		for (SelectedBook selectedItem : selectedItemList) {
			selectedItemService.modifySelectedBook(selectedItem);
			cartTotal = cartTotal.add(selectedItem.getBook().getPrice());
		}
		
		for (SelectedNotes selectedNotesItem : selectedNotesItemList) {
			selectedItemService.modifySelectedNotes(selectedNotesItem);
			cartTotal = cartTotal.add(selectedNotesItem.getNotes().getPrice());
		}
		
		for (SelectedLabMaterial selectedLabMaterialItem : selectedLabMaterialItemList) {
			selectedItemService.modifySelectedLabMaterial(selectedLabMaterialItem);
			cartTotal = cartTotal.add(selectedLabMaterialItem.getLab().getPrice());
		}
		
		studentCart.setTotal(cartTotal);
		studentCartRepository.save(studentCart);
		return studentCart;
	}
}
