/**
 * 
 */
package com.emms.service;

import java.util.List;

import com.emms.model.Book;
import com.emms.model.LabMaterial;
import com.emms.model.Notes;

/**
 * @author Harshal
 *
 */
public interface ItemService{
    
	// adding all books
    List<Book> searchAllBook();
    
    List<Notes> searchAllNotes();
    
    List<LabMaterial> searchAllLabMaterial();
	
    Book searchBookById(Long id);
    
    Notes searchNotesById(Long id);
    
    LabMaterial searchLabMaterialById(Long id);
	
    Book addBook(Book item);
	
    Notes addNotes(Notes notes);
    
    LabMaterial addLabMaterial(LabMaterial labMaterial);
    
    List<Book> searchByTitle(String title);
    
    void deleteBook(Long id);
    
    void deleteNotes(Long id);
    
    void deleteLabMaterial(Long id);
}
