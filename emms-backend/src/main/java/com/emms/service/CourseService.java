/**
 * 
 */
package com.emms.service;

import java.util.List;

import com.emms.model.Course;
import com.emms.model.Department;

/**
 * @author Harshal
 *
 */
public interface CourseService {
    List<Course> getAllCourse();
    Course findById(Long Id);
    void addCourse(Course course);
}
