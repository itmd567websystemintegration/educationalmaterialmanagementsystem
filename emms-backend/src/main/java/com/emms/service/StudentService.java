/**
 * 
 */
package com.emms.service;

import java.util.Set;

import com.emms.model.Student;
import com.emms.model.StudentBillingDetails;
import com.emms.model.StudentDeliveryDetails;
import com.emms.model.StudentPaymentDetails;
import com.emms.security.PasswordResetKey;
import com.emms.security.StudentRole;

/**
 * @author Harshal
 *
 */
public interface StudentService {

	Student createStudent(Student student, Set<StudentRole> studentRoles);

	Student add(Student student);

	Student searchdByStudentId(Long id);

	Student searchByStudentEmail(String email);
	
	Student searchByStudentAccessToken(String accessToken);

	Student searchByStudentUserName(String studentUserName);
	

	void setStudentDefaultDelivery(Student student, Long studentDeliveryDetailsId);

	void modifyStudentPaymentDetails(Student student, StudentPaymentDetails studentPaymentDetails, 
			StudentBillingDetails studentBillingDetails);

	void modifyStudentBillingDetails(Student student, StudentPaymentDetails studentPaymentDetails, 
			StudentBillingDetails studentBillingDetails);

	void modifyStudentDeliveryDetails(StudentDeliveryDetails studentDeliveryDetails, Student student);	
}
