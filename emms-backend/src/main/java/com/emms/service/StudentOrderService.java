/**
 * 
 */
package com.emms.service;

import java.util.List;

import com.emms.model.BillingAddress;
import com.emms.model.DeliveryAddress;
import com.emms.model.Payment;
import com.emms.model.Student;
import com.emms.model.StudentCart;
import com.emms.model.StudentOrder;

/**
 * @author Harshal
 *
 */
public interface StudentOrderService {
	StudentOrder searchStudentOrderById(Long id);
	
	List<StudentOrder> searchAllStudentOrderByStudent(Student student);
	
	List<StudentOrder> searchAllStudentOrder();
	
	StudentOrder checkOutOrder(Student student, StudentCart studentCart, Payment payment, BillingAddress billingAddress,
			DeliveryAddress deliveryAddress, String deliveryMode) throws Exception;	
	
}
