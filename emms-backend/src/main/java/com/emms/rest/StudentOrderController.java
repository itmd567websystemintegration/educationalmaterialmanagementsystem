/**
 * 
 */
package com.emms.rest;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.emms.model.Notes;
import com.emms.model.Student;
import com.emms.model.StudentOrder;
import com.emms.service.StudentOrderService;
import com.emms.service.StudentService;

/**
 * @author Harshal
 *
 */
@RestController
@RequestMapping("/order")
public class StudentOrderController {
	@Autowired
	private StudentOrderService studentOrderService;
	@Autowired
	private StudentService studentService;
	
	@RequestMapping(value = "/getStudentOrderHistory", method=RequestMethod.POST)
	public List<StudentOrder> getStudentOrderHistory(@RequestBody HashMap<String, Object> mapper) throws Exception{
		String idToken = (String) mapper.get("idToken");
		Student student = studentService.searchByStudentAccessToken(idToken);
		List<StudentOrder> studentOrderHistoryList = studentOrderService.searchAllStudentOrderByStudent(student);		
		return studentOrderHistoryList;
	}
	
	@RequestMapping("/studentOrder/{id}")
	public StudentOrder getStudentOrder(@PathVariable("id") Long id) {
		StudentOrder studentOrder = studentOrderService.searchStudentOrderById(id);
		return studentOrder;
	}
}
