/**
 * 
 */
package com.emms.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.emms.model.Course;
import com.emms.model.Department;
import com.emms.service.CourseService;

/**
 * @author Harshal
 *
 */

@RestController
@RequestMapping("/course")
public class CourseController {
	@Autowired
	private CourseService courseService;
	
	@RequestMapping (value="/courseList", method=RequestMethod.GET)
	public List<Course> getAllCourse() {
		return courseService.getAllCourse();
	}
	
	@RequestMapping (value="/addCourse", method=RequestMethod.POST)
	public ResponseEntity addNewCourse(@RequestBody Course course) {
		System.out.println(course);
		courseService.addCourse(course);
		return new ResponseEntity("Course added Success!", HttpStatus.OK);
	}
}
