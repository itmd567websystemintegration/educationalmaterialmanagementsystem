/**
 * 
 */
package com.emms.rest;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.emms.model.Book;
import com.emms.model.LabMaterial;
import com.emms.model.Notes;
import com.emms.model.SelectedBook;
import com.emms.model.SelectedLabMaterial;
import com.emms.model.SelectedNotes;
import com.emms.model.Student;
import com.emms.model.StudentCart;
import com.emms.service.ItemService;
import com.emms.service.SelectedItemService;
import com.emms.service.StudentCartService;
import com.emms.service.StudentService;

/**
 * @author Harshal
 *
 */
@RestController
@RequestMapping("/cart")
public class StudentCartController {
	@Autowired
	private StudentService studentService;
	
	@Autowired
	private ItemService itemService;
	
	@Autowired
	private SelectedItemService selectedItemService;
	
	@Autowired
	private StudentCartService studentCartService;
	
	@RequestMapping("/add/book")
	public ResponseEntity addItem (@RequestBody HashMap<String, String> mapper){
		String bookId = (String) mapper.get("bookId");
		String idToken = (String) mapper.get("id_token");
		//Student student = null;
		Student student = studentService.searchByStudentAccessToken(idToken);
		System.out.println("current student: "+student);
		Book book = itemService.searchBookById(Long.parseLong(bookId));
		SelectedBook selectedBook = selectedItemService.addToBookCart(book, student);
		return new ResponseEntity("Book Added Successfully!", HttpStatus.OK);
	}
	
	@RequestMapping("/add/notes")
	public ResponseEntity addNotes (@RequestBody HashMap<String, String> mapper){
		String notesId = (String) mapper.get("notesId");
		String idToken = (String) mapper.get("id_token");
		//Student student = null;
		Student student = studentService.searchByStudentAccessToken(idToken);
		System.out.println("current student: "+student);
		Notes notes = itemService.searchNotesById(Long.parseLong(notesId));
		SelectedNotes selectedNotes = selectedItemService.addToNotesCart(notes, student);
		return new ResponseEntity("Notes Added Successfully!", HttpStatus.OK);
	}
	
	@RequestMapping("/add/lab")
	public ResponseEntity addLab (@RequestBody HashMap<String, String> mapper){
		String labId = (String) mapper.get("labId");
		String idToken = (String) mapper.get("id_token");
		//Student student = null;
		Student student = studentService.searchByStudentAccessToken(idToken);
		System.out.println("current student: "+student);
		LabMaterial labMaterial = itemService.searchLabMaterialById(Long.parseLong(labId));
		SelectedLabMaterial selectedLabMaterial = selectedItemService.addToLabCart(labMaterial, student);
		return new ResponseEntity("Labs Added Successfully!", HttpStatus.OK);
	}
	
	@RequestMapping("/getSelectedBookList")
	public List<SelectedBook> getCartItemList(@RequestBody HashMap<String, String> mapper) {
		String idToken = (String) mapper.get("id_token");
		Student student = studentService.searchByStudentAccessToken(idToken);
		StudentCart studentCart = student.getStudentCart();
		
		List<SelectedBook> selectedBookList = selectedItemService.searchBookByStudentCart(studentCart);
		
		studentCartService.modifyStudentCart(studentCart);
		
		return selectedBookList;
	}
	
	@RequestMapping("/getSelectedNotesList")
	public List<SelectedNotes> getCartNotesList(@RequestBody HashMap<String, String> mapper) {
		String idToken = (String) mapper.get("id_token");
		Student student = studentService.searchByStudentAccessToken(idToken);
		StudentCart studentCart = student.getStudentCart();
		
		List<SelectedNotes> selectedNotesList = selectedItemService.searchNotesByStudentCart(studentCart);
		
		studentCartService.modifyStudentCart(studentCart);
		
		return selectedNotesList;
	}
	
	@RequestMapping("/getSelectedLabList")
	public List<SelectedLabMaterial> getCartLabList(@RequestBody HashMap<String, String> mapper) {
		String idToken = (String) mapper.get("id_token");
		Student student = studentService.searchByStudentAccessToken(idToken);
		StudentCart studentCart = student.getStudentCart();
		
		List<SelectedLabMaterial> selectedLabMaterialList = selectedItemService.searchLabMaterialByStudentCart(studentCart);
		
		studentCartService.modifyStudentCart(studentCart);
		
		return selectedLabMaterialList;
	}
	
	@RequestMapping("/getStudentCart")
	public StudentCart getStudentCart(@RequestBody HashMap<String, String> mapper) {
		String idToken = (String) mapper.get("id_token");
		Student student = studentService.searchByStudentAccessToken(idToken);
		System.out.println("current student: "+student);
		StudentCart studentCart = student.getStudentCart();
		studentCartService.modifyStudentCart(studentCart);
		return studentCart;
	}
	
	@RequestMapping("/remove/book")
	public ResponseEntity removeItem(@RequestBody HashMap<String, String> mapper) {
		String selectedBookId = (String) mapper.get("selectedBookId");
		String idToken = (String) mapper.get("id_token");
		selectedItemService.deleteSelectedBook(selectedItemService.searchById(Long.parseLong(selectedBookId)));
		return new ResponseEntity("SelectedBook Item Removed Successfully!", HttpStatus.OK);
	}
	
	@RequestMapping("/remove/notes")
	public ResponseEntity removeNotesItem(@RequestBody HashMap<String, String> mapper) {
		String selectedNotesId = (String) mapper.get("selectedNotesId");
		String idToken = (String) mapper.get("id_token");
		selectedItemService.deleteSelectedNotes(selectedItemService.searchByNotesId(Long.parseLong(selectedNotesId)));
		return new ResponseEntity("SelectedNotes Item Removed Successfully!", HttpStatus.OK);
	}
	
	@RequestMapping("/remove/labs")
	public ResponseEntity removeLabsItem(@RequestBody HashMap<String, String> mapper) {
		String selectedLabMaterialId = (String) mapper.get("selectedLabMaterialId");
		String idToken = (String) mapper.get("id_token");
		selectedItemService.deleteSelectedLabMaterial(selectedItemService.searchByLabId(Long.parseLong(selectedLabMaterialId)));
		return new ResponseEntity("SelectedLabMaterial Item Removed Successfully!", HttpStatus.OK);
	}
}
