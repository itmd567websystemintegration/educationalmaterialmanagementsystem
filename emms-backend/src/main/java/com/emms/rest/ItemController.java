
package com.emms.rest;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.emms.model.Book;
import com.emms.model.Course;
import com.emms.model.Department;
import com.emms.model.LabMaterial;
import com.emms.model.Notes;
import com.emms.model.Student;
import com.emms.service.CourseService;
import com.emms.service.DepartmentService;
import com.emms.service.ItemService;
import com.emms.service.StudentService;

@RestController
@RequestMapping("/item")
public class ItemController {

	@Autowired
	private ItemService itemService;

	@Autowired
	private DepartmentService departmentService;

	@Autowired
	private StudentService studentService;

	@Autowired
	private CourseService courseService;

	@RequestMapping(value = "/addBook", method = RequestMethod.POST)
	public Book addBook(@RequestBody Book book) {
		try {
			System.out.println("Book to add: " + book.getTitle());
			Department localDepartment = departmentService.findById(Long.valueOf(book.getDepartment().getId()));
			Course localCourse = courseService.findById(Long.valueOf(book.getCourse().getId()));
			Student student = studentService.searchdByStudentId(book.getStudent().getId());
			book.setDepartment(localDepartment);
			book.setCourse(localCourse);
			book.setStudent(student);
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println(book);
		return itemService.addBook(book);
	}

	@RequestMapping(value = "/addNotes", method = RequestMethod.POST)
	public Notes addNotes(@RequestBody Notes notes) {
		try {
			System.out.println("Book to add: " + notes.getTitle());
			Department localDepartment = departmentService.findById(Long.valueOf(notes.getDepartment().getId()));
			Course localCourse = courseService.findById(Long.valueOf(notes.getCourse().getId()));
			Student student = studentService.searchdByStudentId(notes.getStudent().getId());
			notes.setDepartment(localDepartment);
			notes.setCourse(localCourse);
			notes.setStudent(student);
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println(notes);
		return itemService.addNotes(notes);
	}
	
	@RequestMapping(value = "/addLabs", method = RequestMethod.POST)
	public LabMaterial addLabMaterial(@RequestBody LabMaterial labMaterial) {
		try {
			System.out.println("labMaterial to add: " + labMaterial.getTitle());
			Department localDepartment = departmentService.findById(Long.valueOf(labMaterial.getDepartment().getId()));
			Course localCourse = courseService.findById(Long.valueOf(labMaterial.getCourse().getId()));
			Student student = studentService.searchdByStudentId(labMaterial.getStudent().getId());
			labMaterial.setDepartment(localDepartment);
			labMaterial.setCourse(localCourse);
			labMaterial.setStudent(student);
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println(labMaterial);
		return itemService.addLabMaterial(labMaterial);
	}
	

	@RequestMapping("/{id}")
	public Book getBook(@PathVariable("id") Long id) {
		Book item = itemService.searchBookById(id);
		return item;
	}

	@RequestMapping("/notes/{id}")
	public Notes getNotes(@PathVariable("id") Long id) {
		Notes item = itemService.searchNotesById(id);
		return item;
	}
	
	@RequestMapping("/labs/{id}")
	public LabMaterial getLabMaterial(@PathVariable("id") Long id) {
		LabMaterial item = itemService.searchLabMaterialById(id);
		return item;
	}

	@RequestMapping(value = "/bookListByLoggedInStudent", method = RequestMethod.POST)
	public List<Book> getBookList(@RequestBody HashMap<String, String> mapper) {
		String idToken = (String) mapper.get("id_token");
		System.out.println("idToken: " + idToken);
		Student student = studentService.searchByStudentAccessToken(idToken);
		System.out.println("Current Student bookListByLoggedInStudent(): " + student);
		System.out.println("Current Student Book: " + student.getBookList());
		return student.getBookList();
	}
	
	@RequestMapping(value = "/notesListByLoggedInStudent", method = RequestMethod.POST)
	public List<Notes> getNotesList(@RequestBody HashMap<String, String> mapper) {
		String idToken = (String) mapper.get("id_token");
		System.out.println("idToken: " + idToken);
		Student student = studentService.searchByStudentAccessToken(idToken);
		System.out.println("Current Student notesListByLoggedInStudent(): " + student);
		System.out.println("Current Student Notes: " + student.getNotesList());
		return student.getNotesList();
	}
	
	@RequestMapping(value = "/labListByLoggedInStudent", method = RequestMethod.POST)
	public List<LabMaterial> getLabList(@RequestBody HashMap<String, String> mapper) {
		String idToken = (String) mapper.get("id_token");
		System.out.println("idToken: " + idToken);
		Student student = studentService.searchByStudentAccessToken(idToken);
		System.out.println("Current Student labListByLoggedInStudent(): " + student);
		System.out.println("Current Student Lab: " + student.getLabList());
		return student.getLabList();
	}
	

	@RequestMapping("/bookList")
	public List<Book> getBookList() {
		return itemService.searchAllBook();
	}
	
	@RequestMapping("/notesList")
	public List<Notes> getNotesList() {
		return itemService.searchAllNotes();
	}
	
	@RequestMapping("/labsList")
	public List<LabMaterial> getLabMeterialList() {
		return itemService.searchAllLabMaterial();
	}



	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public Book updateBook(@RequestBody Book book) {
		System.out.println("book to update: " + book);
		return itemService.addBook(book);
	}
	
	@RequestMapping(value = "/notes/update", method = RequestMethod.POST)
	public Notes updateNotes(@RequestBody Notes notes) {
		System.out.println("notes to update: " + notes);
		return itemService.addNotes(notes);
	}
	
	@RequestMapping(value = "/labs/update", method = RequestMethod.POST)
	public LabMaterial updateLabMaterial(@RequestBody LabMaterial labMaterial) {
		System.out.println("notes to update: " + labMaterial);
		return itemService.addLabMaterial(labMaterial);
	}	

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public ResponseEntity delete(@RequestBody String id) throws IOException {
		Book book = itemService.searchBookById(Long.parseLong(id));
		itemService.deleteBook(Long.parseLong(id));
		String fileName = id + ".png";
		Files.delete(
				Paths.get(System.getProperty("user.dir") + "\\src\\main\\resources\\static\\image\\book\\" + fileName));

		return new ResponseEntity("Book delete Success!", HttpStatus.OK);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value = "/notes/delete", method = RequestMethod.POST)
	public ResponseEntity deleteNotes(@RequestBody String id) throws IOException {
		Notes notes = itemService.searchNotesById(Long.parseLong(id));
		itemService.deleteNotes(Long.parseLong(id));
		String fileName = id + ".png";
		Files.delete(
				Paths.get(System.getProperty("user.dir") + "\\src\\main\\resources\\static\\image\\notes\\" + fileName));

		return new ResponseEntity("Notes delete Success!", HttpStatus.OK);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value = "/lab/delete", method = RequestMethod.POST)
	public ResponseEntity deleteLabs(@RequestBody String id) throws IOException {
		LabMaterial labMaterial = itemService.searchLabMaterialById(Long.parseLong(id));
		itemService.deleteLabMaterial(Long.parseLong(id));
		String fileName = id + ".png";
		Files.delete(
				Paths.get(System.getProperty("user.dir") + "\\src\\main\\resources\\static\\image\\labs\\" + fileName));

		return new ResponseEntity("Lab delete Success!", HttpStatus.OK);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value = "/add/image", method = RequestMethod.POST)
	public ResponseEntity uploadImage(@RequestParam("id") Long id, @RequestParam("itemType") String itemType, HttpServletResponse response,
			HttpServletRequest request) {
		try {
			storeImage(itemType, id, request);
			return new ResponseEntity("Image uploaded successfully!", HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity("Image upload failed!", HttpStatus.BAD_REQUEST);
		}
	}

	private void storeImage(String itemType, Long id, HttpServletRequest request) throws IOException {		
		if (itemType.equals("book")) {
			Book book = itemService.searchBookById(id);
			System.out.println("Image Id for book: "+book.getId());
			
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			final Map<String, MultipartFile> files = (Map<String, MultipartFile>) multipartRequest.getFileMap();
			
			for (MultipartFile file : files.values()) {	        	
				System.out.println("Adding image for book: "+book.getTitle());
				System.out.println("File Size: "+file.getSize()+" File Name:"+file.getName()+" "+file.getOriginalFilename());
				String fileName = id+".png";
				
				byte[] bytes = file.getBytes();
				BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File(System.getProperty("user.dir")+"\\src\\main\\resources\\static\\image\\book\\"+fileName)));
				stream.write(bytes);
				stream.close();
			}
		} else if (itemType.equals("notes")) {
			Notes notes = itemService.searchNotesById(id);
			System.out.println("Image Id for notes: "+notes.getId());
			
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			final Map<String, MultipartFile> files = (Map<String, MultipartFile>) multipartRequest.getFileMap();
			
			for (MultipartFile file : files.values()) {	        	
				System.out.println("Adding image for notes: "+notes.getTitle());
				System.out.println("File Size: "+file.getSize()+" File Name:"+file.getName()+" "+file.getOriginalFilename());
				String fileName = id+".png";
				
				byte[] bytes = file.getBytes();
				BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File(System.getProperty("user.dir")+"\\src\\main\\resources\\static\\image\\notes\\"+fileName)));
				stream.write(bytes);
				stream.close();
			}
		} else {
			LabMaterial labMaterial = itemService.searchLabMaterialById(id);
			System.out.println("Image Id for labs: "+labMaterial.getId());
			
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			final Map<String, MultipartFile> files = (Map<String, MultipartFile>) multipartRequest.getFileMap();
			
			for (MultipartFile file : files.values()) {	        	
				System.out.println("Adding image for book: "+labMaterial.getTitle());
				System.out.println("File Size: "+file.getSize()+" File Name:"+file.getName()+" "+file.getOriginalFilename());
				String fileName = id+".png";
				
				byte[] bytes = file.getBytes();
				BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File(System.getProperty("user.dir")+"\\src\\main\\resources\\static\\image\\labs\\"+fileName)));
				stream.write(bytes);
				stream.close();
			}
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value = "/update/image", method = RequestMethod.POST)
	public ResponseEntity updateImage(@RequestParam("id") Long id, @RequestParam("itemType") String itemType, HttpServletResponse response,
			HttpServletRequest request) {
		try {
			modifyImage(itemType, id, request);
			return new ResponseEntity("Image updated successfully!", HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity("Image updated failed!", HttpStatus.BAD_REQUEST);
		}
	}
	
	private void modifyImage(String itemType, Long id, HttpServletRequest request) throws IOException {		
		if (itemType.equals("book")) {
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			Iterator<String> it = multipartRequest.getFileNames();
			while (it.hasNext()) {
				MultipartFile multipartFile = multipartRequest.getFile(it.next());
				String fileName = id + ".png";

				Files.delete(Paths.get(
						System.getProperty("user.dir") + "\\src\\main\\resources\\static\\image\\book\\" + fileName));

				byte[] bytes = multipartFile.getBytes();
				BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File(
						System.getProperty("user.dir") + "\\src\\main\\resources\\static\\image\\book\\" + fileName)));
				stream.write(bytes);
				stream.close();
			}
		} else if (itemType.equals("notes")) {
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			Iterator<String> it = multipartRequest.getFileNames();
			while (it.hasNext()) {
				MultipartFile multipartFile = multipartRequest.getFile(it.next());
				String fileName = id + ".png";

				Files.delete(Paths.get(
						System.getProperty("user.dir") + "\\src\\main\\resources\\static\\image\\notes\\" + fileName));

				byte[] bytes = multipartFile.getBytes();
				BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File(
						System.getProperty("user.dir") + "\\src\\main\\resources\\static\\image\\notes\\" + fileName)));
				stream.write(bytes);
				stream.close();
			}
		} else {
			LabMaterial labMaterial = itemService.searchLabMaterialById(id);
			System.out.println("Image Id for labMaterial: "+labMaterial.getId());
			
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			final Map<String, MultipartFile> files = (Map<String, MultipartFile>) multipartRequest.getFileMap();
			
			for (MultipartFile file : files.values()) {	        	
				System.out.println("Adding image for labMaterial: "+labMaterial.getTitle());
				System.out.println("File Size: "+file.getSize()+" File Name:"+file.getName()+" "+file.getOriginalFilename());
				String fileName = id+".png";
				
				byte[] bytes = file.getBytes();
				BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File(System.getProperty("user.dir")+"\\src\\main\\resources\\static\\image\\labMaterial\\"+fileName)));
				stream.write(bytes);
				stream.close();
			}
		}
	}
}
