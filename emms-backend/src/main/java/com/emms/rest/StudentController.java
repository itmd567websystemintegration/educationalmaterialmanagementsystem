package com.emms.rest;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.emms.model.Student;
import com.emms.security.StudentRole;
import com.emms.security.config.ApplicationSecurityUtil;
import com.emms.service.StudentService;
import com.emms.util.SendHTMLEmail;

@RestController
@RequestMapping("/student")
public class StudentController {
	private static boolean isAdminCreated = false;

	@Autowired
	private StudentService studentService;

	@RequestMapping(value = "/addStudent", method = RequestMethod.POST)
	public ResponseEntity newUserPost(HttpServletRequest request, @RequestBody HashMap<String, String> mapper)
			throws Exception {
		String username = mapper.get("userName");
		String userEmail = mapper.get("studentEmailId");
		String lastName = mapper.get("familyName");
		String firstName = mapper.get("givenName");
		String authToken = mapper.get("id_token");

		System.out.println("Student Username: "+username);
		System.out.println("Student userEmail: "+userEmail);
		System.out.println("Student lastName: "+lastName);
		System.out.println("Student firstName: "+firstName);
		System.out.println("Student id_token: "+authToken);
		Student existingStudent = studentService.searchByStudentUserName(username);
		if (existingStudent != null) {
			existingStudent.setAccessToken(authToken);
			studentService.add(existingStudent);
			return new ResponseEntity("User updated successfully!", HttpStatus.OK);
		}

		Student student = new Student();
		student.setUserName(username);
		student.setEmailId(userEmail);
		student.setFirstName(firstName);
		student.setLastName(lastName);
		student.setAccessToken(authToken);
		if(!isAdminCreated) {
			student.setEnabled(true);
			isAdminCreated = true;			 
		}else {
			student.setEnabled(false);
		}

		String password = ApplicationSecurityUtil.randomPassword();
		System.out.println("Password: "+password);
		String encryptedPassword = ApplicationSecurityUtil.passwordEncoder().encode(password);
		student.setPassword(encryptedPassword);

		com.emms.security.Role role = new com.emms.security.Role();
		role.setRoleId(1);
		role.setName("ROLE_USER");
		Set<StudentRole> studentRoles = new HashSet<>();
		studentRoles.add(new StudentRole(student, role));
		studentService.createStudent(student, studentRoles);

		SendHTMLEmail sendHTMLEmail = new SendHTMLEmail();
		String emailMessage = sendHTMLEmail.getMessage(student, SendHTMLEmail.STUDENT_REG);
		sendHTMLEmail.sendMessage(student.getEmailId(), emailMessage, SendHTMLEmail.STUDENT_REG);
		return new ResponseEntity("User Added Successfully!", HttpStatus.OK);

	}

	@RequestMapping(value="/updateStudentDetails", method=RequestMethod.POST)
	public ResponseEntity profileInfo(
			@RequestBody HashMap<String, Object> mapper
			) throws Exception{
		String idToken = (String) mapper.get("id_token");
		int id = (Integer) mapper.get("id");
		String email = (String) mapper.get("email");
		String username = (String) mapper.get("username");
		String firstName = (String) mapper.get("firstName");
		String lastName = (String) mapper.get("lastName");
		String newPassword = (String) mapper.get("newPassword");
		String currentPassword = (String) mapper.get("currentPassword");
		String phone = (String) mapper.get("phone");

		Student currentStudent = studentService.searchdByStudentId(Long.valueOf(id));

		if(currentStudent == null) {
			throw new Exception ("Student not found");
		}

		if(studentService.searchByStudentEmail(email) != null) {
			if(studentService.searchByStudentEmail(email).getId() != currentStudent.getId()) {
				return new ResponseEntity("Email not found!", HttpStatus.BAD_REQUEST);
			}
		}

		if(studentService.searchByStudentUserName(username) != null) {
			if(studentService.searchByStudentUserName(username).getId() != currentStudent.getId()) {
				return new ResponseEntity("Username not found!", HttpStatus.BAD_REQUEST);
			}
		}

		

		BCryptPasswordEncoder passwordEncoder = ApplicationSecurityUtil.passwordEncoder();
		String dbPassword = currentStudent.getPassword();

		if(null != currentPassword){
			if(passwordEncoder.matches(currentPassword, dbPassword)) {
				if(newPassword != null && !newPassword.isEmpty() && !newPassword.equals("")) {
					currentStudent.setPassword(passwordEncoder.encode(newPassword));
				}
				currentStudent.setEmailId(email);
			} else {
				return new ResponseEntity("Incorrect current password!", HttpStatus.BAD_REQUEST);
			}
		}


		currentStudent.setFirstName(firstName);
		currentStudent.setLastName(lastName);
		currentStudent.setUserName(username);
		currentStudent.setPhone(phone);
		currentStudent.setAccessToken(idToken);


		studentService.add(currentStudent);

		return new ResponseEntity("Update Success", HttpStatus.OK);
	}

	@RequestMapping(value="/getCurrentStudent", method=RequestMethod.POST)
	public Student getCurrentStudent(@RequestBody HashMap<String, String> mapper) {
		Student student = null;
		try {
			student = new Student();
			String idToken = (String) mapper.get("id_token");
			System.out.println("idToken: "+idToken);
			if (null != idToken) {
				student = studentService.searchByStudentAccessToken(idToken);
			}
			System.out.println("Current Student: "+student);
		} catch (Exception e) {
			e.getMessage();
		}
		return student;
	}

}
