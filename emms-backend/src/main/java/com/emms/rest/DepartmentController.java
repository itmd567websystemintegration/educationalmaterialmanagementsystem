/**
 * 
 */
package com.emms.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.emms.model.Book;
import com.emms.model.Course;
import com.emms.model.Department;
import com.emms.model.Notes;
import com.emms.model.Student;
import com.emms.service.DepartmentService;

/**
 * @author Harshal
 *
 */

@RestController
@RequestMapping("/department")
public class DepartmentController {
	@Autowired
	private DepartmentService departmentService;
	
	@RequestMapping (value="/departmentList", method=RequestMethod.GET)
	public List<Department> getAllDepartment() {
		return departmentService.getAllDepartment();
	}
	
	@RequestMapping (value="/addDepartment", method=RequestMethod.POST)
	public ResponseEntity addNewDepartment(@RequestBody Department department) {
		System.out.println(department);
		 departmentService.addDepartment(department);
		 return new ResponseEntity("Department added Success!", HttpStatus.OK);
	}
}
