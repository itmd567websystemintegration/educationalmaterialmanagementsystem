
package com.emms.rest;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.emms.model.BillingAddress;
import com.emms.model.DeliveryAddress;
import com.emms.model.Payment;
import com.emms.model.SelectedBook;
import com.emms.model.Student;
import com.emms.model.StudentCart;
import com.emms.model.StudentOrder;
import com.emms.service.SelectedItemService;
import com.emms.service.StudentCartService;
import com.emms.service.StudentOrderService;
import com.emms.service.StudentService;
import com.emms.util.SendHTMLEmail;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author Harshal
 *
 */
@RestController
@RequestMapping("/checkout")
public class StudentCheckoutOrderController {
	
	@Autowired
	private StudentService studentService;
	
	@Autowired
	private SelectedItemService selectedItemService;
	
	@Autowired
	private StudentOrderService studentOrderService;
	
	@Autowired
	private StudentCartService studentCartService;
	
	private StudentOrder studentOrder = new StudentOrder();
	
	@RequestMapping(value = "/placeOrder", method=RequestMethod.POST)
	public StudentOrder checkoutPost(@RequestBody HashMap<String, Object> mapper) throws Exception{
		ObjectMapper om = new ObjectMapper();
		
		DeliveryAddress deliveryAddress = om.convertValue(mapper.get("deliveryAddress"), DeliveryAddress.class);
		BillingAddress billingAddress = om.convertValue(mapper.get("billingAddress"), BillingAddress.class);
		Payment payment = om.convertValue(mapper.get("payment"), Payment.class);
		String delvieryMethod = (String) mapper.get("deliveryMethod");
		String idToken = (String) mapper.get("idToken");
		if(deliveryAddress != null && billingAddress != null && payment != null 
				&& delvieryMethod != null && idToken != null) {
			StudentCart studentCart = studentService.searchByStudentAccessToken(idToken).getStudentCart();
			System.out.println("studentCart: "+studentCart.getId());
			List<SelectedBook> selectedBookList = selectedItemService.searchBookByStudentCart(studentCart);
			System.out.println("selectedBookList: "+selectedBookList);
			Student student = studentService.searchByStudentAccessToken(idToken);

			StudentOrder studentOrder = studentOrderService.checkOutOrder(student, studentCart, payment, billingAddress, deliveryAddress, delvieryMethod);
			
			studentCartService.deleteStudentCart(studentCart);			
			
			this.studentOrder = studentOrder;
		}else {
			new ResponseEntity("Missing fields required", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
		return studentOrder;
		
	}
}
