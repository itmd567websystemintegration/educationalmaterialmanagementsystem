/*
 * All materials herein: Copyright (c) 2017 Worldnet TPS Ltd. All Rights Reserved.
 *
 *  These materials are owned by Worldnet TPS Ltd and are protected by copyright laws
 *  and international copyright treaties, as well as other intellectual property laws
 *  and treaties.
 *
 *  All right, title and interest in the copyright, confidential information,
 *  patents, design rights and all other intellectual property rights of
 *  whatsoever nature in and to these materials are and shall remain the sole
 *  and exclusive property of Worldnet TPS Ltd.
 */
package com.emms.payments.protocol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang3.StringUtils;

@XmlRootElement(name = "ProcessCardResponse")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "requestResult", "transaction"})
public class ProcessCardResponse {
    @XmlElement(name = "RequestResult")
    private RequestResult requestResult;
    @XmlElement(name = "Transaction")
    private Transaction transaction;

    public static final String TRANSACTION_RESP_VALUE = "SUCCESS";
    public static final String PAYMENT_RESP_CODE = "00";

    public RequestResult getRequestResult() {
        return requestResult;
    }

    public void setRequestResult(RequestResult requestResult) {
        this.requestResult = requestResult;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

    public boolean isSuccessful() {
        return (StringUtils.isNotEmpty(transaction.getResultCode().getResultCode()) && PAYMENT_RESP_CODE.equals(transaction.getResultCode().getResultCode())) &&
                StringUtils.isNotEmpty(transaction.getResultCode().getResultValue()) &&
                TRANSACTION_RESP_VALUE.equals(transaction.getResultCode().getResultValue());
    }

    @Override
    public String toString() {
        return "ProcessCardResponse{" +
                "requestResult=" + requestResult +
                ", transaction=" + transaction +
                '}';
    }
}
