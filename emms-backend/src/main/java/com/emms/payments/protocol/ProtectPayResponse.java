
package com.emms.payments.protocol;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;

public class ProtectPayResponse {
    private static Log log = LogFactory.getLog(ProtectPayResponse.class);
    private ProcessCardResponse processPaymentMethodResponse;
    private Document soapResponseDocument = null;

    public static final String RESULT_CODE_EXP = "/*/*//*[local-name()='ResultCode']/text()";
    public static final String RESULT_VALUE_EXP = "/*/*//*[local-name()='ResultValue']/text()";
    public static final String RESULT_MESSAGE_EXP = "/*/*//*[local-name()='ResultMessage']/text()";
    public static final String TXN_AUTH_CODE_EXP = "/*/*/*[local-name()='Transaction']/*[local-name()='AuthorizationCode']/text()";
    public static final String TXN_RESP_AVS_CODE = "/*/*/*[local-name()='Transaction']/*[local-name()='AVSCode']/text()";
    public static final String TXN_RESP_CVV2_CODE = "/*/*/*[local-name()='Transaction']/*[local-name()='CVVResponseCode']/text()";
    public static final String TXN_HISTORY_ID_EXP = "/*/*/*[local-name()='Transaction']/*[local-name()='TransactionHistoryId']/text()";
    public static final String TXN_ID = "/*/*/*[local-name()='Transaction']/*[local-name()='TransactionId']/text()";


    public static final String TRANSACTION_RESP_VALUE = "SUCCESS";
    public static final String PAYMENT_RESP_CODE = "00";

    public ProcessCardResponse getProcessPaymentMethodResponse() {
        return processPaymentMethodResponse;
    }

    public void setProcessPaymentMethodResponse(ProcessCardResponse processPaymentMethodResponse) {
        this.processPaymentMethodResponse = processPaymentMethodResponse;
    }


    public Document getSoapResponseDocument() {
        return soapResponseDocument;
    }

    public void setSoapResponseDocument(Document soapResponseDocument) {
        this.soapResponseDocument = soapResponseDocument;
    }

    public boolean isSuccessful() throws Exception {
        return ((!StringUtils.isBlank(getXmlElementByExpression(RESULT_CODE_EXP))) && PAYMENT_RESP_CODE.equals(getXmlElementByExpression(RESULT_CODE_EXP))) &&
                (!StringUtils.isBlank(getXmlElementByExpression(RESULT_VALUE_EXP))) &&
                TRANSACTION_RESP_VALUE.equals(getXmlElementByExpression(RESULT_VALUE_EXP));
    }

    public String getXmlElementByExpression(String expression) throws Exception {
        if (soapResponseDocument != null) {
            XPathFactory factory = XPathFactory.newInstance();
            XPath xpath = factory.newXPath();
            XPathExpression expr = xpath.compile(expression);
            return (String) expr.evaluate(soapResponseDocument, XPathConstants.STRING);
        } else {
            String message = "protectpay soap response document is null";
            log.error(message);
            throw new Exception(message);
        }
    }
}
