
package com.emms.payments.protocol;

import javax.xml.bind.annotation.XmlRegistry;

@XmlRegistry
public class ObjectFactory {


    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ProcessCard }
     */
    public ProcessCard createProcessPaymentMethodRequest() {
        return new ProcessCard();
    }

}
