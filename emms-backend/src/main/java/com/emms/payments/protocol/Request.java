
package com.emms.payments.protocol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "request")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"card", "transaction", "inputIpAddress", "sessionId"})
public class Request {
    @XmlElement(name = "InputIpAddress ", namespace = ProtectPayNamespaceMapper.PROP_URI)
    private String inputIpAddress;
    @XmlElement(name = "SessionId ", namespace = ProtectPayNamespaceMapper.PROP_URI)
    private String sessionId;
    @XmlElement(name = "Transaction", namespace = ProtectPayNamespaceMapper.PROP_URI)
    private Transaction transaction;
    @XmlElement(name = "Card", namespace = ProtectPayNamespaceMapper.PROP_URI)
    private Card card;

    public String getInputIpAddress() {
        return inputIpAddress;
    }

    public void setInputIpAddress(String inputIpAddress) {
        this.inputIpAddress = inputIpAddress;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    @Override
    public String toString() {
        return "Request [inputIpAddress=" + inputIpAddress + ", sessionId="
                + sessionId + ", transaction=" + transaction + ", card=" + card
                + "]";
    }
    
}
