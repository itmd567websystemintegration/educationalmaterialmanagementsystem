
package com.emms.payments.protocol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "Transaction")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = ProtectPayNamespaceMapper.CONTRACT_URI, propOrder = { "amount", "refundAmount", "originalTransactionId", "amountSpecified",
        "comment1", "comment2", "currencyCode", "invoice", "payerAccountId", "merchantProfileId", "avsCode", "authorizationCode", "currencyConversionRate",
        "currencyConvertedAmount", "currencyConvertedCurrencyCode", "resultCode", "transactionHistoryId", "transactionId",
        "transactionResult", "cvvResponseCode", "grossAmt", "netAmt", "perTransFee", "rate", "grossAmtLessNetAmt"})
public class Transaction {

    // sale request fields
    @XmlElement(name = "Amount", namespace = ProtectPayNamespaceMapper.TYPES_URI)
    private String amount;
    @XmlElement(name = "Comment1", namespace = ProtectPayNamespaceMapper.TYPES_URI)
    private String comment1;
    @XmlElement(name = "Comment2", namespace = ProtectPayNamespaceMapper.TYPES_URI)
    private String comment2;
    @XmlElement(name = "CurrencyCode", namespace = ProtectPayNamespaceMapper.TYPES_URI)
    private String currencyCode;
    @XmlElement(name = "Invoice", namespace = ProtectPayNamespaceMapper.TYPES_URI)
    private String invoice;
    @XmlElement(name = "PayerAccountId", namespace = ProtectPayNamespaceMapper.TYPES_URI)
    private String payerAccountId;
    @XmlElement(name = "MerchantProfileId", namespace = ProtectPayNamespaceMapper.TYPES_URI)
    private String merchantProfileId;

    // refund request fields
    @XmlElement(name = "Amount", namespace = ProtectPayNamespaceMapper.PROP_URI)
    private String refundAmount;
    @XmlElement(name = "OriginalTransactionId", namespace = ProtectPayNamespaceMapper.PROP_URI)
    private String originalTransactionId;
    @XmlElement(name = "AmountSpecified", namespace = ProtectPayNamespaceMapper.PROP_URI)
    private boolean amountSpecified;


    // response fields
    @XmlElement(name = "AVSCode")
    private String avsCode;
    @XmlElement(name = "AuthorizationCode")
    private String authorizationCode;
    @XmlElement(name = "CurrencyConversionRate")
    private String currencyConversionRate;
    @XmlElement(name = "CurrencyConvertedAmount")
    private String currencyConvertedAmount;
    @XmlElement(name = "CurrencyConvertedCurrencyCode")
    private String currencyConvertedCurrencyCode;
    @XmlElement(name = "ResultCode")
    private ResultCode  resultCode;
    @XmlElement(name = "TransactionHistoryId")
    private String transactionHistoryId;
    @XmlElement(name = "TransactionId")
    private String transactionId;
    @XmlElement(name = "TransactionResult")
    private String transactionResult;
    @XmlElement(name = "CVVResponseCode")
    private String cvvResponseCode;
    @XmlElement(name = "GrossAmt")
    private String grossAmt;
    @XmlElement(name = "NetAmt")
    private String netAmt;
    @XmlElement(name = "PerTransFee")
    private String perTransFee;
    @XmlElement(name = "Rate")
    private String rate;
    @XmlElement(name = "GrossAmtLessNetAmt")
    private String grossAmtLessNetAmt;

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getComment1() {
        return comment1;
    }

    public void setComment1(String comment1) {
        this.comment1 = comment1;
    }

    public boolean isAmountSpecified() {
        return amountSpecified;
    }

    public void setAmountSpecified(boolean amountSpecified) {
        this.amountSpecified = amountSpecified;
    }

    public String getComment2() {
        return comment2;
    }

    public void setComment2(String comment2) {
        this.comment2 = comment2;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getInvoice() {
        return invoice;
    }

    public void setInvoice(String invoice) {
        this.invoice = invoice;
    }

    public String getPayerAccountId() {
        return payerAccountId;
    }

    public void setPayerAccountId(String payerAccountId) {
        this.payerAccountId = payerAccountId;
    }

    public String getMerchantProfileId() {
        return merchantProfileId;
    }

    public void setMerchantProfileId(String merchantProfileId) {
        this.merchantProfileId = merchantProfileId;
    }

    public String getRefundAmount() {
        return refundAmount;
    }

    public void setRefundAmount(String refundAmount) {
        this.refundAmount = refundAmount;
    }

    public String getOriginalTransactionId() {
        return originalTransactionId;
    }

    public void setOriginalTransactionId(String originalTransactionId) {
        this.originalTransactionId = originalTransactionId;
    }

    public String getAvsCode() {
        return avsCode;
    }

    public void setAvsCode(String avsCode) {
        this.avsCode = avsCode;
    }

    public String getAuthorizationCode() {
        return authorizationCode;
    }

    public void setAuthorizationCode(String authorizationCode) {
        this.authorizationCode = authorizationCode;
    }

    public String getCurrencyConversionRate() {
        return currencyConversionRate;
    }

    public void setCurrencyConversionRate(String currencyConversionRate) {
        this.currencyConversionRate = currencyConversionRate;
    }

    public String getCurrencyConvertedAmount() {
        return currencyConvertedAmount;
    }

    public void setCurrencyConvertedAmount(String currencyConvertedAmount) {
        this.currencyConvertedAmount = currencyConvertedAmount;
    }

    public String getCurrencyConvertedCurrencyCode() {
        return currencyConvertedCurrencyCode;
    }

    public void setCurrencyConvertedCurrencyCode(String currencyConvertedCurrencyCode) {
        this.currencyConvertedCurrencyCode = currencyConvertedCurrencyCode;
    }

    public ResultCode getResultCode() {
        return resultCode;
    }

    public void setResultCode(ResultCode resultCode) {
        this.resultCode = resultCode;
    }

    public String getTransactionHistoryId() {
        return transactionHistoryId;
    }

    public void setTransactionHistoryId(String transactionHistoryId) {
        this.transactionHistoryId = transactionHistoryId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getTransactionResult() {
        return transactionResult;
    }

    public void setTransactionResult(String transactionResult) {
        this.transactionResult = transactionResult;
    }

    public String getCvvResponseCode() {
        return cvvResponseCode;
    }

    public void setCvvResponseCode(String cvvResponseCode) {
        this.cvvResponseCode = cvvResponseCode;
    }

    public String getGrossAmt() {
        return grossAmt;
    }

    public void setGrossAmt(String grossAmt) {
        this.grossAmt = grossAmt;
    }

    public String getNetAmt() {
        return netAmt;
    }

    public void setNetAmt(String netAmt) {
        this.netAmt = netAmt;
    }

    public String getPerTransFee() {
        return perTransFee;
    }

    public void setPerTransFee(String perTransFee) {
        this.perTransFee = perTransFee;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getGrossAmtLessNetAmt() {
        return grossAmtLessNetAmt;
    }

    public void setGrossAmtLessNetAmt(String grossAmtLessNetAmt) {
        this.grossAmtLessNetAmt = grossAmtLessNetAmt;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "amount='" + amount + '\'' +
                ", comment1='" + comment1 + '\'' +
                ", comment2='" + comment2 + '\'' +
                ", currencyCode='" + currencyCode + '\'' +
                ", invoice='" + invoice + '\'' +
                ", payerAccountId='" + payerAccountId + '\'' +
                ", merchantProfileId='" + merchantProfileId + '\'' +
                ", refundAmount='" + refundAmount + '\'' +
                ", originalTransactionId='" + originalTransactionId + '\'' +
                ", amountSpecified=" + amountSpecified +
                ", avsCode='" + avsCode + '\'' +
                ", authorizationCode='" + authorizationCode + '\'' +
                ", currencyConversionRate='" + currencyConversionRate + '\'' +
                ", currencyConvertedAmount='" + currencyConvertedAmount + '\'' +
                ", currencyConvertedCurrencyCode='" + currencyConvertedCurrencyCode + '\'' +
                ", resultCode=" + resultCode +
                ", transactionHistoryId='" + transactionHistoryId + '\'' +
                ", transactionId='" + transactionId + '\'' +
                ", transactionResult='" + transactionResult + '\'' +
                ", cvvResponseCode='" + cvvResponseCode + '\'' +
                ", grossAmt='" + grossAmt + '\'' +
                ", netAmt='" + netAmt + '\'' +
                ", perTransFee='" + perTransFee + '\'' +
                ", rate='" + rate + '\'' +
                ", grossAmtLessNetAmt='" + grossAmtLessNetAmt + '\'' +
                '}';
    }
}
