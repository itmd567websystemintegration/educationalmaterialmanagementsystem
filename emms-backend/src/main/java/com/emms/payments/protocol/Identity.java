
package com.emms.payments.protocol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "authenticationToken", "billerAccountId"})
public class Identity {
    @XmlElement(name = "AuthenticationToken", namespace = ProtectPayNamespaceMapper.TYPES_URI)
    private String authenticationToken;
    @XmlElement(name = "BillerAccountId", namespace = ProtectPayNamespaceMapper.TYPES_URI)
    private String billerAccountId;

    public String getAuthenticationToken() {
        return authenticationToken;
    }

    public void setAuthenticationToken(String authenticationToken) {
        this.authenticationToken = authenticationToken;
    }

    public String getBillerAccountId() {
        return billerAccountId;
    }

    public void setBillerAccountId(String billerAccountId) {
        this.billerAccountId = billerAccountId;
    }

    @Override
    public String toString() {
        return "Identity{" +
                "authenticationToken='" + authenticationToken + '\'' +
                ", billerAccountId='" + billerAccountId + '\'' +
                '}';
    }
}
