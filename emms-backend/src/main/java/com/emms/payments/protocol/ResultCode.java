
package com.emms.payments.protocol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "ResultCode")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "resultCode", "resultMessage", "resultValue"})
public class ResultCode {
    @XmlElement(name = "ResultCode")
    private String resultCode;
    @XmlElement(name = "ResultMessage")
    private String resultMessage;
    @XmlElement(name = "ResultValue")
    private String resultValue;

    public String getResultCode() {
        return resultCode;
    }

    public void setResultCode(String resultCode) {
        this.resultCode = resultCode;
    }

    public String getResultMessage() {
        return resultMessage;
    }

    public void setResultMessage(String resultMessage) {
        this.resultMessage = resultMessage;
    }

    public String getResultValue() {
        return resultValue;
    }

    public void setResultValue(String resultValue) {
        this.resultValue = resultValue;
    }

    @Override
    public String toString() {
        return "ResultCode{" +
                "resultCode='" + resultCode + '\'' +
                ", resultMessage='" + resultMessage + '\'' +
                ", resultValue='" + resultValue + '\'' +
                '}';
    }
}
