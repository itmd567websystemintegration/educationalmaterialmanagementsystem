
package com.emms.payments.protocol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "ProcessCard", namespace = ProtectPayNamespaceMapper.CONTRACT_URI)
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = ProtectPayNamespaceMapper.CONTRACT_URI, propOrder = {"id", "request"})
public class ProcessCard {
    @XmlElement(name = "identification", namespace = ProtectPayNamespaceMapper.CONTRACT_URI)
    private Identity id;
    @XmlElement(name = "request", namespace = ProtectPayNamespaceMapper.CONTRACT_URI)
    private Request request;

    public Identity getId() {
        return id;
    }

    public void setId(Identity id) {
        this.id = id;
    }

    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }

    @Override
    public String toString() {
        return "ProcessCard{" +
                "id=" + id +
                ", request=" + request +
                '}';
    }
}
