
@XmlSchema(
        xmlns={
                @XmlNs(prefix="prop", namespaceURI=ProtectPayNamespaceMapper.PROP_URI),
                @XmlNs(prefix="con", namespaceURI=ProtectPayNamespaceMapper.CONTRACT_URI),
        },
        elementFormDefault= XmlNsForm.UNQUALIFIED)
package com.emms.payments.protocol;

import javax.xml.bind.annotation.XmlNs;
import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlSchema;