
package com.emms.payments.protocol;

public class ProtectPayRequest {

    public static final int TXN_TYPE_SALE = 1;


    private int transactionType;
    private ProcessCard processCard;

    public int getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(int transactionType) {
        this.transactionType = transactionType;
    }

    public ProcessCard getProcessCard() {
        return processCard;
    }

    public void setProcessCard(ProcessCard processCard) {
        this.processCard = processCard;
    }
}
