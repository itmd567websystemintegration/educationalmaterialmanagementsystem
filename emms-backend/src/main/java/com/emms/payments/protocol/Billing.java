
package com.emms.payments.protocol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement()
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "address1", "address2", "address3", "city", "country", "email", "state", "telephoneNumber", "zipCode"})
public class Billing {

    @XmlElement(name = "Address1", namespace = ProtectPayNamespaceMapper.TYPES_URI)
    private String address1;
    @XmlElement(name = "Address2", namespace = ProtectPayNamespaceMapper.TYPES_URI)
    private String address2;
    @XmlElement(name = "Address3", namespace = ProtectPayNamespaceMapper.TYPES_URI)
    private String address3;
    @XmlElement(name = "City", namespace = ProtectPayNamespaceMapper.TYPES_URI)
    private String city;
    @XmlElement(name = "Country", namespace = ProtectPayNamespaceMapper.TYPES_URI)
    private String country;
    @XmlElement(name = "email", namespace = ProtectPayNamespaceMapper.TYPES_URI)
    private String email;
    @XmlElement(name = "State", namespace = ProtectPayNamespaceMapper.TYPES_URI)
    private String state;
    @XmlElement(name = "TelephoneNumber", namespace = ProtectPayNamespaceMapper.TYPES_URI)
    private String telephoneNumber;
    @XmlElement(name = "ZipCode", namespace = ProtectPayNamespaceMapper.TYPES_URI)
    private String zipCode;

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAddress3() {
        return address3;
    }

    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    @Override
    public String toString() {
        return "Billing{" +
                "address1='" + address1 + '\'' +
                ", address2='" + address2 + '\'' +
                ", address3='" + address3 + '\'' +
                ", city='" + city + '\'' +
                ", country='" + country + '\'' +
                ", email='" + email + '\'' +
                ", state='" + state + '\'' +
                ", telephoneNumber='" + telephoneNumber + '\'' +
                ", zipCode='" + zipCode + '\'' +
                '}';
    }
}
