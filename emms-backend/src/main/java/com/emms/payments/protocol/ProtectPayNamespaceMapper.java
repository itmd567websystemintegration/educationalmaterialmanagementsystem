
package com.emms.payments.protocol;


import com.sun.xml.internal.bind.marshaller.NamespacePrefixMapper;

@SuppressWarnings("restriction")
public class ProtectPayNamespaceMapper  extends NamespacePrefixMapper {

    public static final String CONTRACT_PREFIX = "con";
    public static final String CONTRACT_URI = "http://propay.com/SPS/contracts";
    public static final String TYPES_PREFIX = "typ";
    public static final String TYPES_URI = "http://propay.com/SPS/types";
    public static final String PROP_PREFIX = "prop";
    public static final String PROP_URI = "http://schemas.datacontract.org/2004/07/Propay.Contracts.SPS.External";
    public static final String SOAPACTION_URI = "http://propay.com/SPS/contracts/SPSService";



    @Override
    public String getPreferredPrefix(String namespaceUri, String suggestion, boolean requirePrefix) {
        if(CONTRACT_URI.equals(namespaceUri)) {
            return CONTRACT_PREFIX;
        } else if(TYPES_URI.equals(namespaceUri)) {
            return TYPES_PREFIX;
        } else if(PROP_URI.equals(namespaceUri)) {
            return PROP_PREFIX;
        }
        return suggestion;
    }

    @Override
    public String[] getPreDeclaredNamespaceUris() {
        return new String[] { CONTRACT_URI, TYPES_URI, PROP_URI };
    }

}
