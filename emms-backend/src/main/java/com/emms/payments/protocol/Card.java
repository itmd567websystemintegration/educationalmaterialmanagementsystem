
package com.emms.payments.protocol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "Card")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"accountNumber", "billing", "cardholderName", "expirationDate"})
public class Card {
    // process card method call
    @XmlElement(name = "AccountNumber", namespace = ProtectPayNamespaceMapper.PROP_URI)
    private String accountNumber;
    @XmlElement(name = "BillingInformation", namespace = ProtectPayNamespaceMapper.PROP_URI)
    private Billing billing;
    @XmlElement(name = "CardholderName", namespace = ProtectPayNamespaceMapper.PROP_URI)
    private String cardholderName;
    @XmlElement(name = "ExpirationDate", namespace = ProtectPayNamespaceMapper.PROP_URI)
    private String expirationDate;

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getCardholderName() {
        return cardholderName;
    }

    public void setCardholderName(String cardholderName) {
        this.cardholderName = cardholderName;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    public Billing getBilling() {
        return billing;
    }

    public void setBilling(Billing billing) {
        this.billing = billing;
    }

    @Override
    public String toString() {
        return "Card{" +
                "accountNumber='" + accountNumber + '\'' +
                ", cardholderName='" + cardholderName + '\'' +
                ", expirationDate='" + expirationDate + '\'' +
                ", billing=" + billing +
                '}';
    }
}
