package com.emms.payments;

import java.math.BigDecimal;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.emms.model.StudentOrder;
import com.emms.payments.protocol.Billing;
import com.emms.payments.protocol.Card;
import com.emms.payments.protocol.Identity;
import com.emms.payments.protocol.ProcessCard;
import com.emms.payments.protocol.ProtectPayRequest;
import com.emms.payments.protocol.ProtectPayResponse;
import com.emms.payments.protocol.Request;
import com.emms.payments.protocol.Transaction;
import com.emms.util.SendHTMLEmail;


public class ProtectPayAuth{

    private static Log log = LogFactory.getLog(ProtectPayAuth.class);
    
    private StudentOrder studentOrder;
    
    public ProtectPayAuth(StudentOrder studentOrder) {
       this.studentOrder = studentOrder;
    }

    public boolean performSale() throws Exception {    	
        ProtectPayRequest protectPayRequest = new ProtectPayRequest();
        protectPayRequest.setTransactionType(ProtectPayRequest.TXN_TYPE_SALE);
        ProcessCard processSaleRequest =  new ProcessCard();
        protectPayRequest.setProcessCard(processSaleRequest);
        populateCrendentials(protectPayRequest);
        // create transaction
        Transaction transaction = new Transaction();
        BigDecimal x100 = new BigDecimal(100);
        System.out.println("order amount(usd): "+ this.studentOrder.getOrderTotal());
        BigDecimal rounded = this.studentOrder.getOrderTotal().setScale(2, BigDecimal.ROUND_CEILING);
        BigDecimal bigDecimalInCents = rounded.multiply(x100);
        int cents = bigDecimalInCents.intValueExact();
        System.out.println("order amount(cents): "+ String.valueOf(cents));
        transaction.setAmount(String.valueOf(cents));
        transaction.setComment1("student order");
        transaction.setCurrencyCode("USD");
        transaction.setInvoice(String.format("%06d", this.studentOrder.getId()));
        // set credit card details
        Card card = new Card();
        card.setAccountNumber(this.studentOrder.getPayment().getCardNumber());
        card.setExpirationDate(String.valueOf(this.studentOrder.getPayment().getExpiryMonth())
        		+String.valueOf(this.studentOrder.getPayment().getExpiryYear()));
        card.setCardholderName(this.studentOrder.getPayment().getCardHolderName());

        // set billing details to card details
        Billing billing = new Billing();
        billing.setAddress1(this.studentOrder.getStudentBillingAddress().getBillingAddressStreet1());
        billing.setCountry(this.studentOrder.getStudentBillingAddress().getBillingAddressCountry());
        billing.setCity(this.studentOrder.getStudentBillingAddress().getBillingAddressCity());
        billing.setState(this.studentOrder.getStudentBillingAddress().getBillingAddressState());
        billing.setEmail(this.studentOrder.getStudent().getEmailId());
        billing.setTelephoneNumber(this.studentOrder.getStudent().getPhone());
        billing.setZipCode(this.studentOrder.getStudentBillingAddress().getBillingAddressPostalcode());
        card.setBilling(billing);
        // set card  and transaction details to request
        Request request = new Request();
        request.setTransaction(transaction);
        request.setCard(card);
        // set request to soap body
        processSaleRequest.setRequest(request);

        ProtectPayResponse response = sendRequest(protectPayRequest);
        
        return processResponse(response);
    }

    private void populateCrendentials(ProtectPayRequest protectPayRequest) {
        Identity identity = new Identity();
        identity.setAuthenticationToken("16dfe8d7-889b-4380-925f-9c2c6ea4d930");
        identity.setBillerAccountId("2781086379225246");
        if (protectPayRequest.getTransactionType() == ProtectPayRequest.TXN_TYPE_SALE) {
            protectPayRequest.getProcessCard().setId(identity);
        } 
    }

    private ProtectPayResponse sendRequest(ProtectPayRequest protectPayRequest) throws Exception {
            return new ProtectPayConnector().send(protectPayRequest);
    }

    private boolean processResponse(ProtectPayResponse protectPayResponse) throws Exception {
       if(protectPayResponse.isSuccessful()) {
    	   String authorizationId = protectPayResponse.getXmlElementByExpression(ProtectPayResponse.TXN_ID);
    	   this.studentOrder.setStudentPaymentAuthorizationId(authorizationId);
    	   this.studentOrder.setStudentPaymentStatus("APPROVED");
    	   SendHTMLEmail sendHTMLEmail = new SendHTMLEmail();
			String emailMessage = sendHTMLEmail.getMessage(studentOrder, SendHTMLEmail.ORDER_PLACED);
			sendHTMLEmail.sendMessage(this.studentOrder.getStudent().getEmailId(), emailMessage, SendHTMLEmail.ORDER_PLACED);
    	   System.out.println("payment was successfull>>>>>>>>>>>>>>>>>>>>>>>>");
    	   return true;
       }
       String declinedCode = protectPayResponse.getXmlElementByExpression(ProtectPayResponse.RESULT_CODE_EXP);
       this.studentOrder.setStudentPaymentAuthorizationId(declinedCode);
	   this.studentOrder.setStudentPaymentStatus("DECLINED");
       System.out.println("payment failed>>>>>>>>>>>>>>>>>>>>>>>>");
       return false;       
    }
}
