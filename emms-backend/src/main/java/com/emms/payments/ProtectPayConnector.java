
package com.emms.payments;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;

import com.emms.payments.protocol.ProcessCard;
import com.emms.payments.protocol.ProtectPayNamespaceMapper;
import com.emms.payments.protocol.ProtectPayRequest;
import com.emms.payments.protocol.ProtectPayResponse;
public class ProtectPayConnector {
    public static final String PARAM_AUTHSERVER_PRIMARY_URL = "AUTHSERVERPRIMARYURL";
    public static final String PARAM_MAX_RETRIES = "MAXRETRIES";
    public static final String AUTH_AND_CAPTURE_METHOD_CALL = "/ProcessCard";

    // soap constants
    public static final String SOAP_ENV = "soapenv";
    public static final String SOAP_ACTION = "SOAPAction";
    public static final String CONTENT_TYPE = "Content-Type";
    public static final String CONTENT_TYPE_VALUE = "text/xml";
    public static final String NAME_SPACE_PREFIX_MAPPER = "com.sun.xml.internal.bind.namespacePrefixMapper";

    private static Log log = LogFactory.getLog(ProtectPayConnector.class);

    private static final int DEFAULT_MAX_RETRIES = 0;

    private static String authServerPrimaryUrl = "https://protectpaytest.propay.com/API/SPS.svc";

    public static int socketTimeoutMS;

    public static int socketConnectionTimeoutMS;

    private static int maxRetries;

    public ProtectPayResponse send(ProtectPayRequest request) throws Exception {
        int retryNumber = 0;
        SOAPMessage soapMessageXmlResponse = null;
        // create soap message
        SOAPMessage soapMessageXmlRequest = createSoapMessage(request);

        while (retryNumber <= maxRetries) {
            // sending request
            try {
                log.info("Processing ProtectPay request to " + authServerPrimaryUrl);
                if (log.isDebugEnabled()) {
                    log.debug("Sending ProtectPay request " + " (attempt " + (retryNumber + 1) + "/" + (maxRetries + 1) + ")\n");
                }
                // Create SOAP Connection
                SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
                SOAPConnection soapConnection = soapConnectionFactory.createConnection();
              
                // Send SOAP Message to SOAP Server
                soapMessageXmlResponse = soapConnection.call(soapMessageXmlRequest, authServerPrimaryUrl);
              
                log.info("ProtectPay request sent.");
                // If fault is null, then request was processed successfully, else, request was not valid request.
                if (soapMessageXmlResponse.getSOAPPart().getEnvelope().getBody().getFault() == null) {
                    try( ByteArrayOutputStream responseOutputStream = new ByteArrayOutputStream()) {
                        soapMessageXmlResponse.writeTo(responseOutputStream);
                        String soapXmlResponse = new String(responseOutputStream.toByteArray());
                        log.info("ProtectPay response: " + soapXmlResponse);
                    } catch (Exception e) {
                        log.warn("Error while logging protectpay response: "+e);
                    }
                    break;
                } else {
                    String faultCode = soapMessageXmlResponse.getSOAPPart().getEnvelope().getBody().getFault().getFaultCode();
                    String faultString = soapMessageXmlResponse.getSOAPPart().getEnvelope().getBody().getFault().getFaultString();
                    log.error("Response fault code: " + faultCode);
                    log.error("Response fault string: " + faultString);
                    if (retryNumber == maxRetries) {
                        throw new Exception("Invalid request fault code: " + faultCode);
                    }
                    retryNumber++;
                }
            } catch (IOException e) {             
                log.error("Connectivity Exception occurred when connecting to ProtectPay (attempt " + (retryNumber + 1) + "/" + (maxRetries + 1) + ")", e);
                if (retryNumber == maxRetries) {
                    throw new Exception("Failed to connect to ProtectPay with " + (retryNumber + 1) + " attempts.", e);
                }
                retryNumber++;
            } catch (Exception e) {
                throw (e);
            }
        }
        ProtectPayResponse protectPayResponse = new ProtectPayResponse();
        protectPayResponse.setSoapResponseDocument(soapMessageXmlResponse.getSOAPBody().extractContentAsDocument());
        return protectPayResponse;
    }

    private SOAPMessage createSoapMessage(ProtectPayRequest protectPayRequest) throws JAXBException,
            ParserConfigurationException, SOAPException, IOException {
        Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
        Marshaller marshaller = null;
        if (protectPayRequest.getTransactionType() == ProtectPayRequest.TXN_TYPE_SALE) {
            marshaller = JAXBContext.newInstance(ProcessCard.class).createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.setProperty(NAME_SPACE_PREFIX_MAPPER, new ProtectPayNamespaceMapper());
            marshaller.marshal(protectPayRequest.getProcessCard(), document);
        }
        MessageFactory messageFactory = MessageFactory.newInstance();
        SOAPMessage soapMessage = messageFactory.createMessage();
        SOAPPart part = soapMessage.getSOAPPart();
        /// Envelope creation
        SOAPEnvelope envelope = part.getEnvelope();
        envelope.removeNamespaceDeclaration(envelope.getPrefix());
        envelope.setPrefix(SOAP_ENV);
        envelope.addNamespaceDeclaration(ProtectPayNamespaceMapper.CONTRACT_PREFIX, ProtectPayNamespaceMapper.CONTRACT_URI);
        envelope.addNamespaceDeclaration(ProtectPayNamespaceMapper.TYPES_PREFIX, ProtectPayNamespaceMapper.TYPES_URI);
        envelope.addNamespaceDeclaration(ProtectPayNamespaceMapper.PROP_PREFIX, ProtectPayNamespaceMapper.PROP_URI);
        MimeHeaders headers = soapMessage.getMimeHeaders();
        headers.addHeader(CONTENT_TYPE, CONTENT_TYPE_VALUE);
        if(protectPayRequest.getTransactionType() == ProtectPayRequest.TXN_TYPE_SALE) {
            headers.addHeader(SOAP_ACTION, ProtectPayNamespaceMapper.SOAPACTION_URI+AUTH_AND_CAPTURE_METHOD_CALL);
        }
        envelope.getBody().addDocument(document);
        envelope.getBody().setPrefix(SOAP_ENV);
        return soapMessage;
    }
}
