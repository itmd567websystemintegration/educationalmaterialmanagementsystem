/**
 * 
 */
package com.emms.repository;

import org.springframework.data.repository.CrudRepository;

import com.emms.model.BillingAddress;

/**
 * @author Harshal
 *
 */
public interface BillingAddressRepository extends CrudRepository<BillingAddress, Long>{

}
