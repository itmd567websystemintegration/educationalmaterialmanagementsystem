/**
 * 
 */
package com.emms.repository;

import org.springframework.data.repository.CrudRepository;

import com.emms.model.DeliveryAddress;

/**
 * @author Harshal
 *
 */
public interface DeliveryAddressRepository extends CrudRepository<DeliveryAddress, Long> {

}
