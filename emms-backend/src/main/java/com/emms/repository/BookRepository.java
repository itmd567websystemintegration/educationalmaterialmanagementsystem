/**
 * 
 */
package com.emms.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.emms.model.Book;

/**
 * @author Harshal
 *
 */
public interface BookRepository extends CrudRepository<Book, Long> {
	List<Book> findBookByTitle(String tag);
}
