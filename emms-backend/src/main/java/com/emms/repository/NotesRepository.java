/**
 * 
 */
package com.emms.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.emms.model.Notes;

/**
 * @author Harshal
 *
 */
public interface NotesRepository extends CrudRepository<Notes, Long> {
	List<Notes> findBookByTitle(String tag);
}
