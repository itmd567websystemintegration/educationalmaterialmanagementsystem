/**
 * 
 */
package com.emms.repository;

import org.springframework.data.repository.CrudRepository;

import com.emms.model.StudentPaymentDetails;

/**
 * @author Harshal
 *
 */
public interface StudentPaymentDetailsRepository extends CrudRepository<StudentPaymentDetails, Long> {

}
