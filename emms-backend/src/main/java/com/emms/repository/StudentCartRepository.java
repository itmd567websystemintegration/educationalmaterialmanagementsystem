/**
 * 
 */
package com.emms.repository;

import org.springframework.data.repository.CrudRepository;

import com.emms.model.StudentCart;

/**
 * @author Harshal
 *
 */
public interface StudentCartRepository extends CrudRepository<StudentCart, Long> {

}
