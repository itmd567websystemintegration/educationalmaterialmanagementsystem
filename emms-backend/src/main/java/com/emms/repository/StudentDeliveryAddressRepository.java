/**
 * 
 */
package com.emms.repository;

import com.emms.model.StudentDeliveryDetails;
import org.springframework.data.repository.CrudRepository;

/**
 * @author Harshal
 *
 */
public interface StudentDeliveryAddressRepository extends CrudRepository<StudentDeliveryDetails, Long> {

}
