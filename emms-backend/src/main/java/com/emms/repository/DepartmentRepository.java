/**
 * 
 */
package com.emms.repository;

import org.springframework.data.repository.CrudRepository;

import com.emms.model.Department;
import com.emms.model.Student;

/**
 * @author Harshal
 *
 */
public interface DepartmentRepository  extends CrudRepository<Department, Long>{
}
