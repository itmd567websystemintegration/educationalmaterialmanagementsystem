/**
 * 
 */
package com.emms.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.emms.model.LabMaterial;

/**
 * @author Harshal
 *
 */
public interface LabMaterialRepository extends CrudRepository<LabMaterial, Long> {
	List<LabMaterial> findBookByTitle(String tag);
}
