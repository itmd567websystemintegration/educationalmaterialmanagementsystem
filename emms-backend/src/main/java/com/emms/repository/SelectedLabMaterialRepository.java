/**
 * 
 */
package com.emms.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.emms.model.SelectedLabMaterial;
import com.emms.model.StudentCart;

/**
 * @author Harshal
 *
 */
public interface SelectedLabMaterialRepository extends CrudRepository<SelectedLabMaterial, Long> {
    List<SelectedLabMaterial> findLabMaterialByStudentCart(StudentCart studentCart);
}
