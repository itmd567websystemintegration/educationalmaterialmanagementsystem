/**
 * 
 */
package com.emms.repository;

import org.springframework.data.repository.CrudRepository;

import com.emms.model.Payment;

/**
 * @author Harshal
 *
 */
public interface StudentPaymentRepository extends CrudRepository<Payment, Long> {

}
