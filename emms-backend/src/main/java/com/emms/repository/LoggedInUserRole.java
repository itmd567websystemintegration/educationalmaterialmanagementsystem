package com.emms.repository;

import org.springframework.data.repository.CrudRepository;

import com.emms.security.Role;

public interface LoggedInUserRole extends CrudRepository<Role, Integer> {

}
