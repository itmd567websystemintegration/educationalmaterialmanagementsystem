package com.emms.repository;

import org.springframework.data.repository.CrudRepository;

import com.emms.model.StudentBillingDetails;

public interface StudentBillingDetailsRepository extends CrudRepository<StudentBillingDetails, Long> {

}
