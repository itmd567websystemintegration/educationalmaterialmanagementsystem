/**
 * 
 */
package com.emms.repository;

import org.springframework.data.repository.CrudRepository;

import com.emms.model.Student;

/**
 * @author Harshal
 *
 */
public interface StudentRepository extends CrudRepository<Student, Long> {
	Student findByEmailId(String emailId);
	Student findByUserName(String userName);
	Student findByAccessToken(String userName);
}
