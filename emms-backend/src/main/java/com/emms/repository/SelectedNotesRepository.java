/**
 * 
 */
package com.emms.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import com.emms.model.SelectedNotes;
import com.emms.model.StudentCart;

/**
 * @author Harshal
 *
 */
public interface SelectedNotesRepository extends CrudRepository<SelectedNotes, Long> {
    List<SelectedNotes> findSelectedNotesByStudentCart(StudentCart studentCart);
}
