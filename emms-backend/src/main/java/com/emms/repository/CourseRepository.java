/**
 * 
 */
package com.emms.repository;

import org.springframework.data.repository.CrudRepository;

import com.emms.model.Course;
import com.emms.model.Department;

/**
 * @author Harshal
 *
 */
public interface CourseRepository  extends CrudRepository<Course, Long>{
}
