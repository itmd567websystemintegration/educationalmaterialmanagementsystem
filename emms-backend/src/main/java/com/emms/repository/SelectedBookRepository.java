/**
 * 
 */
package com.emms.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.emms.model.SelectedBook;
import com.emms.model.SelectedNotes;
import com.emms.model.StudentCart;

/**
 * @author Harshal
 *
 */
public interface SelectedBookRepository extends CrudRepository<SelectedBook, Long> {
    List<SelectedBook> findSelectedBookByStudentCart(StudentCart studentCart);
}
