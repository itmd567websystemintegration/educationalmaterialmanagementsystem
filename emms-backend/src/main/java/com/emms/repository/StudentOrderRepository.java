/**
 * 
 */
package com.emms.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.emms.model.StudentOrder;
import com.emms.model.Student;

/**
 * @author Harshal
 *
 */
public interface StudentOrderRepository extends CrudRepository<StudentOrder, Long> {
	List<StudentOrder> findByStudent(Student student);
}
