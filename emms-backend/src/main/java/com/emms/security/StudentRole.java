/**
 * 
 */
package com.emms.security;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.emms.model.Student;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author Harshal
 *
 */
@Entity
@Table(name = "STUDENT_ROLE")
public class StudentRole implements Serializable {
	private static final long serialVersionUID = 8890563125135327959L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long studentRoleId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "STUDENT_ID")
	@JsonIgnore
	private Student student;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ROLE_ID")
	@JsonIgnore
	private Role role;

	public StudentRole(Student student, Role role) {
		this.student = student;
		this.role = role;
	}

	public StudentRole() {
	}
	
	/**
	 * @return the student
	 */
	public Student getStudent() {
		return student;
	}

	/**
	 * @param student the student to set
	 */
	public void setStudent(Student student) {
		this.student = student;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}
	

	public long getStudentRoleId() {
		return studentRoleId;
	}

	public void setUserRoleId(long studentRoleId) {
		this.studentRoleId = studentRoleId;
	}
}
