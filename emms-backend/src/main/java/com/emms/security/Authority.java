/**
 * 
 */
package com.emms.security;

import java.io.Serializable;

import org.springframework.security.core.GrantedAuthority;

/**
 * @author Harshal
 *
 */
public class Authority implements GrantedAuthority, Serializable {

	private static final long serialVersionUID = 353859828906296479L;
	private final String authority;

	public Authority(String authority) {
		this.authority = authority;
	}

	@Override
	public String getAuthority() {
		// TODO Auto-generated method stub
		return authority;
	}

}
