/**
 * 
 */
package com.emms.security;

import java.util.Calendar;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.emms.model.Student;

/**
 * @author Harshal
 *
 */
@Entity
public class PasswordResetKey {
	private static final int KEY_EXPIRY_TIME = 1440;
	private Date resetPasswordExpiryDate;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	private String passwordResetKey;

	@OneToOne(targetEntity = Student.class, fetch = FetchType.EAGER)
	@JoinColumn(nullable = false, name = "STUDENT_ID")
	private Student student;

	public PasswordResetKey() {
        super();
    }

	public PasswordResetKey(final String passwordResetKey) {
        super();
        this.passwordResetKey = passwordResetKey;
        this.resetPasswordExpiryDate = calculatePasswordKeyExpiryDate(KEY_EXPIRY_TIME);
    }

	public PasswordResetKey(final Student student, final String key) {
        super();
        this.passwordResetKey = key;
        this.resetPasswordExpiryDate = calculatePasswordKeyExpiryDate(KEY_EXPIRY_TIME);
        this.student = student;
        
    }

	private Date calculatePasswordKeyExpiryDate(final int expiryTimeInMinutes) {
		final Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MINUTE, expiryTimeInMinutes);
		cal.setTimeInMillis(new Date().getTime());
		return new Date(cal.getTime().getTime());
	}

	public void modifyPasswordResetKey(final String passwordResetKey) {
		this.passwordResetKey = passwordResetKey;
		this.resetPasswordExpiryDate = calculatePasswordKeyExpiryDate(KEY_EXPIRY_TIME);
	}

	public static int getEXPIRATION() {
		return KEY_EXPIRY_TIME;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the passwordResetKey
	 */
	public String getPasswordResetKey() {
		return passwordResetKey;
	}

	/**
	 * @param passwordResetKey the key to set
	 */
	public void setPasswordResetKey(String passwordResetKey) {
		this.passwordResetKey = passwordResetKey;
	}

	/**
	 * @return the student
	 */
	public Student getStudent() {
		return student;
	}

	/**
	 * @param student the student to set
	 */
	public void setStudent(Student student) {
		this.student = student;
	}

	public Date getPasswordExpiryDate() {
		return resetPasswordExpiryDate;
	}

	public void setPasswordExpiryDate(Date passwordExpiryDate) {
		this.resetPasswordExpiryDate = passwordExpiryDate;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "PasswordResetKey [expiryDate=" + resetPasswordExpiryDate + ", id=" + id + ", passwordResetKey=" + passwordResetKey + ", student=" + student
				+ "]";
	}
}
