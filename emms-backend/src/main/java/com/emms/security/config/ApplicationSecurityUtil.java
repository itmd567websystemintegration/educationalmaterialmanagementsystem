/**
 * 
 */
package com.emms.security.config;

import java.security.SecureRandom;
import java.util.Random;

import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

/**
 * @author Harshal
 *
 */
@Component
public class ApplicationSecurityUtil {
private static final String EMMS_SALT = "SECURE_EMMS_SALT";	
	@Bean
	public static BCryptPasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder(12, new SecureRandom(EMMS_SALT.getBytes()));
	}
	
	@Bean
	public static String randomPassword() {
		String STIMULI = "1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		StringBuilder generatedSalt = new StringBuilder();
		Random rnd = new Random();
		
		while(generatedSalt.length() < 18) {
			int index = (int) (rnd.nextFloat() * STIMULI.length());
			generatedSalt.append(STIMULI.charAt(index));
		}		
		return generatedSalt.toString();
	}
}
