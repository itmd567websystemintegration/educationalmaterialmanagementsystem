/**
 * 
 */
package com.emms.security;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author Harshal
 *
 */

@Entity
@Table(name = "ROLE")
public class Role implements Serializable {
	private static final long serialVersionUID = -8421153980916474100L;
	@Id
	private int roleId;
	private String name;

	@OneToMany(mappedBy = "role", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JsonIgnore
	private Set<StudentRole> studentRoleSet = new HashSet<>();

	public Role() {

	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<StudentRole> getUserRoles() {
		return studentRoleSet;
	}

	public void setUserRoles(Set<StudentRole> studentRoleSet) {
		this.studentRoleSet = studentRoleSet;
	}
}
