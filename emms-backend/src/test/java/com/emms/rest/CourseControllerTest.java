/**
 * 
 */
package com.emms.rest;

import static org.junit.Assert.assertArrayEquals;

import org.junit.Before;
import org.junit.Test;

import com.emms.model.Course;
import com.emms.service.CourseService;
import com.emms.service.impl.CourseServiceImpl;

/**
 * @author Harshal
 *
 */
public class CourseControllerTest {
	private CourseService courseService;
	/**
	 * Test method for {@link com.emms.service.impl.CourseServiceImpl#getAllCourse()}.
	 */
	@Before
	public void setUp(){
		courseService = new CourseServiceImpl();
	}
	@Test
	public void testGetAllCourse() {
		courseService = new CourseServiceImpl();
		courseService.getAllCourse().iterator().forEachRemaining(e-> {
			System.out.println("CourseName: "+ e.getCourseName());
		} );
	}

	/**
	 * Test method for {@link com.emms.service.impl.CourseServiceImpl#findById(java.lang.Long)}.
	 */
	@Test
	public void testFindById() {
		courseService = new CourseServiceImpl();
		Course c1 = courseService.findById((long) 1);
		String courseName = c1.getCourseName();
		assertArrayEquals("FindById() test passed", "java".getBytes(), courseName.getBytes());
	}

	/**
	 * Test method for {@link com.emms.service.impl.CourseServiceImpl#addCourse(com.emms.model.Course)}.
	 */
	@Test
	public void testAddCourse() {
		courseService = new CourseServiceImpl();
		Course c1 = new Course();
		courseService.addCourse(c1);	
	}

}
